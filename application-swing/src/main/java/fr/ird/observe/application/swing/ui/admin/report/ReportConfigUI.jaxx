<!--
  #%L
  ObServe :: Application Swing
  %%
  Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->
<!-- ************************************************************* -->
<!-- La configuration de l'import GPS                              -->
<!-- ************************************************************* -->

<Table id="reportConfig">

  <import>
    fr.ird.observe.application.swing.ui.admin.AdminUIModel
    fr.ird.observe.services.dto.ObserveModelType

    java.io.File

    static fr.ird.observe.application.swing.ui.UIHelper.getStringValue
  </import>

  <ReportUIHandler id='handler' initializer='getContextValue(ReportUIHandler.class)'/>

  <AdminUIModel id='model' initializer='getContextValue(AdminUIModel.class)'/>

  <ReportModel id='stepModel' initializer='model.getReportModel()'/>

  <script><![CDATA[
public void destroy() {
    model = null;
    stepModel = null;
}

@Override
protected void finalize() throws Throwable {
    super.finalize();
    destroy();
}
]]>
  </script>

  <row>
    <cell anchor="west">
      <JLabel id='modelTypeLabel'/>
    </cell>
    <cell weightx='1' fill="horizontal" columns="2">
      <EnumEditor id="modelType"
                  genericType='ObserveModelType'
                  constructorParams='ObserveModelType.class'
                  onItemStateChanged='getHandler().setModelTypeFromEvent(event)'/>
    </cell>
    </row>
  <row>
    <cell anchor="west">
      <JLabel id='reportFileLabel'/>
    </cell>
    <cell weightx='1' fill="horizontal">
      <JTextField id="reportFile"
                  onKeyReleased='stepModel.setReportFile(new File(((JTextField)event.getSource()).getText()))'/>
    </cell>
    <cell anchor="east">
      <JButton id="reportFileChooserAction"
               onActionPerformed="getHandler().chooseReportFile()"/>
    </cell>
  </row>
</Table>
