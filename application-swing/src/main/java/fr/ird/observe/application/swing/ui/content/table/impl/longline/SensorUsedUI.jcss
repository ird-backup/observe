/*
 * #%L
 * ObServe :: Swing
 * %%
 * Copyright (C) 2008 - 2010 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

#sensorTypeLabel {
  text:"observe.content.sensorUsed.sensorType";
  labelFor:{sensorType};
}

#sensorType {
  property:{SensorUsedDto.PROPERTY_SENSOR_TYPE};
  selectedItem:{tableEditBean.getSensorType()};
  _validatorLabel:{t("observe.content.sensorUsed.sensorType")};
}

#sensorDataFormatLabel {
  text:"observe.content.sensorUsed.sensorDataFormat";
  labelFor:{sensorDataFormat};
}

#sensorDataFormat {
  property:{SensorUsedDto.PROPERTY_SENSOR_DATA_FORMAT};
  selectedItem:{tableEditBean.getSensorDataFormat()};
  _validatorLabel:{t("observe.content.sensorUsed.sensorDataFormat")};
}

#sensorBrandLabel {
  text:"observe.content.sensorUsed.sensorBrand";
  labelFor:{sensorBrand};
}

#sensorBrand {
  property:{SensorUsedDto.PROPERTY_SENSOR_BRAND};
  selectedItem:{tableEditBean.getSensorBrand()};
  _validatorLabel:{t("observe.content.sensorUsed.sensorBrand")};
}

#sensorSerialNoLabel {
  text:"observe.content.sensorUsed.sensorSerialNo";
  labelFor:{sensorSerialNo};
}

#sensorSerialNo {
  _tablePropertyName:{SensorUsedDto.PROPERTY_SENSOR_SERIAL_NO};
  text:{tableEditBean.getSensorSerialNo()};
  _validatorLabel:{t("observe.content.sensorUsed.sensorSerialNo")};
}

#resetSensorSerialNo {
  toolTipText:"observe.content.sensorUsed.action.reset.sensorSerialNo.tip";
  _resetTablePropertyName: {SensorUsedDto.PROPERTY_SENSOR_SERIAL_NO};
}

#dataLocationLabel {
  text:"observe.content.sensorUsed.dataLocation";
  toolTipText:"observe.content.sensorUsed.dataLocation.tip";
  labelFor:{dataLocation};
}

#dataLocation {
  _tablePropertyName:{SensorUsedDto.PROPERTY_DATA_LOCATION};
  text:{tableEditBean.getDataLocation()};
}

#resetDataLocation {
  toolTipText:"observe.content.sensorUsed.action.reset.dataLocation.tip";
  _resetTablePropertyName: {SensorUsedDto.PROPERTY_DATA_LOCATION};
  focusable:false;
}

#dataLabel {
  text:"observe.content.sensorUsed.data";
}

#importDataButton {
  actionIcon:data-import;
  text:"observe.content.sensorUsed.importData";
  toolTipText:"observe.content.sensorUsed.importData.tip";
}

#exportDataButton {
  actionIcon:data-export;
  text:"observe.content.sensorUsed.exportData";
  toolTipText:"observe.content.sensorUsed.exportData.tip";
  enabled:{tableEditBean.isHasData()};
}

#deleteDataButton {
  actionIcon:delete;
  text:"observe.content.sensorUsed.deleteData";
  toolTipText:"observe.content.sensorUsed.deleteData.tip";
  enabled:{tableEditBean.isHasData()};
}

#comment {
  columnHeaderView:{new JLabel(t("observe.common.comment.activity"))};
}
