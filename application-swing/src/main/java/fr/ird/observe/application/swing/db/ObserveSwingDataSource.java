package fr.ird.observe.application.swing.db;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import fr.ird.observe.application.swing.ObserveRunner;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.configuration.ObserveSwingApplicationConfig;
import fr.ird.observe.application.swing.db.event.ObserveSwingDataSourceEvent;
import fr.ird.observe.application.swing.db.event.ObserveSwingDataSourceListener;
import fr.ird.observe.services.ObserveService;
import fr.ird.observe.services.ObserveServiceInitializer;
import fr.ird.observe.services.ObserveServicesProvider;
import fr.ird.observe.services.configuration.*;
import fr.ird.observe.services.configuration.rest.ObserveDataSourceConfigurationRest;
import fr.ird.observe.services.configuration.topia.ObserveDataSourceConfigurationTopiaH2;
import fr.ird.observe.services.configuration.topia.ObserveDataSourceConfigurationTopiaPG;
import fr.ird.observe.services.dto.*;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.ReferentialReferenceSet;
import fr.ird.observe.services.runner.ObserveServiceMainFactory;
import fr.ird.observe.services.service.*;
import fr.ird.observe.services.service.actions.consolidate.ConsolidateDataService;
import fr.ird.observe.services.service.actions.report.ReportService;
import fr.ird.observe.services.service.actions.synchro.referential.diff.ReferentialSynchronizeDiffService;
import fr.ird.observe.services.service.actions.synchro.referential.legacy.UnidirectionalReferentialSynchronizeLocalService;
import fr.ird.observe.services.service.actions.synchro.referential.ng.ReferentialSynchronizeService;
import fr.ird.observe.services.service.actions.validate.ValidateService;
import fr.ird.observe.services.service.longline.*;
import fr.ird.observe.services.service.seine.*;
import fr.ird.observe.services.service.trip.TripManagementService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.beans.AbstractSerializableBean;
import org.nuiton.version.Version;

import javax.swing.*;
import javax.swing.event.EventListenerList;
import java.io.Closeable;
import java.io.File;
import java.util.Locale;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class ObserveSwingDataSource extends AbstractSerializableBean implements Closeable, ObserveServicesProvider {

    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(ObserveSwingDataSource.class);

    /** le conteneur de listeners */
    protected final EventListenerList listenerList;

    protected final ObserveDataSourceConfiguration configuration;

    protected final ObserveReferentialCache referentialCache;

    protected ObserveDataSourceConnection connection;

    // indique que la connexion a expiré mais que la source n'est pas complétement fermer
    private boolean expired;

    public ObserveSwingDataSource(ObserveDataSourceConfiguration configuration) {
        this.configuration = configuration;
        this.listenerList = new EventListenerList();
        this.referentialCache = new ObserveReferentialCache();
        this.expired = false;
    }

    public ObserveDataSourceConfiguration getConfiguration() {
        return configuration;
    }

    public ObserveDataSourceConnection getConnection() {
        return connection;
    }

    public String getLabel() {
        return configuration.getLabel();
    }

    public boolean isSqlDataSource() {
        return ObserveDataSourceType.SQL == configuration.getType();
    }

    public boolean canWriteData() {
        return connection != null && connection.canWriteData();
    }

    public Version getVersion() {
        Version result = null;
        if (connection != null) {
            result = connection.getVersion();
        }
        return result;
    }

    public boolean canReadReferential() {
        return connection != null && connection.canReadReferential();
    }

    public boolean canReadData() {
        return connection != null && connection.canReadData();
    }

    public boolean canWriteReferential() {
        return connection != null && connection.canWriteReferential();
    }

    public ObserveDataSourceInformation getInformation() {
        return new ObserveDataSourceInformation(
                canReadReferential(),
                canWriteReferential(),
                canReadData(),
                canWriteData(),
                getVersion(),
                getVersion(),
                ImmutableList.of());
    }

    public void open() throws DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException, BabModelVersionException {

        checkIsNotAlreadyOpen();

        fireNewMessage(t("observe.storage.message.opening", getLabel()));
        fireOpening();


        DataSourceService dataSourceService = newDataSourceService();

        connection = dataSourceService.open(configuration);

        fireNewMessage(t("observe.storage.message.opened", getLabel()));
        fireOpened();
    }

    public void create(DataSourceCreateConfigurationDto createDto)
            throws IncompatibleDataSourceCreateConfigurationException, DataSourceCreateWithNoReferentialImportException,
            DatabaseNotFoundException, DatabaseConnexionNotAuthorizedException, BabModelVersionException {

        checkIsNotAlreadyOpen();

        fireNewMessage(t("observe.storage.message.creating", getLabel()));
        fireOpening();

        DataSourceService dataSourceService = newDataSourceService();

        connection = dataSourceService.create(configuration, createDto);

        fireNewMessage(t("observe.storage.message.created", getLabel()));
        fireOpened();
    }

    @Override
    public void close() {

        checkIsOpen();

        fireNewMessage(t("observe.storage.message.closing", getLabel()));
        fireClosing();

        referentialCache.close();

        try {
            // si la connection a expirée la source a deja été fermer pas le serveur
            if (!expired) {
                DataSourceService dataSourceService = newDataSourceService();

                dataSourceService.close();
            }
        } finally {

            ObserveRunner.cleanMemory();

            connection = null;
            expired = false;

            fireNewMessage(t("observe.storage.message.closed", getLabel()));
            fireClosed();
        }

    }

    public void destroy() throws DatabaseDestroyNotAuthorizedException {

        checkIsOpen();

        fireNewMessage(t("observe.storage.message.destroying", getLabel()));
        fireClosing();

        referentialCache.close();

        DataSourceService dataSourceService = newDataSourceService();

        dataSourceService.destroy();

        connection = null;

        fireNewMessage(t("observe.storage.message.destroyed", getLabel()));
        fireClosed();

    }

    public Set<ObserveDbUserDto> getUsers() {

        checkIsNotOpen();

        DataSourceService dataSourceService = newDataSourceService();

        return dataSourceService.getUsers(getConfiguration());
    }


    public void applySecurity(Set<ObserveDbUserDto> users) {

        checkIsNotOpen();

        DataSourceService dataSourceService = newDataSourceService();

        dataSourceService.applySecurity(getConfiguration(), users);
    }

    public void migrateData(ObserveDataSourceInformation dataSourceInformation, Version targetVersion) {

        checkIsNotOpen();

        Version dbVersion = dataSourceInformation.getVersion();
        if (!dataSourceInformation.getMigrations().isEmpty()) {

            if (dbVersion.before(dataSourceInformation.getMinnimumVersion())) {

                JOptionPane.showMessageDialog(
                        null,
                        t("observe.storage.migrate.not.possible.before.version.3.0.message", targetVersion, dbVersion),
                        t("observe.storage.migrate.not.possible.before.version.3.0.title", targetVersion),
                        JOptionPane.WARNING_MESSAGE);

            } else {

                int answer = JOptionPane.showConfirmDialog(
                        null,
                        t("observe.storage.migrate.askUser.message", dbVersion, targetVersion, dataSourceInformation.getMigrations()),
                        t("observe.storage.migrate.askUser.title", targetVersion),
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.WARNING_MESSAGE);

                if (answer == JOptionPane.YES_OPTION) {

                    DataSourceService dataSourceService = newDataSourceService();


                    if (log.isInfoEnabled()) {
                        log.info("Migrate data source " + getLabel() + " in " + dbVersion + " to " + targetVersion);
                    }

                    dataSourceService.migrateData(getConfiguration());
                }
            }
        }

    }

    public void migrateDataIfPossible(ObserveDataSourceInformation dataSourceInformation, Version targetVersion) {

        checkIsNotOpen();

        Version dbVersion = dataSourceInformation.getVersion();
        if (!dataSourceInformation.getMigrations().isEmpty()) {

            if (dbVersion.before(dataSourceInformation.getMinnimumVersion())) {

                log.warn(t("observe.storage.migrate.not.possible.before.version.3.0.message", targetVersion, dbVersion));

            } else {

                log.info(t("observe.storage.migrate.askUser.message", dbVersion, targetVersion, dataSourceInformation.getMigrations()));

                DataSourceService dataSourceService = newDataSourceService();


                if (log.isInfoEnabled()) {
                    log.info("Migrate data source " + getLabel() + " in " + dbVersion + " to " + targetVersion);
                }

                dataSourceService.migrateData(getConfiguration());

            }
        }

    }

    public ImmutableMap<Class<?>, ReferentialReferenceSet<?>> updateReferentialReferenceSetsCache(String referentialReferenceSetsRequestName) {

        ReferentialService referentialService = newReferentialService();
        return referentialCache.loadReferenceSets(referentialService, referentialReferenceSetsRequestName);

    }

    protected <D extends ReferentialDto> ReferentialReferenceSet<D> getReferentialReferenceSet(Class<D> type) {

        checkIsOpen();

        ReferentialService referentialService = newReferentialService();
        return referentialCache.getReferentialReferenceSet(referentialService, type);
    }

    public <D extends ReferentialDto> Set<ReferentialReference<D>> getReferentialReferences(Class<D> type) {

        ReferentialReferenceSet<D> referentialReferenceSet = getReferentialReferenceSet(type);
        return referentialReferenceSet.getReferences();

    }

    public <D extends ReferentialDto> ReferentialReference<D> getReferentialReference(Class<D> type, String id) {

        ReferentialReferenceSet<D> referentialReferenceSet = getReferentialReferenceSet(type);
        return referentialReferenceSet.getReferenceById(id);

    }

    public boolean isOpen() {
        return connection != null;
    }

    @Override
    public SetLonglineService newSetLonglineService() {
        return newService(SetLonglineService.class);
    }

    public ObserveDataSourceInformation checkCanConnect() throws DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException {

        checkIsNotAlreadyOpen();

        DataSourceService dataSourceService = newDataSourceService();

        return dataSourceService.checkCanConnect(configuration);

    }

    public boolean isLocal() {
        return configuration instanceof ObserveDataSourceConfigurationTopiaH2;
    }

    public boolean isRemote() {
        return configuration instanceof ObserveDataSourceConfigurationTopiaPG;
    }

    public boolean isServer() {
        return configuration instanceof ObserveDataSourceConfigurationRest;
    }

    public void addObserveSwingDataSourceListener(ObserveSwingDataSourceListener listener) {
        listenerList.add(ObserveSwingDataSourceListener.class, listener);
    }

    public ObserveSwingDataSourceListener[] getObserveSwingDataSourceListener() {
        return listenerList.getListeners(ObserveSwingDataSourceListener.class);
    }

    public void removeObserveSwingDataSourceListener(ObserveSwingDataSourceListener listener) {
        if (log.isInfoEnabled()) {
            log.info("removing listener " + listener);
        }
        listenerList.remove(ObserveSwingDataSourceListener.class, listener);
    }

    public void removeObserveSwingDataSourceListeners() {
        // remove all listeners
        for (ObserveSwingDataSourceListener listener : getObserveSwingDataSourceListener()) {
            removeObserveSwingDataSourceListener(listener);
        }
    }

    public void fireNewMessage(String message) {
        fireNewMessage(message, ObserveSwingDataSourceEvent.MessageLevel.INFO);
    }

    public void fireNewMessage(String message, ObserveSwingDataSourceEvent.MessageLevel level) {
        ObserveSwingDataSourceEvent evt = new ObserveSwingDataSourceEvent(this, message, level);
        for (ObserveSwingDataSourceListener listener : getObserveSwingDataSourceListener()) {
            listener.onNewMessage(evt);
        }
    }

    public boolean isExpired() {
        return expired;
    }

    public void expired() {
        this.expired = true;
    }

    protected ObserveServiceInitializer getObserveServiceInitializer() {

        ObserveSwingApplicationContext context = ObserveSwingApplicationContext.get();

        ObserveSwingApplicationConfig config = context.getConfig();

        Locale locale = config.getLocale();

        File tmpDirectory = config.getTmpDirectory();

        ReferentialLocale referentialLocale = ReferentialLocale.valueOf(locale);

        ObserveDataSourceConfigurationAndConnection configurationAndConnection = new ObserveDataSourceConfigurationAndConnection(configuration, connection);

        ObserveSpeciesListConfiguration speciesListConfiguration = new ObserveSpeciesListConfiguration();
        speciesListConfiguration.setSpeciesListLonglineCatchId(config.getSpeciesListLonglineCatchId());
        speciesListConfiguration.setSpeciesListLonglineDepredatorId(config.getSpeciesListLonglineDepredatorId());
        speciesListConfiguration.setSpeciesListLonglineEncounterId(config.getSpeciesListLonglineEncounterId());
        speciesListConfiguration.setSpeciesListSeineNonTargetCatchId(config.getSpeciesListSeineNonTargetCatchId());
        speciesListConfiguration.setSpeciesListSeineObjectObservedSpeciesId(config.getSpeciesListSeineObjectObservedSpeciesId());
        speciesListConfiguration.setSpeciesListSeineObjectSchoolEstimateId(config.getSpeciesListSeineObjectSchoolEstimateId());
        speciesListConfiguration.setSpeciesListSeineSchoolEstimateId(config.getSpeciesListSeineSchoolEstimateId());
        speciesListConfiguration.setSpeciesListSeineTargetCatchId(config.getSpeciesListSeineTargetCatchId());

        return ObserveServiceInitializer.create(
                locale,
                referentialLocale,
                tmpDirectory,
                speciesListConfiguration,
                configurationAndConnection);
    }

    protected void fireOpening() {
        ObserveSwingDataSourceEvent evt = new ObserveSwingDataSourceEvent(this);
        for (ObserveSwingDataSourceListener listener : getObserveSwingDataSourceListener()) {
            listener.onOpening(evt);
        }
    }

    protected void fireOpened() {
        ObserveSwingDataSourceEvent evt = new ObserveSwingDataSourceEvent(this);
        for (ObserveSwingDataSourceListener listener : getObserveSwingDataSourceListener()) {
            listener.onOpened(evt);
        }
    }

    protected void fireClosing() {
        ObserveSwingDataSourceEvent evt = new ObserveSwingDataSourceEvent(this);
        for (ObserveSwingDataSourceListener listener : getObserveSwingDataSourceListener()) {
            listener.onClosing(evt);
        }
    }

    protected void fireClosed() {
        ObserveSwingDataSourceEvent evt = new ObserveSwingDataSourceEvent(this);
        for (ObserveSwingDataSourceListener listener : getObserveSwingDataSourceListener()) {
            listener.onClosed(evt);
        }
    }

    protected void checkIsOpen() {
        Preconditions.checkState(isOpen(), "Connection is not open");
    }

    protected void checkIsNotAlreadyOpen() {
        Preconditions.checkState(!isOpen(), "Connection is already open");
    }

    protected void checkIsNotOpen() {
        Preconditions.checkState(!isOpen(), "Connection is open");
    }

    private <S extends ObserveService> S newService(Class<S> serviceType) {

        ObserveServiceInitializer observeServiceInitializer = getObserveServiceInitializer();

        ObserveServiceMainFactory serviceFactory = ObserveSwingApplicationContext.get().getMainServiceFactory();
        return serviceFactory.newService(observeServiceInitializer, serviceType);
    }

    @Override
    public LastUpdateDateService newLastUpdateDateService() {
        return newService(LastUpdateDateService.class);
    }

    @Override
    public SqlScriptProducerService newSqlScriptProducerService() {
        return newService(SqlScriptProducerService.class);
    }

    @Override
    public ValidateService newValidateService() {
        return newService(ValidateService.class);
    }

    @Override
    public ReportService newReportService() {
        return newService(ReportService.class);
    }

    @Override
    public PingService newPingService() {
        return newService(PingService.class);
    }

    @Override
    public TripManagementService newTripManagementService() {
        return newService(TripManagementService.class);
    }

    @Override
    public ConsolidateDataService newConsolidateDataService() {
        return newService(ConsolidateDataService.class);
    }

    @Override
    public UnidirectionalReferentialSynchronizeLocalService newUnidirectionalReferentialSynchronizeLocalService() {
        return newService(UnidirectionalReferentialSynchronizeLocalService.class);
    }

    @Override
    public ReferentialSynchronizeService newReferentialSynchronizeService() {
        return newService(ReferentialSynchronizeService.class);
    }

    @Override
    public ReferentialSynchronizeDiffService newReferentialSynchronizeDiffService() {
        return newService(ReferentialSynchronizeDiffService.class);
    }

    @Override
    public DataSourceService newDataSourceService() {
        return newService(DataSourceService.class);
    }

    @Override
    public ReferentialService newReferentialService() {
        return newService(ReferentialService.class);
    }

    @Override
    public TripSeineService newTripSeineService() {
        return newService(TripSeineService.class);
    }

    @Override
    public RouteService newRouteService() {
        return newService(RouteService.class);
    }

    @Override
    public FloatingObjectService newFloatingObjectService() {
        return newService(FloatingObjectService.class);
    }

    @Override
    public ActivitySeineService newActivitySeineService() {
        return newService(ActivitySeineService.class);
    }

    @Override
    public SetSeineService newSetSeineService() {
        return newService(SetSeineService.class);
    }

    @Override
    public TripLonglineService newTripLonglineService() {
        return newService(TripLonglineService.class);
    }

    @Override
    public ActivityLonglineService newActivityLonglineService() {
        return newService(ActivityLonglineService.class);
    }

    @Override
    public SetLonglineGlobalCompositionService newSetLonglineGlobalCompositionService() {
        return newService(SetLonglineGlobalCompositionService.class);
    }

    @Override
    public SetLonglineDetailCompositionService newSetLonglineDetailCompositionService() {
        return newService(SetLonglineDetailCompositionService.class);
    }

    @Override
    public TransmittingBuoyOperationService newTransmittingBuoyOperationService() {
        return newService(TransmittingBuoyOperationService.class);
    }

    @Override
    public BranchlineService newBranchlineService() {
        return newService(BranchlineService.class);
    }

    @Override
    public SetLonglineCatchService newSetLonglineCatchService() {
        return newService(SetLonglineCatchService.class);
    }

    @Override
    public ActivityLongLineEncounterService newActivityLongLineEncounterService() {
        return newService(ActivityLongLineEncounterService.class);
    }

    @Override
    public TripLonglineGearUseService newTripLonglineGearUseService() {
        return newService(TripLonglineGearUseService.class);
    }

    @Override
    public ActivityLongLineSensorUsedService newActivityLongLineSensorUsedService() {
        return newService(ActivityLongLineSensorUsedService.class);
    }

    @Override
    public TdrService newTdrService() {
        return newService(TdrService.class);
    }

    @Override
    public TripSeineGearUseService newTripSeineGearUseService() {
        return newService(TripSeineGearUseService.class);
    }

    @Override
    public NonTargetCatchService newNonTargetCatchService() {
        return newService(NonTargetCatchService.class);
    }

    @Override
    public NonTargetSampleService newNonTargetSampleService() {
        return newService(NonTargetSampleService.class);
    }

    @Override
    public ObjectObservedSpeciesService newObjectObservedSpeciesService() {
        return newService(ObjectObservedSpeciesService.class);
    }

    @Override
    public ObjectSchoolEstimateService newObjectSchoolEstimateService() {
        return newService(ObjectSchoolEstimateService.class);
    }

    @Override
    public SchoolEstimateService newSchoolEstimateService() {
        return newService(SchoolEstimateService.class);
    }

    @Override
    public TargetCatchService newTargetCatchService() {
        return newService(TargetCatchService.class);
    }

    @Override
    public TargetSampleService newTargetSampleService() {
        return newService(TargetSampleService.class);
    }
}
