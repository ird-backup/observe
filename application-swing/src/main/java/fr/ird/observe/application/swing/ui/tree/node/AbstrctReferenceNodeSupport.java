package fr.ird.observe.application.swing.ui.tree.node;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ui.tree.loadors.AbstractNodeChildLoador;
import fr.ird.observe.services.dto.AbstractReference;
import fr.ird.observe.services.dto.IdDto;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created on 4/9/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 4.0
 */
public abstract class AbstrctReferenceNodeSupport<E extends IdDto, R extends AbstractReference<E>> extends ObserveNode {

    /** Logger. */
    private static final Log log = LogFactory.getLog(AbstrctReferenceNodeSupport.class);

    private static final long serialVersionUID = 1L;

    /**
     * Pour cacher l'entité attachée au noeud.
     * Elle pourra être directement injectée pour éviter de la recharger (lors de la création d'un modèle initiale).
     *
     * @since 4.0
     */
    protected transient R entity;

    protected boolean reloadEntity;

    protected abstract R fetchEntity();

    protected AbstrctReferenceNodeSupport(Class<E> type, R entity, AbstractNodeChildLoador<?, ?> childLoador) {
        this(type, entity, null, childLoador);
    }

    protected AbstrctReferenceNodeSupport(Class<E> type, R entity, String context, AbstractNodeChildLoador<?, ?> childLoador) {
        super(type, entity.getId(), context, childLoador, false);
        this.entity = entity;
    }

    @Override
    public void setDirty(boolean dirty) {
        super.setDirty(dirty);

        if (dirty && reloadEntity) {
            entity = null;
        }

    }

    public R getEntity() {
        return entity;
    }

    public R getOrLoadEntity() {

        if (entity == null) {

            if (log.isInfoEnabled()) {
                log.info("will load entity " + internalClass.getSimpleName() + " : " + id);
            }

            entity = fetchEntity();

        }

        return entity;
    }

    public void setReloadEntity() {
        this.reloadEntity = true;
    }

}
