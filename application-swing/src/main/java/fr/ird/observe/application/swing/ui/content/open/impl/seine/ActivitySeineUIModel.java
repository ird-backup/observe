package fr.ird.observe.application.swing.ui.content.open.impl.seine;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.seine.VesselActivitySeineDto;
import fr.ird.observe.services.dto.referential.seine.VesselActivitySeineHelper;
import fr.ird.observe.services.dto.seine.ActivitySeineDto;
import fr.ird.observe.application.swing.ui.content.open.ContentOpenableUIModel;

import java.util.Set;

/**
 * Created on 9/26/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since XXX
 */
public class ActivitySeineUIModel extends ContentOpenableUIModel<ActivitySeineDto> {

    public static final String PROPERTY_GENERAL_TAB_VALID = "generalTabValid";

    public static final String PROPERTY_MEASUREMENTS_TAB_VALID = "measurementsTabValid";

    public static final String PROPERTY_OBSERVED_SYSTEM_TAB_VALID = "observedSystemTabValid";

    public static final String PROPERTY_SET_OPERATION = "setOperation";

    public static final String PROPERTY_CHANGED_ZONE_OPERATION = "changedZoneOperation";

    public static final Set<String> GENERAL_TAB_PROPERTIES =
            ImmutableSet.<String>builder().add(ActivitySeineDto.PROPERTY_TIME,
                                               ActivitySeineDto.PROPERTY_LATITUDE,
                                               ActivitySeineDto.PROPERTY_LONGITUDE,
                                               ActivitySeineDto.PROPERTY_VESSEL_ACTIVITY_SEINE,
                                               ActivitySeineDto.PROPERTY_SURROUNDING_ACTIVITY,
                                               ActivitySeineDto.PROPERTY_PREVIOUS_FPA_ZONE,
                                               ActivitySeineDto.PROPERTY_CURRENT_FPA_ZONE,
                                               ActivitySeineDto.PROPERTY_NEXT_FPA_ZONE,
                                               ActivitySeineDto.PROPERTY_ERS_ID).build();

    public static final Set<String> MEASUREMENTS_TAB_PROPERTIES =
            ImmutableSet.<String>builder().add(ActivitySeineDto.PROPERTY_VESSEL_SPEED,
                                               ActivitySeineDto.PROPERTY_SEA_SURFACE_TEMPERATURE,
                                               ActivitySeineDto.PROPERTY_WIND,
                                               ActivitySeineDto.PROPERTY_DETECTION_MODE,
                                               ActivitySeineDto.PROPERTY_REASON_FOR_NO_FISHING).build();

    public static final Set<String> OBSERVED_SYSTEM_TAB_PROPERTIES =
            ImmutableSet.<String>builder().add(ActivitySeineDto.PROPERTY_OBSERVED_SYSTEM_DISTANCE,
                                               ActivitySeineDto.PROPERTY_OBSERVED_SYSTEM,
                                               ActivitySeineDto.PROPERTY_WIND,
                                               ActivitySeineDto.PROPERTY_DETECTION_MODE,
                                               ActivitySeineDto.PROPERTY_REASON_FOR_NO_FISHING).build();

    private static final long serialVersionUID = 1L;

    protected boolean generalTabValid;

    protected boolean measurementsTabValid;
    protected boolean observedSystemTabValid;

    public ActivitySeineUIModel() {
        super(ActivitySeineDto.class);
        getBean().addPropertyChangeListener(ActivitySeineDto.PROPERTY_VESSEL_ACTIVITY_SEINE, evt -> {
            ReferentialReference<VesselActivitySeineDto> oldActivitySeine = (ReferentialReference<VesselActivitySeineDto>) evt.getOldValue();
            ReferentialReference<VesselActivitySeineDto> newActivitySeine = (ReferentialReference<VesselActivitySeineDto>) evt.getNewValue();
            {
                boolean oldValue = VesselActivitySeineHelper.isSetOperation(oldActivitySeine);
                boolean newValue = VesselActivitySeineHelper.isSetOperation(newActivitySeine);
                firePropertyChange(PROPERTY_SET_OPERATION, oldValue, newValue);
            }
            {
                boolean oldValue = VesselActivitySeineHelper.isChangedZoneOperation(oldActivitySeine);
                boolean newValue = VesselActivitySeineHelper.isChangedZoneOperation(newActivitySeine);
                firePropertyChange(PROPERTY_CHANGED_ZONE_OPERATION, oldValue, newValue);
            }
        });

    }

    public boolean isMeasurementsTabValid() {
        return measurementsTabValid;
    }

    public void setMeasurementsTabValid(boolean measurementsTabValid) {
        Object oldValue = isMeasurementsTabValid();
        this.measurementsTabValid = measurementsTabValid;
        firePropertyChange(PROPERTY_MEASUREMENTS_TAB_VALID, oldValue, measurementsTabValid);
    }

    public boolean isGeneralTabValid() {
        return generalTabValid;
    }

    public void setGeneralTabValid(boolean generalTabValid) {
        Object oldValue = isGeneralTabValid();
        this.generalTabValid = generalTabValid;
        firePropertyChange(PROPERTY_GENERAL_TAB_VALID, oldValue, generalTabValid);
    }

    public boolean isObservedSystemTabValid() {
        return observedSystemTabValid;
    }

    public void setObservedSystemTabValid(boolean observedSystemTabValid) {
        Object oldValue = isObservedSystemTabValid();
        this.observedSystemTabValid = observedSystemTabValid;
        firePropertyChange(PROPERTY_OBSERVED_SYSTEM_TAB_VALID, oldValue, observedSystemTabValid);
    }

    public boolean isSetOperation() {
        return VesselActivitySeineHelper.isSetOperation(bean.getVesselActivitySeine());
    }

    public boolean isChangedZoneOperation() {
        return VesselActivitySeineHelper.isChangedZoneOperation(bean.getVesselActivitySeine());
    }

}
