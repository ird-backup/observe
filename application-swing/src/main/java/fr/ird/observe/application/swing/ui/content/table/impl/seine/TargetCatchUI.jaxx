<!--
  #%L
  ObServe :: Application Swing
  %%
  Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

<fr.ird.observe.application.swing.ui.content.table.ContentTableUI
  superGenericType='SetSeineTargetCatchDto, TargetCatchDto'
  contentTitle='{n("observe.common.targetCatch")}'
  saveNewEntryText='{n("observe.content.action.create.targetCatch")}'
  saveNewEntryTip='{n("observe.content.action.create.targetCatch.tip")}'>

  <style source="../../CommonTable.jcss"/>

  <import>
    fr.ird.observe.services.dto.CommentableDto
    fr.ird.observe.services.dto.seine.SetSeineTargetCatchDto
    fr.ird.observe.services.dto.seine.TargetCatchDto
    fr.ird.observe.services.dto.referential.ReferentialReference
    fr.ird.observe.services.dto.referential.SpeciesDto
    fr.ird.observe.services.dto.referential.seine.WeightCategoryDto
    fr.ird.observe.application.swing.ui.content.table.*

    jaxx.runtime.swing.editor.NumberEditor
    jaxx.runtime.swing.editor.bean.BeanComboBox

    java.awt.Dimension

    static fr.ird.observe.application.swing.ui.UIHelper.getStringValue
    static org.nuiton.i18n.I18n.n
  </import>

  <!-- handler -->
  <TargetCatchUIHandler id='handler' constructorParams='this'/>

  <!-- model -->
  <TargetCatchUIModel id='model' constructorParams='this'/>

  <!-- edit bean -->
  <SetSeineTargetCatchDto id='bean'/>

  <!-- table edit bean -->
  <TargetCatchDto id='tableEditBean'/>

  <!-- table model -->
  <ContentTableModel id='tableModel'/>

  <!-- le validateur de l'écran -->
  <BeanValidator id='validator'
                 beanClass='fr.ird.observe.services.dto.seine.SetSeineTargetCatchDto'
                 errorTableModel='{getErrorTableModel()}'
                 context='ui-update-targetCatch'>
    <!-- clef unique -->
    <field name="targetCatch" component="editorPanel"/>
  </BeanValidator>

  <!-- le validateur d'une entrée de tableau -->
  <BeanValidator id='validatorTable'
                 autoField='true'
                 beanClass='fr.ird.observe.services.dto.seine.TargetCatchDto'
                 errorTableModel='{getErrorTableModel()}'
                 context='ui-update-targetCatch'/>

  <Table id='editorPanel' fill='both' insets='1'
         onFocusGained='species.requestFocus()'>

    <!-- species -->
    <row>
      <cell>
        <JLabel id='speciesLabel'/>
      </cell>
      <cell weightx='1' anchor='east'>
        <BeanComboBox id='species' genericType='ReferentialReference&lt;SpeciesDto&gt;' _entityClass='SpeciesDto.class' constructorParams='this'/>

      </cell>
    </row>

    <!-- categorie species thon -->
    <row>
      <cell>
        <JLabel id='weightCategoryLabel'/>
      </cell>
      <cell weightx='1' anchor='east'>
        <BeanComboBox id='weightCategory' genericType='ReferentialReference&lt;WeightCategoryDto&gt;' _entityClass='WeightCategoryDto.class' constructorParams='this'/>
      </cell>
    </row>

    <!-- weight -->
    <row>
      <cell>
        <JLabel id='catchWeightLabel'/>
      </cell>
      <cell weightx='1' anchor='east'>
        <NumberEditor id='catchWeight' constructorParams='this'/>
      </cell>
    </row>

    <!-- well -->
    <row>
      <cell>
        <JLabel id='wellLabel'/>
      </cell>
      <cell weightx='1' anchor='east'>
        <JPanel layout='{new BorderLayout()}'>
          <JToolBar id='wellToolbar' constraints='BorderLayout.WEST'>
            <JButton id='resetWell' constraints='BorderLayout.WEST' styleClass='resetButton'/>
          </JToolBar>
          <JTextField id='well' constraints='BorderLayout.CENTER'/>
        </JPanel>
      </cell>
    </row>

    <!-- comment -->
    <row>
      <cell columns='2' weighty='1'>
        <JScrollPane id='comment'
                     onFocusGained='comment2.requestFocus()'>
          <JTextArea id='comment2'
                     onKeyReleased='getTableEditBean().setComment(((JTextArea) event.getSource()).getText())'/>
        </JScrollPane>
      </cell>
    </row>
  </Table>

</fr.ird.observe.application.swing.ui.content.table.ContentTableUI>
