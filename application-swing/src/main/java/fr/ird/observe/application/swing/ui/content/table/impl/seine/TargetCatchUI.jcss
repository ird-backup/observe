/*
 * #%L
 * ObServe :: Swing
 * %%
 * Copyright (C) 2008 - 2010 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

#tableModel {
  deleteExtraMessage:{t("observe.content.targetCatch.table.deleteExtraMessage")};
}

#speciesLabel {
  text:"observe.content.targetCatch.table.speciesThon";
  toolTipText:"observe.content.targetCatch.table.speciesThon.tip";
  labelFor:{species};
}

#species {
  property:"species";
  selectedItem:{tableEditBean.getSpecies()};
  enabled:{!tableModel.isEditable() || !model.isRowSaved()};
}

#weightCategoryLabel {
  text:"observe.common.weightCategory";
  toolTipText:"observe.content.targetCatch.table.weightCategory.tip";
  labelFor:{weightCategory};
}

#weightCategory {
  property:{TargetCatchDto.PROPERTY_WEIGHT_CATEGORY};
  selectedItem:{tableEditBean.getWeightCategory()};
  enabled:{!tableModel.isEditable() || !model.isRowSaved()};
}

#catchWeightLabel {
  text:"observe.common.catchWeight";
  labelFor:{catchWeight};
}

#catchWeight {
  property:{TargetCatchDto.PROPERTY_CATCH_WEIGHT};
  model:{tableEditBean.getCatchWeight()};
  useFloat:true;
  numberPattern:{fr.ird.observe.application.swing.ui.UIHelper.DECIMAL4_PATTERN};
}

#wellLabel {
  text:"observe.common.well";
  toolTipText:"observe.content.targetCatch.table.well.tip";
  labelFor:{well};
}

#resetWell {
  toolTipText:"observe.content.action.reset.well.tip";
  enabled:{!tableModel.isEditable() || !model.isRowSaved()};
  _resetTablePropertyName:{TargetCatchDto.PROPERTY_WELL};
}

#well {
  text:{getStringValue(tableEditBean.getWell())};
  enabled:{!tableModel.isEditable() || !model.isRowSaved()};
  _tablePropertyName:{TargetCatchDto.PROPERTY_WELL};
}

#comment {
  columnHeaderView:{new JLabel(t("observe.common.comment.targetCatch"))};
  minimumSize:{new Dimension(10,80)};
}

#comment2 {
  text:{getStringValue(tableEditBean.getComment())};
}
