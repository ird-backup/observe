/*
 * #%L
 * ObServe :: Swing
 * %%
 * Copyright (C) 2008 - 2010 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

.dbMode {
  buttonGroup:"dbMode";
}

.creationMode {
  buttonGroup:"creationMode";
}

#content {
  layout:{new BorderLayout()};
}

#useLocalMode {
  value:{DbMode.USE_LOCAL};
  text:{I18nEnumHelper.getLabel(DbMode.USE_LOCAL)};
  enabled:{model.isCanUseLocalService()};
  selected:{model.getDbMode() ==  DbMode.USE_LOCAL};
}

#createLocalMode {
  value:{DbMode.CREATE_LOCAL};
  text:{I18nEnumHelper.getLabel(DbMode.CREATE_LOCAL)};
  enabled:{model.isCanCreateLocalService()};
  selected:{model.getDbMode() ==  DbMode.CREATE_LOCAL};
}

#useRemoteMode {
  value:{DbMode.USE_REMOTE};
  text:{I18nEnumHelper.getLabel(DbMode.USE_REMOTE)};
  enabled:{model.isCanUseRemoteService()};
  selected:{model.getDbMode() ==  DbMode.USE_REMOTE};
}

#useServerMode {
  value:{DbMode.USE_SERVER};
  text:{I18nEnumHelper.getLabel(DbMode.USE_SERVER)};
  enabled:{model.isCanUseServerService()};
  selected:{model.getDbMode() ==  DbMode.USE_SERVER};
}

#dbModeContent {
  layout:{new GridLayout(0,1)};
  border:{new TitledBorder(t("observe.storage.step.dbMode.detail"))};
}

#creationModeLayout {
   selected:{updateCreationModeContent(model.getDbMode())};
}

#creationModeContent {
  layout:{creationModeLayout};
  border:{new TitledBorder(t("observe.storage.step.creationMode"))};
}

#useCreateMode {
  layout:{new GridLayout(0,1)};
}

#noCreateMode {
  text:"observe.storage.report.no.create.mode";
}

#importInternalDumpMode {
  value:{CreationMode.IMPORT_INTERNAL_DUMP};
  text:{getHandler().updateInternalDumpModeLabel(ChooseDbModeUI.this, config.isInitialDumpExist())};
  visible:{getHandler().updateCreationModeLayout(ChooseDbModeUI.this, config.isInitialDumpExist(), importInternalDumpMode)};
  selected:{model.getCreationMode() ==  CreationMode.IMPORT_INTERNAL_DUMP};
}

#importExternalDumpMode {
  value:{CreationMode.IMPORT_EXTERNAL_DUMP};
  text:{I18nEnumHelper.getLabel(CreationMode.IMPORT_EXTERNAL_DUMP)};
  selected:{model.getCreationMode() ==  CreationMode.IMPORT_EXTERNAL_DUMP};
}

/** non utilisé dans cette version d'ObServe */

#importLocalStorageMode {
  visible:{getHandler().updateCreationModeLayout(ChooseDbModeUI.this, false, importLocalStorageMode)};
  value:{CreationMode.IMPORT_LOCAL_STORAGE};
  text:{I18nEnumHelper.getLabel(CreationMode.IMPORT_LOCAL_STORAGE)};
  selected:{model.getCreationMode() ==  CreationMode.IMPORT_LOCAL_STORAGE};
}

#importRemoteStorageMode {
  value:{CreationMode.IMPORT_REMOTE_STORAGE};
  text:{I18nEnumHelper.getLabel(CreationMode.IMPORT_REMOTE_STORAGE)};
  selected:{model.getCreationMode() ==  CreationMode.IMPORT_REMOTE_STORAGE};
}

#importServerStorageMode {
  value:{CreationMode.IMPORT_SERVER_STORAGE};
  text:{I18nEnumHelper.getLabel(CreationMode.IMPORT_SERVER_STORAGE)};
  selected:{model.getCreationMode() ==  CreationMode.IMPORT_SERVER_STORAGE};
}

#migrationContent {
  layout:{new GridLayout(0,1)};
  border:{new TitledBorder(t("observe.storage.report.action.migrate"))};
}

#migrationPolicy {
   text:{updateMigrationPolicy(model.isCanMigrate())};
}

#showMigrationSql {
   visible:{model.isCanMigrate()};
   text:"observe.storage.showMigrationSql";
   selected:{model.isShowMigrationSql()};
}

#showMigrationProgression {
   visible:{model.isCanMigrate()};
   text:"observe.storage.showMigrationProgression";
   selected:{model.isShowMigrationProgression()};
}

#resume {
   editable:false;
   focusable:false;
   contentType:"text/html";
   font-size:11;
}

#resumePane {
  columnHeaderView:{new JLabel(t("observe.common.resume"), UIHelper.getUIManagerActionIcon("information"), 10)};
}

