<!--
  #%L
  ObServe :: Application Swing
  %%
  Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

<fr.ird.observe.application.swing.ui.content.table.ContentTableUI superGenericType='SetLonglineCatchDto, CatchLonglineDto'
                                                contentTitle='{n("observe.content.catchLongline.title")}'
                                                newEntryText='{n("observe.content.catchLongline.action.new")}'
                                                newEntryTip='{n("observe.content.catchLongline.action.new.tip")}'
                                                saveNewEntryText='{n("observe.content.catchLongline.action.create")}'
                                                saveNewEntryTip='{n("observe.content.catchLongline.action.create.tip")}'>

  <style source="../../CommonTable.jcss"/>

  <import>
    fr.ird.observe.services.dto.DataReference
    fr.ird.observe.services.dto.CommentableDto
    fr.ird.observe.services.dto.longline.BasketDto
    fr.ird.observe.services.dto.longline.BranchlineDto
    fr.ird.observe.services.dto.longline.CatchLonglineDto
    fr.ird.observe.services.dto.longline.SectionDto
    fr.ird.observe.services.dto.longline.SetLonglineCatchDto
    fr.ird.observe.services.dto.referential.ReferentialReference
    fr.ird.observe.services.dto.referential.SpeciesDto
    fr.ird.observe.services.dto.referential.SexDto
    fr.ird.observe.services.dto.referential.longline.BaitHaulingStatusDto
    fr.ird.observe.services.dto.referential.longline.CatchFateLonglineDto
    fr.ird.observe.services.dto.referential.longline.HealthnessDto
    fr.ird.observe.services.dto.referential.longline.HookPositionDto
    fr.ird.observe.services.dto.referential.longline.MaturityStatusDto
    fr.ird.observe.services.dto.referential.longline.StomacFullnessDto
    fr.ird.observe.application.swing.ui.content.table.*
    fr.ird.observe.application.swing.ui.util.BooleanEditor

    jaxx.runtime.swing.editor.NumberEditor
    jaxx.runtime.swing.editor.bean.BeanComboBox
    org.nuiton.jaxx.widgets.select.FilterableDoubleList
    org.nuiton.jaxx.widgets.datetime.DateTimeEditor
    org.nuiton.jaxx.widgets.datetime.TimeEditor

    java.awt.Dimension

    java.util.Collection

    org.apache.commons.lang3.BooleanUtils

    static fr.ird.observe.application.swing.ui.UIHelper.getStringValue
    static org.nuiton.i18n.I18n.n
  </import>

  <!-- handler -->
  <CatchLonglineUIHandler id='handler'/>

  <!-- model -->
  <CatchLonglineUIModel id='model'/>

  <SizeMeasuresTableModel id='sizeMeasuresTableModel' initializer="getModel().getSizeMeasuresTableModel()"/>

  <WeightMeasuresTableModel id='weightMeasuresTableModel' initializer="getModel().getWeightMeasuresTableModel()"/>

  <!-- edit bean -->
  <SetLonglineCatchDto id='bean'/>

  <!-- table edit bean -->
  <CatchLonglineDto id='tableEditBean'/>

  <!-- table model -->
  <ContentTableModel id='tableModel'/>

  <!-- edit branchline -->
  <BranchlineDto id='branchlineBean' initializer='new BranchlineDto()'/>

  <!-- le validateur de l'écran -->
  <BeanValidator id='validator' context='ui-update'
                 beanClass='fr.ird.observe.services.dto.longline.SetLonglineCatchDto'
                 errorTableModel='{getErrorTableModel()}'>
  </BeanValidator>

  <!-- le validateur d'une entrée de tableau -->
  <BeanValidator id='validatorTable' autoField='true' context='ui-update'
                 beanClass='fr.ird.observe.services.dto.longline.CatchLonglineDto'
                 errorTableModel='{getErrorTableModel()}'
                 parentValidator='{validator}'/>

  <!-- le validateur de la branchline -->
  <BeanValidator id='branchlineValidator' context='ui-update-catchLongline'
                 beanClass='fr.ird.observe.services.dto.longline.BranchlineDto'
                 errorTableModel='{getErrorTableModel()}'
                 parentValidator='{validator}'>
    <field name='depthRecorder' component='depthRecorder'/>
    <field name='hookLost' component='hookLost'/>
    <field name='traceCutOff' component='traceCutOff'/>
    <field name='timer' component='timer'/>
    <field name='timeSinceContact' component='timeSinceContact'/>
    <field name='timerTimeOnBoard' component='timerTimeOnBoard'/>
    <field name='baitHaulingStatus' component='baitHaulingStatus'/>

  </BeanValidator>

  <JPopupMenu id='sizeMeasuresTablePopup'>
    <JMenuItem id='addSizeMeasure' onActionPerformed='getHandler().addSizeMeasure()'/>
    <JMenuItem id='deleteSelectedSizeMeasure' onActionPerformed='getHandler().deleteSelectedSizeMeasure()'/>
  </JPopupMenu>

  <JPopupMenu id='weightMeasuresTablePopup'>
    <JMenuItem id='addWeightMeasure' onActionPerformed='getHandler().addWeightMeasure()'/>
    <JMenuItem id='deleteSelectedWeightMeasure' onActionPerformed='getHandler().deleteSelectedWeightMeasure()'/>
  </JPopupMenu>

  <ButtonGroup id='acquisitionModeGroup'
               onStateChanged='getHandler().updateCatchAcquisitionMode((CatchAcquisitionModeEnum) acquisitionModeGroup.getSelectedValue())'/>

  <Table id='editorPanel' fill='both' insets='0'>
    <row>
      <cell weightx="1" weighty="1">
        <JTabbedPane id='fishingOperationTabPane'>
          <tab id='caracteristicTab'>
            <Table id='editForm' fill='both' insets='1'>

              <row>
                <cell columns="4">
                  <JPanel layout="{new GridLayout()}">

                    <!-- acquisition Mode -->
                    <JPanel id='acquisitionModePanel'>
                      <JRadioButton id='acquisitionModeIndividual'/>
                      <JRadioButton id='acquisitionModeGrouped'/>
                    </JPanel>

                    <!-- location on longline -->
                    <Table id="locationOnLonglinePanel">
                      <row>
                        <cell anchor="west">
                          <JLabel id='sectionLabel'/>
                        </cell>
                        <cell fill="both" weightx="1">
                          <BeanComboBox id='section' genericType='DataReference&lt;SectionDto&gt;' _entityClass='SectionDto.class' constructorParams='this'/>
                        </cell>
                      </row>
                      <row>
                        <cell anchor="west">
                          <JLabel id='basketLabel'/>
                        </cell>
                        <cell fill="both" weightx="1">
                          <BeanComboBox id='basket' genericType='DataReference&lt;BasketDto&gt;' _entityClass='BasketDto.class' constructorParams='this'/>
                        </cell>
                      </row>
                      <row>
                        <cell anchor="west">
                          <JLabel id='branchlineLabel'/>
                        </cell>
                        <cell fill="both" weightx="1">
                          <BeanComboBox id='branchline' genericType='DataReference&lt;BranchlineDto&gt;' _entityClass='BranchlineDto.class' constructorParams='this'/>
                        </cell>
                      </row>
                    </Table>
                  </JPanel>
                </cell>
              </row>

              <!-- speciesCatch -->
              <row>
                <cell>
                  <JLabel id='speciesCatchLabel'/>
                </cell>
                <cell weightx='1' anchor='east' columns="3">
                  <BeanComboBox id='speciesCatch' genericType='ReferentialReference&lt;SpeciesDto&gt;' _entityClass='SpeciesDto.class' constructorParams='this'/>
                </cell>

              </row>

              <!-- count -->
              <!-- total weight -->
              <row>
                <cell>
                  <JLabel id='countLabel'/>
                </cell>
                <cell weightx='1' anchor='east'>
                  <NumberEditor id='count' constructorParams='this'/>
                </cell>
                <cell>
                  <JLabel id='totalWeightLabel'/>
                </cell>
                <cell weightx='1' anchor='east'>
                  <NumberEditor id='totalWeight' constructorParams='this'/>
                </cell>
              </row>

              <!-- catch healthness -->
              <!-- hook position -->
              <row>
                <cell>
                  <JLabel id='catchHealthnessLabel'/>
                </cell>
                <cell weightx='1' anchor='east'>
                  <BeanComboBox id='catchHealthness' constructorParams='this' genericType='ReferentialReference&lt;HealthnessDto&gt;' _entityClass='HealthnessDto.class'/>
                </cell>
                <cell>
                  <JLabel id='hookPositionLabel'/>
                </cell>
                <cell weightx='1' anchor='east'>
                  <BeanComboBox id='hookPosition' constructorParams='this' genericType='ReferentialReference&lt;HookPositionDto&gt;' _entityClass='HookPositionDto.class'/>
                </cell>
              </row>

              <!-- catch fate -->
              <!-- discard healthness -->
              <row>
                <cell>
                  <JLabel id='catchFateLonglineLabel'/>
                </cell>
                <cell weightx='1' anchor='east'>
                  <BeanComboBox id='catchFateLongline' constructorParams='this' genericType='ReferentialReference&lt;CatchFateLonglineDto&gt;' _entityClass='CatchFateLonglineDto.class'/>
                </cell>
                <cell>
                  <JLabel id='discardHealthnessLabel'/>
                </cell>
                <cell weightx='1' anchor='east'>
                  <BeanComboBox id='discardHealthness' constructorParams='this' genericType='ReferentialReference&lt;HealthnessDto&gt;' _entityClass='HealthnessDto.class'/>
                </cell>
              </row>

              <!-- hookWhenDiscarded -->
              <!-- photoReferences -->
              <row>
                <cell anchor="west">
                  <JLabel id='hookWhenDiscardedLabel'/>
                </cell>
                <cell anchor='west' fill="both">
                  <BooleanEditor id='hookWhenDiscarded'/>
                </cell>
                <cell anchor='west'>
                  <JLabel id='photoReferencesLabel'/>
                </cell>
                <cell anchor='east' weightx="1" fill="both">
                  <JPanel layout='{new BorderLayout()}'>
                    <JToolBar id='photoReferencesToolbar' constraints='BorderLayout.WEST'>
                      <JButton id='resetPhotoReferences' styleClass='resetButton'/>
                    </JToolBar>
                    <JTextField id='photoReferences' constraints='BorderLayout.CENTER'/>
                  </JPanel>
                </cell>
              </row>

            </Table>
          </tab>

          <tab id='depredatedFormTab'>

            <Table id='depredatedForm' fill='both' insets='1'>

              <!-- depredated -->
              <row>
                <cell anchor='west' fill="both">
                  <JCheckBox id='depredated'/>
                </cell>
              </row>

              <!-- beatDiameter -->
              <row>
                <cell>
                  <JLabel id='beatDiameterLabel'/>
                </cell>
                <cell weightx='1' anchor='east'>
                  <NumberEditor id='beatDiameter' constructorParams='this'/>
                </cell>
              </row>

              <!-- predator -->
              <row>
                <cell columns="2" fill="both" weighty="0.7">
                  <JScrollPane id='predatorPane'>
                    <FilterableDoubleList id='predator' genericType='ReferentialReference&lt;SpeciesDto&gt;' _entityClass='SpeciesDto.class'/>
                  </JScrollPane>
                </cell>
              </row>

            </Table>
          </tab>

          <tab id='foodAndSexualFormTab'>

            <JPanel layout="{new BorderLayout()}">
              <Table id='foodAndSexualForm' fill='both' insets='1' constraints='BorderLayout.NORTH'>

                <!-- stomac fullness -->
                <row>
                  <cell>
                    <JLabel id='stomacFullnessLabel'/>
                  </cell>
                  <cell weightx='1' anchor='east'>
                    <BeanComboBox id='stomacFullness' constructorParams='this' genericType='ReferentialReference&lt;StomacFullnessDto&gt;' _entityClass='StomacFullnessDto.class'/>
                  </cell>
                </row>

                <!-- sex -->
                <row>
                  <cell>
                    <JLabel id='sexLabel'/>
                  </cell>
                  <cell weightx='1' anchor='east'>
                    <BeanComboBox id='sex' constructorParams='this' genericType='ReferentialReference&lt;SexDto&gt;' _entityClass='SexDto.class'/>
                  </cell>
                </row>

                <!-- maturity status -->
                <row>
                  <cell>
                    <JLabel id='maturityStatusLabel'/>
                  </cell>
                  <cell weightx='1' anchor='east'>
                    <BeanComboBox id='maturityStatus' constructorParams='this' genericType='ReferentialReference&lt;MaturityStatusDto&gt;' _entityClass='MaturityStatusDto.class'/>
                  </cell>
                </row>

                <!-- gonadeWeight -->
                <row>
                  <cell>
                    <JLabel id='gonadeWeightLabel'/>
                  </cell>
                  <cell weightx='1' anchor='east'>
                    <NumberEditor id='gonadeWeight' constructorParams='this'/>
                  </cell>
                </row>

              </Table>
            </JPanel>

          </tab>

          <tab id='sizeMeasuresFormTab'>

            <JScrollPane id='sizeMeasuresScrollPane'>
              <JTable id='sizeMeasuresTable'/>
            </JScrollPane>

          </tab>

          <tab id='weightMeasuresFormTab'>

            <JScrollPane id='weightMeasuresScrollPane'>
              <JTable id='weightMeasuresTable'/>
            </JScrollPane>

          </tab>

          <tab id='branchlineFormTab'>

            <JPanel layout="{new BorderLayout()}">
              <Table id='branchlineForm' fill='both' insets='3' constraints='BorderLayout.NORTH'>

                <!-- depthRecorder -->
                <!-- hookLost -->
                <!-- traceCutOff -->
                <row>
                  <cell columns="3">
                    <JPanel layout="{new GridLayout()}">
                      <JCheckBox id='depthRecorder'/>
                      <JCheckBox id='hookLost'/>
                      <JCheckBox id='traceCutOff'/>
                    </JPanel>
                  </cell>
                </row>

                <!-- timer -->
                <!-- timeSinceContact -->
                <row>
                  <cell anchor='east'>
                    <JCheckBox id='timer'/>
                  </cell>
                  <cell>
                    <JLabel id='timeSinceContactLabel'/>
                  </cell>
                  <cell>
                    <TimeEditor id='timeSinceContact' constructorParams='this'/>
                  </cell>
                </row>

                <!-- timerTimeOnBoard -->
                <row>
                  <cell columns="3">
                    <DateTimeEditor id='timerTimeOnBoard' constructorParams='this'/>
                  </cell>
                </row>

                <!-- baitHaulingStatus -->
                <row>
                  <cell anchor='west'>
                    <JLabel id='baitHaulingStatusLabel'/>
                  </cell>
                  <cell anchor='east' weightx="1" fill="both" columns="2">
                    <BeanComboBox id='baitHaulingStatus' constructorParams='this' genericType='ReferentialReference&lt;BaitHaulingStatusDto&gt;' _entityClass='BaitHaulingStatusDto.class'/>
                  </cell>
                </row>

                <row>
                  <cell columns="3">
                    <JPanel id='branchlineActions' layout="{new GridLayout()}">
                      <JButton id='resetBranchline' onActionPerformed='getHandler().resetBranchline()'/>
                      <JButton id='saveBranchline' onActionPerformed='getHandler().saveBranchline()'/>
                    </JPanel>
                  </cell>
                </row>

              </Table>
            </JPanel>

          </tab>

        </JTabbedPane>
      </cell>
    </row>
    <row>
      <cell weighty='1'>
        <JScrollPane id='comment' onFocusGained='comment2.requestFocus()'>
          <JTextArea id='comment2'/>
        </JScrollPane>
      </cell>
    </row>
  </Table>

</fr.ird.observe.application.swing.ui.content.table.ContentTableUI>
