package fr.ird.observe.application.swing.ui.content.ref.usage;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.decoration.DecoratorService;
import fr.ird.observe.application.swing.decoration.ObserveI18nDecoratorHelper;
import fr.ird.observe.services.dto.AbstractReference;
import fr.ird.observe.services.dto.DataDto;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.IdDto;
import fr.ird.observe.services.dto.ReferenceMap;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import jaxx.runtime.JAXXObject;
import jaxx.runtime.spi.UIHandler;
import jaxx.runtime.swing.editor.bean.BeanComboBox;
import org.apache.commons.lang3.BooleanUtils;
import org.nuiton.decorator.Decorator;
import org.nuiton.decorator.JXPathDecorator;

import javax.swing.DefaultListSelectionModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Created on 16/12/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.1
 */
public abstract class UsageUIHandlerSupport<U extends JAXXObject> implements UIHandler<U> {

    U ui;

    protected abstract JLabel getMessage();

    protected abstract JPanel getUsages();

    protected abstract BeanComboBox<?> getReplace();

    @Override
    public void beforeInit(U ui) {
        this.ui = ui;
    }

    @Override
    public void afterInit(U ui) {

        String message = ui.getContextValue(String.class);
        getMessage().setText(message);
        ReferenceMap usages = ui.getContextValue(ReferenceMap.class);
        if (usages.isEmpty()) {
            getUsages().add(new JLabel(t("observe.message.no.usage.for.entity")));
        } else {

            for (Map.Entry<Class<? extends IdDto>, Set<? extends AbstractReference>> entry : usages.entrySet()) {
                Class dtoType = entry.getKey();
                Set references = entry.getValue();
                String typeTitle = t(ObserveI18nDecoratorHelper.getTypeI18nKey(dtoType));
                if (DataDto.class.isAssignableFrom(dtoType)) {
                    addDataReferenceUsages(dtoType, references, typeTitle);
                } else {
                    addReferentialReferenceUsages(dtoType, references, typeTitle);
                }

            }

        }

        BeanComboBox<?> comboBox = getReplace();
        if (comboBox != null) {
            List<AbstractReference> references = ui.getContextValue(List.class);
            AbstractReference reference = references.get(0);
            Class type = reference.getType();
            comboBox.setBeanType(type);
            comboBox.setI18nPrefix("observe.common.");
            DecoratorService decoratorService = ObserveSwingApplicationContext.get().getDecoratorService();
            Decorator referenceDecorator = decoratorService.getReferenceDecorator(type);

            comboBox.init((JXPathDecorator) referenceDecorator, (List) references);
        }
    }

    public void attachToOptionPane(JOptionPane pane, String message) {
        JButton jButton = findButton(pane, message);
        Objects.requireNonNull(jButton);
        jButton.setEnabled(false);
        ui.addPropertyChangeListener("canApply", evt -> {
            Boolean newValue = (Boolean) evt.getNewValue();
            jButton.setEnabled(BooleanUtils.isTrue(newValue));
        });
    }

    protected <D extends DataDto> void addDataReferenceUsages(Class<D> dtoType,
                                                              Set<DataReference<D>> references,
                                                              String typeTitle) {

        String typetitle = n("observe.content.label.usage.data.title");
        typetitle = t(typetitle, typeTitle, references.size());

        Decorator<?> decorator = ObserveSwingApplicationContext.get().getDecoratorService().getDataReferenceDecorator(dtoType);
        Objects.requireNonNull(decorator, "could not find decorator for type " + dtoType);

        buildUsagePanel(decorator, references, typetitle);
    }


    protected <D extends ReferentialDto> void addReferentialReferenceUsages(Class<D> dtoType,
                                                                            Set<ReferentialReference<D>> references,
                                                                            String typeTitle) {

        String typetitle = n("observe.content.label.usage.referentiel.title");
        typetitle = t(typetitle, typeTitle, references.size());

        Decorator<?> decorator = ObserveSwingApplicationContext.get().getDecoratorService().getReferentialReferenceDecorator(dtoType);
        Objects.requireNonNull(decorator, "could not find decorator for type " + dtoType);

        buildUsagePanel(decorator, references, typetitle);
    }

    protected <D extends IdDto, R extends AbstractReference<D>> void buildUsagePanel(Decorator<?> decorator, Set<R> references, String typetitle) {

        List<String> data = new ArrayList<>(references.size());
        data.addAll(references.stream().map(decorator::toString).collect(Collectors.toList()));

        JList<? super String> l = new JList<>(data.toArray());
        l.setSelectionModel(new DisabledItemSelectionModel());
        JScrollPane pane = new JScrollPane();
        pane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        pane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        pane.setMinimumSize(new Dimension(300, 30));
        pane.setColumnHeaderView(new JLabel(typetitle));
        pane.setViewportView(l);
        getUsages().add(pane);
    }

    private class DisabledItemSelectionModel extends DefaultListSelectionModel {

        private static final long serialVersionUID = 1L;

        DisabledItemSelectionModel() {
            setSelectionMode(SINGLE_SELECTION);
        }

        @Override
        public void setSelectionInterval(int index0, int index1) {
            super.setSelectionInterval(-1, -1);
        }

    }

    protected JButton findButton(Container c, String text) {

        for (Component component : c.getComponents()) {
            if (component instanceof JButton) {
                if (text.equals(((JButton) component).getText())) {
                    return (JButton) component;
                }
                continue;
            }
            if (component instanceof Container) {
                JButton button = findButton((Container) component, text);
                if (button != null) {
                    return button;
                }
            }
        }
        return null;
    }

}
