package fr.ird.observe.application.swing.backup;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.StringUtil;

import java.io.File;
import java.util.Date;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 19/09/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class LocalDatabaseBackupTaskSupport implements Runnable {

    /** Logger. */
    private static final Log log = LogFactory.getLog(LocalDatabaseBackupTaskSupport.class);

    protected File doBackup(ObserveSwingApplicationContext applicationContext, ObserveSwingDataSource mainDataSource) {
        File file = applicationContext.getConfig().newBackupDataFile();
        String startMessage = t("observe.backup.start", new Date());

        ObserveMainUI mainUI = applicationContext.getMainUI();
        mainUI.getStatus().setStatus(startMessage);
        if (log.isInfoEnabled()) {
            log.info(startMessage + " - " + file);
        }
        long t0 = System.nanoTime();
        mainUI.setBusy(true);
        try {
            mainDataSource.newDataSourceService().backup(file);
        } finally {
            String endMessage = t("observe.backup.done", new Date(), StringUtil.convertTime(System.nanoTime() - t0));
            mainUI.getStatus().setStatus(endMessage);
            if (log.isInfoEnabled()) {
                log.info(endMessage + " - " + file);
            }
            mainUI.setBusy(false);
        }
        return file;
    }
}
