package fr.ird.observe.application.swing.ui.tree.node;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import fr.ird.observe.services.service.seine.TripSeineService;

/**
 * Created on 4/9/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 4.0
 */
public class TripSeineNode extends DataReferenceNodeSupport<TripSeineDto> {

    private static final long serialVersionUID = 1L;

    public TripSeineNode(DataReference<TripSeineDto> entity) {
        super(TripSeineDto.class, entity);
    }

    private int initialRouteCount;

    @Override
    protected DataReference<TripSeineDto> fetchEntity() {
        TripSeineService tripSeineService = ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newTripSeineService();
        return tripSeineService.loadReferenceToRead(getId());
    }

    public void setInitialRouteCount(int initialRouteCount) {
        this.initialRouteCount = initialRouteCount;
    }

    public int getRouteCount() {
        if (entity == null) {
            return initialRouteCount;
        }
        return (int) entity.getPropertyValue(TripSeineDto.PROPERTY_ROUTE_COUNT);
    }
}
