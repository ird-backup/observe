/*
 * #%L
 * ObServe :: Swing
 * %%
 * Copyright (C) 2008 - 2010 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

NumberEditor {
  bean:{bean};
  useFloat:true;
  numberPattern:{fr.ird.observe.application.swing.ui.UIHelper.DECIMAL2_PATTERN};
}

#model {
  editable:true;
  modified:{validator.isChanged()};
  valid:{validator.isValid()};
}

#dateLabel {
  text:"observe.common.date";
  labelFor:{date};
}

#date {
  date:{bean.getDate()};
  _propertyName:{RouteDto.PROPERTY_DATE};
}

#startLogValueLabel {
  text:"observe.common.startLogValue";
  labelFor:{startLogValue};
}

#startLogValue {
  property:{RouteDto.PROPERTY_START_LOG_VALUE};
  model:{bean.getStartLogValue()};
}

#endLogValueLabel {
  text:"observe.common.endLogValue";
  labelFor:{endLogValue};
}

#endLogValue {
  property:{RouteDto.PROPERTY_END_LOG_VALUE};
  model:{bean.getEndLogValue()};
  enabled:{!model.isCreatingMode()};
}

#comment {
  columnHeaderView:{new JLabel(t("observe.common.comment"))};
  minimumSize:{new Dimension(10,50)};
}

#comment2 {
  text:{getStringValue(bean.getComment())};
}

#reopen {
  _toolTipText:{t("observe.content.action.reopen.route.tip")};
}

#close {
  enabled:{!model.isModified() && (model.isHistoricalData() || model.isValid()) && !dataContext.isOpenActivity()};
  _toolTipText:{t("observe.action.close.route.tip")};
}

#closeAndCreate {
  enabled:{!model.isModified() && (model.isHistoricalData() || model.isValid()) && !dataContext.isOpenActivity()};
  _text:{t("observe.content.action.closeAndCreate.route")};
  _toolTipText:{t("observe.content.action.closeAndCreate.route.tip")};
}

#delete {
  _toolTipText:{t("observe.action.delete.route.tip")};
}
