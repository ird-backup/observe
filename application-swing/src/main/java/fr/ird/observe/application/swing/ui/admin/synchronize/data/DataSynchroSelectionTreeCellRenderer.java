package fr.ird.observe.application.swing.ui.admin.synchronize.data;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.tree.renderer.DataSelectionTreeCellRenderer;
import fr.ird.observe.application.swing.ui.tree.node.ObserveNode;
import fr.ird.observe.application.swing.ui.tree.node.ReferentialReferenceNodeSupport;

import javax.swing.Icon;
import javax.swing.JTree;
import javax.swing.tree.TreeModel;
import java.util.Enumeration;

/**
 * Created by tchemit on 04/10/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class DataSynchroSelectionTreeCellRenderer extends DataSelectionTreeCellRenderer {

    private final Icon newTripIcon = UIHelper.createActionIcon("synchroAdd");
    private final Icon existingTripIcon = UIHelper.createActionIcon("synchroUpdate");
    private final JTree oppositeTree;

    private ImmutableSet<String> oppositeIds;

    public DataSynchroSelectionTreeCellRenderer(JTree oppositeTree) {
        this.oppositeTree = oppositeTree;
    }

    @Override
    public void setIcon(ObserveNode node) {
        if (node instanceof ReferentialReferenceNodeSupport<?> || !(oppositeTree.getModel().getRoot() instanceof ObserveNode)) {
            super.setIcon(node);
        } else {
            if (getOppositeIds().contains(node.getId())) {
                setDefaultIcons(existingTripIcon);
            } else {
                setDefaultIcons(newTripIcon);
            }
        }
    }

    private ImmutableSet<String> getOppositeIds() {
        if (oppositeIds == null) {
            ImmutableSet.Builder<String> builder = ImmutableSet.builder();
            TreeModel model = oppositeTree.getModel();
            ObserveNode rootNode = (ObserveNode) model.getRoot();
            Enumeration<ObserveNode> programNodes = rootNode.children();
            while (programNodes.hasMoreElements()) {
                ObserveNode programNode = programNodes.nextElement();
                Enumeration<ObserveNode> tripNodes = programNode.children();
                while (tripNodes.hasMoreElements()) {
                    ObserveNode tripNode = tripNodes.nextElement();
                    builder.add(tripNode.getId());
                }
            }
            oppositeIds = builder.build();
        }
        return oppositeIds;
    }
}
