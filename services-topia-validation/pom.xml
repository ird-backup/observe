<?xml version="1.0" encoding="UTF-8"?>
<!--
  #%L
  ObServe :: Business
  $HeadURL: https://svn.mpl.ird.fr/osiris/observe/trunk/observe-business/pom.xml $
  %%
  Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <parent>
    <groupId>fr.ird.observe</groupId>
    <artifactId>pom</artifactId>
    <version>5.3.5-SNAPSHOT</version>
  </parent>

  <artifactId>services-topia-validation</artifactId>

  <name>ObServe :: Services ToPIA Validation</name>
  <description>ObServe Services ToPIA Validation module</description>

  <properties>
    <maven.deploy.skip>true</maven.deploy.skip>
  </properties>

  <dependencies>

    <!-- sibling dependencies -->

    <dependency>
      <groupId>${project.groupId}</groupId>
      <artifactId>entities</artifactId>
      <version>${project.version}</version>
    </dependency>
    <dependency>
      <groupId>${project.groupId}</groupId>
      <artifactId>services</artifactId>
      <version>${project.version}</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>${project.groupId}</groupId>
      <artifactId>services-topia-validators</artifactId>
      <version>${project.version}</version>
      <scope>runtime</scope>
    </dependency>

    <!-- commons -->

    <dependency>
      <groupId>org.apache.commons</groupId>
      <artifactId>commons-lang3</artifactId>
    </dependency>

    <!-- Nuiton -->

    <dependency>
      <groupId>org.nuiton</groupId>
      <artifactId>nuiton-validator</artifactId>
    </dependency>
    <dependency>
      <groupId>org.nuiton</groupId>
      <artifactId>nuiton-utils</artifactId>
    </dependency>
    <dependency>
      <groupId>org.nuiton.i18n</groupId>
      <artifactId>nuiton-i18n</artifactId>
    </dependency>

    <!-- test  -->
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
    </dependency>
    <dependency>
      <groupId>org.slf4j</groupId>
      <artifactId>slf4j-jcl</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>log4j</groupId>
      <artifactId>log4j</artifactId>
      <scope>test</scope>
    </dependency>


    <dependency>
      <groupId>org.nuiton</groupId>
      <artifactId>nuiton-validator</artifactId>
      <classifier>tests</classifier>
      <scope>test</scope>
    </dependency>

  </dependencies>

  <build>
    <resources>

      <resource>
        <directory>src/main/resources</directory>
        <includes>
          <include>**/*</include>
        </includes>
      </resource>
    </resources>

    <plugins>

      <plugin>
        <groupId>${project.groupId}</groupId>
        <artifactId>toolbox-maven-plugin</artifactId>
        <version>${project.version}</version>
        <configuration>
          <validatorsFile>${project.basedir}/src/test/resources/validators.xml</validatorsFile>
        </configuration>
        <executions>
          <execution>
            <id>generate-i18n-validator-fields</id>
            <phase>generate-sources</phase>
            <goals>
              <goal>generate-i18n-validator-fields</goal>
            </goals>
            <configuration>
              <prefix>observe.common.</prefix>
            </configuration>
          </execution>
          <execution>
            <id>generate-validators-descriptor</id>
            <phase>generate-sources</phase>
            <goals>
              <goal>generate-validators-descriptor</goal>
            </goals>
          </execution>
        </executions>
        <dependencies>
          <dependency>
            <groupId>${project.groupId}</groupId>
            <artifactId>entities</artifactId>
            <version>${project.version}</version>
          </dependency>
          <dependency>
            <groupId>${project.groupId}</groupId>
            <artifactId>services-topia-validators</artifactId>
            <version>${project.version}</version>
          </dependency>
        </dependencies>
      </plugin>

      <plugin>
        <groupId>org.nuiton.i18n</groupId>
        <artifactId>i18n-maven-plugin</artifactId>
        <executions>
          <execution>
            <id>parse-java</id>
            <goals>
              <goal>parserJava</goal>
            </goals>
            <configuration>
              <treateDefaultEntry>false</treateDefaultEntry>
              <entries>
                <entry>
                  <specificGoal>parserJava</specificGoal>
                  <basedir>${project.build.directory}/generated-sources/java</basedir>
                </entry>
              </entries>
            </configuration>
          </execution>
          <execution>
            <id>parse-validation</id>
            <goals>
              <goal>parserValidation</goal>
            </goals>
            <configuration>
              <entries>
                <entry>
                  <basedir>${project.basedir}/src/main/resources/</basedir>
                  <includes>
                    <param>**/**-validation.xml</param>
                  </includes>
                </entry>
              </entries>
            </configuration>
          </execution>
          <execution>
            <id>gen</id>
            <goals>
              <goal>gen</goal>
            </goals>
          </execution>
        </executions>
      </plugin>

    </plugins>

  </build>
</project>
