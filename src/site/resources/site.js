/*-
 * #%L
 * ObServe
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
$(document).ready(function () {

    var metas = $('#mavenProjectProperties');

    if (metas.length == 0) {
        return;
    }

    $.debugOn = function () {
        $('#mavenProjectProperties').attr('DEBUG', true);
    };

    $.debugOff = function () {
        $('#mavenProjectProperties').attr('DEBUG', null);
    };

    var debug = function (metas) {
        return metas.attr('DEBUG');
    };

    var urlParameters = decodeURIComponent(window.location.search.slice(1))
        .split('&')
        .reduce(function _reduce(a, b) {
            b = b.split('=');
            a[b[0]] = b[1];
            return a;
        }, {});

    if (urlParameters.debug) {
        $.debugOn();
    }

    var projectId = metas.attr('projectId');
    var version = metas.attr('version');
    var locale = metas.attr('locale');

    var rootUrl= 'http://' + window.location.hostname ;
    var useDocUrl = false;
    var localeInPath = window.location.pathname.indexOf('/' + locale + '/') > -1;

    var DEBUG = debug(metas);

    if (DEBUG) {
        console.info("localeInPath: " + localeInPath);
    }

    var addVersion = function (metas) {

        var DEBUG = debug(metas);

        var versionsUrl = rootUrl + '/versions';

        $.get(versionsUrl)
            .fail(function () {
                console.error("can't load url: " + versionsUrl);
            })
            .done(function (allVersions) {

                if (DEBUG) {
                    console.info("Versions url: " + versionsUrl);
                    console.info("All versions: " + allVersions);
                }

                var labels;
                if ('en' == locale) {
                    labels = {develop: 'in progress', latest: 'latest release'};
                } else {
                    labels = {develop: 'en cours', latest: 'dernière stable'};
                }
                var versions = allVersions.split(' ');
                if (DEBUG) {
                    console.info("Versions : " + versions);
                    console.info("version  : " + version);
                }
                var versionsHtml = "<select id='versions' class='versions pull-left'>";
                $(versions).each(function (index, value) {
                    value = value.trim();
                    var label;
                    if ('develop' == value || 'latest' == value) {
                        label = labels[value];
                    } else {
                        label = value;
                    }

                    if (DEBUG) {
                        console.info("Version to add " + label + " (" + value + ")");
                    }
                    versionsHtml += "<option value='" + value + "'";
                    if (version == value) {
                        versionsHtml += " selected='selected'";
                    }
                    versionsHtml += ">Version " + label + "</option>";
                });
                versionsHtml += "</select><span class='divider'>»</span>";

                $('#breadcrumbs').find('> ul > li:nth-child(2)').html(versionsHtml);
                $('#versions').change(function () {
                    var DEBUG = debug(metas);
                    var newVersion = $(this).val();
                    var newUrl = rootUrl + '/' + newVersion;
                    if (localeInPath) {
                        newUrl += "/" + locale;
                    }
                    if (DEBUG) {
                        console.info("Change to version: " + newUrl);
                    }
                    window.location = newUrl;
                });

            });

    };

    var addScmwebeditor = function (metas) {

        var editorUrl = 'https://scmwebeditor.nuiton.org/scmwebeditor/edit.action';

        if ('develop' != version) {
            return;
        }

        var skipDefaultFiles = [
            "project-info.html",
            "team-list.html",
            "mail-lists.html",
            "integration.html",
            "issue-tracking.html",
            "license.html",
            "source-repository.html",
            "dependencies.html",
            "dependency-info.html",
            "dependency-convergence.html",
            "plugin-management.html",
            "plugins.html",
            "dependency-management.html",
            "project-summary.html",
            "project-reports.html",
            "dev-activity.html",
            "file-activity.html",
            "changelog.html",
            "surefire-report.html",
            "third-party-report.html",
            "sonar.html",
            "dependency-updates-report.html",
            "plugin-updates-report.html",
            "property-updates-report.html",
            "changes-report.html",
            "redmine-report.html",
            "application-config-report.html"
        ];

        var DEBUG = debug(metas);

        var scm = 'https://gitlab.com/ultreia.io/observe.git';

        if (DEBUG) {
            console.info('scm: ' + scm);
        }

        var sourcesType = metas.attr('sourcesType');

        var pathname = document.location.pathname;

        // remove prefix
        if (useDocUrl) {
            pathname = pathname.replace('/' + projectId + '/develop', '');
        } else {
            pathname = pathname.replace('/develop', '');
        }

        if (DEBUG) {
            console.info('pathname: ' + pathname);
        }

        var path;
        if (pathname.indexOf('target/site') > 0) {
            path = pathname.substr(pathname.indexOf('/site')). replace('/site/', '');
        } else {
            if (pathname == '/' || pathname == '') {
                // on / page
                path = 'index.html';
            } else {
                path = pathname;
            }
        }

        if (localeInPath) {

            // locale detected in path (means not default locale)
            // remove locale from path
            path = path.substr(('/' + locale).length);
        }

        if (DEBUG) {
            console.info('path: ' + path);
        }
        if ($.inArray(path, skipDefaultFiles) > -1) {
            // skip this file
            if (DEBUG) {
                console.info('skipDefaultFiles: ' + path);
            }
            return;
        }

        var skipFiles = metas.attr('scmwebeditor_skipFiles');
        if (skipFiles && skipFiles.indexOf(',' + path + ',') > -1) {
            // skip this file
            if (DEBUG) {
                console.info('skipFiles: ' + path);
            }
            return;
        }

        path = path.replace('.html', '');
        if (path.indexOf('/') == 0) {
            path = path.substr(1);
        }

        var vmFiles = metas.attr('scmwebeditor_vmFiles');

        // add vm on vmFiles
        if (vmFiles && vmFiles.indexOf(',' + path + ',') > -1) {
            if (DEBUG) {
                console.info("Found vm file: " + path);
            }
            path += '.' + sourcesType + '.vm';
        } else {
            if (DEBUG) {
                console.info("Not found vm file: " + path);
            }
            path += '.' + sourcesType;
        }

        if (DEBUG) {
            console.info('final path: ' + path);
        }

        var address = scm + "/src/site";
        if (localeInPath) {
            address += '/' + locale;
        }
        address += '/' + sourcesType + '/' + path;
        if (DEBUG) {
            console.info('address: ' + address);
        }
        var url = editorUrl + "?scmType=Git&selectedBranch=develop&repositoryRoot=" + scm + "&address=" + address;

        if (DEBUG) {
            console.info('edit url: ' + url);
        }

        $('#publishDate').prepend(
            "<li class='pull-right'><a target='edit' href='" + url +
            "'>Éditer la page</a></li>" +
            "<li class='divider pull-right'>|</li>"
        );

    };

    addScmwebeditor(metas);

    addVersion(metas);

});