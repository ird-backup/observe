package fr.ird.observe.services.dto.reference;

/*-
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ird.observe.services.dto.DataDto;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.longline.ActivityLonglineDto;
import fr.ird.observe.services.dto.longline.ActivityLonglineEncounterDto;
import fr.ird.observe.services.dto.longline.ActivityLonglineSensorUsedDto;
import fr.ird.observe.services.dto.longline.ActivityLonglineStubDto;
import fr.ird.observe.services.dto.longline.BaitsCompositionDto;
import fr.ird.observe.services.dto.longline.BasketDto;
import fr.ird.observe.services.dto.longline.BasketWithSectionIdDto;
import fr.ird.observe.services.dto.longline.BranchlineDto;
import fr.ird.observe.services.dto.longline.BranchlineWithBasketIdDto;
import fr.ird.observe.services.dto.longline.BranchlinesCompositionDto;
import fr.ird.observe.services.dto.longline.CatchLonglineDto;
import fr.ird.observe.services.dto.longline.EncounterDto;
import fr.ird.observe.services.dto.longline.FloatlinesCompositionDto;
import fr.ird.observe.services.dto.longline.GearUseFeaturesLonglineDto;
import fr.ird.observe.services.dto.longline.GearUseFeaturesMeasurementLonglineDto;
import fr.ird.observe.services.dto.longline.HooksCompositionDto;
import fr.ird.observe.services.dto.longline.SectionDto;
import fr.ird.observe.services.dto.longline.SensorUsedDto;
import fr.ird.observe.services.dto.longline.SetLonglineCatchDto;
import fr.ird.observe.services.dto.longline.SetLonglineDetailCompositionDto;
import fr.ird.observe.services.dto.longline.SetLonglineDto;
import fr.ird.observe.services.dto.longline.SetLonglineGlobalCompositionDto;
import fr.ird.observe.services.dto.longline.SetLonglineStubDto;
import fr.ird.observe.services.dto.longline.SetLonglineTdrDto;
import fr.ird.observe.services.dto.longline.SizeMeasureDto;
import fr.ird.observe.services.dto.longline.TdrDto;
import fr.ird.observe.services.dto.longline.TripLonglineActivityDto;
import fr.ird.observe.services.dto.longline.TripLonglineDto;
import fr.ird.observe.services.dto.longline.TripLonglineGearUseDto;
import fr.ird.observe.services.dto.longline.WeightMeasureDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.seine.ActivitySeineDto;
import fr.ird.observe.services.dto.seine.ActivitySeineStubDto;
import fr.ird.observe.services.dto.seine.FloatingObjectDto;
import fr.ird.observe.services.dto.seine.FloatingObjectObservedSpeciesDto;
import fr.ird.observe.services.dto.seine.FloatingObjectSchoolEstimateDto;
import fr.ird.observe.services.dto.seine.FloatingObjectTransmittingBuoyDto;
import fr.ird.observe.services.dto.seine.GearUseFeaturesMeasurementSeineDto;
import fr.ird.observe.services.dto.seine.GearUseFeaturesSeineDto;
import fr.ird.observe.services.dto.seine.NonTargetCatchDto;
import fr.ird.observe.services.dto.seine.NonTargetLengthDto;
import fr.ird.observe.services.dto.seine.NonTargetSampleDto;
import fr.ird.observe.services.dto.seine.ObjectObservedSpeciesDto;
import fr.ird.observe.services.dto.seine.ObjectSchoolEstimateDto;
import fr.ird.observe.services.dto.seine.RouteDto;
import fr.ird.observe.services.dto.seine.RouteStubDto;
import fr.ird.observe.services.dto.seine.SchoolEstimateDto;
import fr.ird.observe.services.dto.seine.SetSeineDto;
import fr.ird.observe.services.dto.seine.SetSeineNonTargetCatchDto;
import fr.ird.observe.services.dto.seine.SetSeineSchoolEstimateDto;
import fr.ird.observe.services.dto.seine.SetSeineTargetCatchDto;
import fr.ird.observe.services.dto.seine.TargetCatchDto;
import fr.ird.observe.services.dto.seine.TargetLengthDto;
import fr.ird.observe.services.dto.seine.TargetSampleDto;
import fr.ird.observe.services.dto.seine.TransmittingBuoyDto;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import fr.ird.observe.services.dto.seine.TripSeineGearUseDto;

import java.util.Date;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

/**
 * Contient les définitions de tous les ensembles de références (de type donnée) reconnus dans l'application.
 *
 * Created on 11/11/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public enum DataReferenceSetDefinitions {

    // -------------------------------------------------------------------------------------------------------------- //
    // -- SEINE ----------------------------------------------------------------------------------------------------- //
    // -------------------------------------------------------------------------------------------------------------- //

    TRIP_SEINE(newDefinitionBuilder(TripSeineDto.class)
                       .addProperty(Date.class, TripSeineDto.PROPERTY_START_DATE)
                       .addProperty(Date.class, TripSeineDto.PROPERTY_END_DATE)
                       .addProperty(String.class, TripSeineDto.PROPERTY_PROGRAM + "Id")
                       .addProperty(int.class, TripSeineDto.PROPERTY_ROUTE_COUNT)
                       .addProperty(String.class, TripSeineDto.PROPERTY_VESSEL)
                       .addProperty(String.class, TripSeineDto.PROPERTY_OBSERVER)),

    ROUTE(newDefinitionBuilder(RouteDto.class)
                  .addProperty(Date.class, RouteDto.PROPERTY_DATE)
                  .addProperty(String.class, RouteDto.PROPERTY_COMMENT)),

    TARGET_SAMPLE(newDefinitionBuilder(TargetSampleDto.class)),

    FLOATING_OBJECT(newDefinitionBuilder(FloatingObjectDto.class)
                            .addProperty(String.class, FloatingObjectDto.PROPERTY_OBJECT_TYPE)),
    FLOATING_OBJECT_SCHOOL_ESTIMATE(newDefinitionBuilder(FloatingObjectSchoolEstimateDto.class)),
    FLOATING_OBJECT_OBSERVED_SPECIES(newDefinitionBuilder(FloatingObjectObservedSpeciesDto.class)),
    OBJECT_OBSERVED_SPECIES(newDefinitionBuilder(ObjectObservedSpeciesDto.class)
                                    .addProperty(String.class, ObjectObservedSpeciesDto.PROPERTY_SPECIES)
                                    .addProperty(String.class, ObjectObservedSpeciesDto.PROPERTY_SPECIES_STATUS)),
    OBJECT_SCHOOL_ESTIMATE(newDefinitionBuilder(ObjectSchoolEstimateDto.class)
                                   .addProperty(String.class, ObjectSchoolEstimateDto.PROPERTY_SPECIES)
                                   .addProperty(Float.class, ObjectSchoolEstimateDto.PROPERTY_TOTAL_WEIGHT)),

    FLOATING_OBJECT_TRANSMITTING_BUOY(newDefinitionBuilder(FloatingObjectTransmittingBuoyDto.class)),

    NON_TARGET_LENGTH(newDefinitionBuilder(NonTargetLengthDto.class)
                              .addProperty(String.class, NonTargetLengthDto.PROPERTY_SPECIES)
                              .addProperty(Float.class, NonTargetLengthDto.PROPERTY_LENGTH)),

    NON_TARGET_CATCH(newDefinitionBuilder(NonTargetCatchDto.class)
                             .addProperty(String.class, NonTargetCatchDto.PROPERTY_SPECIES)
                             .addProperty(String.class, NonTargetCatchDto.PROPERTY_SPECIES_FATE)),

    SCHOOL_ESTIMATE(newDefinitionBuilder(SchoolEstimateDto.class)
                            .addProperty(String.class, SchoolEstimateDto.PROPERTY_SPECIES)
                            .addProperty(String.class, SchoolEstimateDto.PROPERTY_MEAN_WEIGHT)
                            .addProperty(String.class, SchoolEstimateDto.PROPERTY_TOTAL_WEIGHT)
    ),

    TARGET_CATCH(newDefinitionBuilder(TargetCatchDto.class)
                         .addProperty(String.class, TargetCatchDto.PROPERTY_SPECIES)),

    TARGET_LENGTH(newDefinitionBuilder(TargetLengthDto.class)
                          .addProperty(String.class, TargetLengthDto.PROPERTY_SPECIES)
                          .addProperty(String.class, TargetLengthDto.PROPERTY_LENGTH)
                          .addProperty(String.class, TargetLengthDto.PROPERTY_COUNT)
    ),

    ACTIVITY_SEINE(newDefinitionBuilder(ActivitySeineDto.class)
                           .addProperty(Date.class, ActivitySeineDto.PROPERTY_TIME)
                           .addProperty(String.class, ActivitySeineDto.PROPERTY_VESSEL_ACTIVITY_SEINE + "Id")
                           .addProperty(String.class, ActivitySeineDto.PROPERTY_VESSEL_ACTIVITY_SEINE)
                           .addProperty(DataReference.class, ActivitySeineDto.PROPERTY_SET_SEINE)
    ),

    SET_SEINE(newDefinitionBuilder(SetSeineDto.class)
                      .addProperty(String.class, SetSeineDto.PROPERTY_COMMENT)),

    ENCOUNTER(newDefinitionBuilder(EncounterDto.class)
                      .addProperty(String.class, EncounterDto.PROPERTY_ENCOUNTER_TYPE)
                      .addProperty(String.class, EncounterDto.PROPERTY_SPECIES)
    ),

    TRANSMITTING_BUYO(newDefinitionBuilder(TransmittingBuoyDto.class)
                              .addProperty(String.class, TransmittingBuoyDto.PROPERTY_CODE)
                              .addProperty(String.class, TransmittingBuoyDto.PROPERTY_BRAND)
                              .addProperty(ReferentialReference.class, TransmittingBuoyDto.PROPERTY_TRANSMITTING_BUOY_TYPE)
                              .addProperty(ReferentialReference.class, TransmittingBuoyDto.PROPERTY_TRANSMITTING_BUOY_OPERATION)
    ),


    // -------------------------------------------------------------------------------------------------------------- //
    // -- LONGLINE -------------------------------------------------------------------------------------------------- //
    // -------------------------------------------------------------------------------------------------------------- //

    TRIP_LONGLINE(newDefinitionBuilder(TripLonglineDto.class)
                          .addProperty(Date.class, TripLonglineDto.PROPERTY_START_DATE)
                          .addProperty(Date.class, TripLonglineDto.PROPERTY_END_DATE)
                          .addProperty(String.class, TripLonglineDto.PROPERTY_PROGRAM + "Id")
                          .addProperty(String.class, TripLonglineDto.PROPERTY_TRIP_TYPE + "Id")
                          .addProperty(String.class, TripLonglineDto.PROPERTY_VESSEL)
                          .addProperty(String.class, TripLonglineDto.PROPERTY_OBSERVER)),

    ACTIVITY_LONGLINE(newDefinitionBuilder(ActivityLonglineDto.class)
                              .addProperty(Date.class, ActivityLonglineDto.PROPERTY_TIME_STAMP)
                              .addProperty(String.class, ActivityLonglineDto.PROPERTY_VESSEL_ACTIVITY_LONGLINE + "Id")
                              .addProperty(String.class, ActivityLonglineDto.PROPERTY_VESSEL_ACTIVITY_LONGLINE)
                              .addProperty(DataReference.class, ActivityLonglineDto.PROPERTY_SET_LONGLINE)
    ),

    CATCH_LONGLINE(newDefinitionBuilder(CatchLonglineDto.class)
                           .addProperty(String.class, CatchLonglineDto.PROPERTY_HOME_ID)),

    SET_LONGLINE(newDefinitionBuilder(SetLonglineDto.class)
                         .addProperty(String.class, SetLonglineDto.PROPERTY_HOME_ID)),

    SECTION(newDefinitionBuilder(SectionDto.class)
                    .addProperty(Integer.class, SectionDto.PROPERTY_SETTING_IDENTIFIER)
                    .addProperty(Integer.class, SectionDto.PROPERTY_HAULING_IDENTIFIER)),

    BASKET(newDefinitionBuilder(BasketDto.class)
                   .addProperty(Integer.class, BasketDto.PROPERTY_SETTING_IDENTIFIER)
                   .addProperty(Integer.class, BasketDto.PROPERTY_HAULING_IDENTIFIER)),

    BRANCHLINE(newDefinitionBuilder(BranchlineDto.class)
                       .addProperty(Integer.class, BranchlineDto.PROPERTY_SETTING_IDENTIFIER)
                       .addProperty(Integer.class, BranchlineDto.PROPERTY_HAULING_IDENTIFIER)),

    BASKET_WITH_SECTION(newDefinitionBuilder(BasketWithSectionIdDto.class)
                                .addProperty(Integer.class, BasketWithSectionIdDto.PROPERTY_SETTING_IDENTIFIER)
                                .addProperty(Integer.class, BasketWithSectionIdDto.PROPERTY_HAULING_IDENTIFIER)
                                .addProperty(String.class, BasketWithSectionIdDto.PROPERTY_SECTION_ID)),

    BRANCHLINE_WITH_BASKET(newDefinitionBuilder(BranchlineWithBasketIdDto.class)
                                   .addProperty(Integer.class, BranchlineWithBasketIdDto.PROPERTY_SETTING_IDENTIFIER)
                                   .addProperty(Integer.class, BranchlineWithBasketIdDto.PROPERTY_HAULING_IDENTIFIER)
                                   .addProperty(String.class, BranchlineWithBasketIdDto.PROPERTY_BASKET_ID)),

    FLOATLINES_COMPOSITION(newDefinitionBuilder(FloatlinesCompositionDto.class)
                                   .addProperty(String.class, FloatlinesCompositionDto.PROPERTY_LINE_TYPE)
                                   .addProperty(Float.class, FloatlinesCompositionDto.PROPERTY_LENGTH)
                                   .addProperty(Float.class, FloatlinesCompositionDto.PROPERTY_PROPORTION)),
    BRANCHLINES_COMPOSITION(newDefinitionBuilder(BranchlinesCompositionDto.class)
                                    .addProperty(String.class, BranchlinesCompositionDto.PROPERTY_TOP_TYPE)
                                    .addProperty(String.class, BranchlinesCompositionDto.PROPERTY_TRACELINE_TYPE)
                                    .addProperty(Float.class, BranchlinesCompositionDto.PROPERTY_LENGTH)
                                    .addProperty(Float.class, BranchlinesCompositionDto.PROPERTY_PROPORTION)),
    HOOKS_COMPOSITION(newDefinitionBuilder(HooksCompositionDto.class)
                              .addProperty(String.class, HooksCompositionDto.PROPERTY_HOOK_TYPE)
                              .addProperty(String.class, HooksCompositionDto.PROPERTY_HOOK_SIZE)
                              .addProperty(Float.class, HooksCompositionDto.PROPERTY_HOOK_OFFSET)
                              .addProperty(Float.class, HooksCompositionDto.PROPERTY_PROPORTION)),
    BAITS_COMPOSITION(newDefinitionBuilder(BaitsCompositionDto.class)
                              .addProperty(String.class, BaitsCompositionDto.PROPERTY_BAIT_TYPE)
                              .addProperty(String.class, BaitsCompositionDto.PROPERTY_BAIT_SETTING_STATUS)
                              .addProperty(Float.class, BaitsCompositionDto.PROPERTY_INDIVIDUAL_SIZE)
                              .addProperty(Float.class, BaitsCompositionDto.PROPERTY_INDIVIDUAL_WEIGHT)
                              .addProperty(Float.class, BaitsCompositionDto.PROPERTY_PROPORTION)),
    WEIGHT_MEASURE(newDefinitionBuilder(WeightMeasureDto.class)
                           .addProperty(String.class, WeightMeasureDto.PROPERTY_WEIGHT_MEASURE_TYPE)
                           .addProperty(Float.class, WeightMeasureDto.PROPERTY_WEIGHT)),
    SIZE_MEASURE(newDefinitionBuilder(SizeMeasureDto.class)
                         .addProperty(String.class, SizeMeasureDto.PROPERTY_SIZE_MEASURE_TYPE)
                         .addProperty(Float.class, SizeMeasureDto.PROPERTY_SIZE)),
    ACTIVITY_LONGLINE_ENCOUNTER(newDefinitionBuilder(ActivityLonglineEncounterDto.class)),
    ACTIVITY_LONGLINE_SENSOR_USED(newDefinitionBuilder(ActivityLonglineSensorUsedDto.class)),
    ACTIVITY_LONGLINE_STUB(newDefinitionBuilder(ActivityLonglineStubDto.class)),
    ACTIVITY_SEINE_STUB(newDefinitionBuilder(ActivitySeineStubDto.class)),
    GEAR_USE_FEATURES_LONGLINE(newDefinitionBuilder(GearUseFeaturesLonglineDto.class)),
    GEAR_USE_FEATURES_MEASUREMENTS_LONGLINE(newDefinitionBuilder(GearUseFeaturesMeasurementLonglineDto.class)),
    GEAR_USE_FEATURES_SEINE(newDefinitionBuilder(GearUseFeaturesSeineDto.class)),
    GEAR_USE_FEATURES_MEASUREMENTS_SEINE(newDefinitionBuilder(GearUseFeaturesMeasurementSeineDto.class)),
    SET_LONGLINE_STUB(newDefinitionBuilder(SetLonglineStubDto.class)),
    SET_LONGLINE_CATCH(newDefinitionBuilder(SetLonglineCatchDto.class)),
    SET_LONGLINE_TDR(newDefinitionBuilder(SetLonglineTdrDto.class)),
    SET_LONGLINE_DETAIL_COMPOSITION(newDefinitionBuilder(SetLonglineDetailCompositionDto.class)),
    SET_LONGLINE_GLOBAL_COMPOSITION(newDefinitionBuilder(SetLonglineGlobalCompositionDto.class)),
    TRIP_LONGLINE_ACTIVITY(newDefinitionBuilder(TripLonglineActivityDto.class)),
    TRIP_LONGLINE_GEAR_USE(newDefinitionBuilder(TripLonglineGearUseDto.class)),
    TRIP_SEINE_GEAR_USE(newDefinitionBuilder(TripSeineGearUseDto.class)),
    NON_TARGET_SAMPLE(newDefinitionBuilder(NonTargetSampleDto.class)),
    SET_SEINE_TARGET_CATCH(newDefinitionBuilder(SetSeineTargetCatchDto.class)),
    SET_SEINE_NON_TARGET_CATCH(newDefinitionBuilder(SetSeineNonTargetCatchDto.class)),
    SET_SEINE_SCHOOL_ESTIMATE(newDefinitionBuilder(SetSeineSchoolEstimateDto.class)),
    ROUTE_STUB(newDefinitionBuilder(RouteStubDto.class)),

    TDR(newDefinitionBuilder(TdrDto.class)
                .addProperty(String.class, TdrDto.PROPERTY_HOME_ID)),

    SENSOR_USED(newDefinitionBuilder(SensorUsedDto.class)
                        .addProperty(String.class, SensorUsedDto.PROPERTY_SENSOR_TYPE));

    public static final Map<String, ReferenceSetDefinition> MAPPING = new TreeMap<>();

    static {

        for (DataReferenceSetDefinitions definitions : DataReferenceSetDefinitions.values()) {

            ReferenceSetDefinition<? extends DataDto> definition = definitions.getDefinition();

            String dtoType = definition.getType().getName();

            // On ne peut pas définir 2 définitions différentes pour un même dto
            Preconditions.checkArgument(!MAPPING.containsKey(dtoType), "A defintion already exist for dto : " + dtoType);

            MAPPING.put(dtoType, definition);

        }
    }

    private final ReferenceSetDefinition definition;

    protected static <R extends DataDto> ReferenceSetDefinition.Builder newDefinitionBuilder(Class<R> type) {
        return ReferenceSetDefinition.builder(type);
    }

    public static <D extends DataDto> ReferenceSetDefinition<D> getDefinition(Class<D> type) {
        ReferenceSetDefinition<D> referenceSetDefinition = MAPPING.get(type.getName());
        Objects.requireNonNull(referenceSetDefinition, "Could not find definition for type: " + type);
        return referenceSetDefinition;
    }

    public static <D extends DataDto> ReferenceSetDefinition<D> getDefinition(String name) {
        ReferenceSetDefinition<D> referenceSetDefinition = MAPPING.get(name);
        Objects.requireNonNull(referenceSetDefinition, "Could not find definition for type: " + name);
        return referenceSetDefinition;
    }

    DataReferenceSetDefinitions(ReferenceSetDefinition.Builder builder) {
        this.definition = builder.build();
    }

    public <D extends DataDto> ReferenceSetDefinition<D> getDefinition() {
        return definition;
    }

}
