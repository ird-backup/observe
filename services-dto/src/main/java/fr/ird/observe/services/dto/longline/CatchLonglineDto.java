package fr.ird.observe.services.dto.longline;

/*-
 * #%L
 * ObServe :: Services DTO
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.DataReference;

import javax.annotation.Generated;

@Generated(value = "org.nuiton.eugene.java.BeanTransformer", date = "Wed Mar 01 18:04:05 CET 2017")
public class CatchLonglineDto extends GeneratedCatchLonglineDto {


    public void setSection(DataReference<SectionDto> section) {
        super.setSection(section);
        System.out.println("WTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTF ???? to section: "+section);
    }

    private static final long serialVersionUID = 3846975004402399536L;
} //CatchLonglineDto
