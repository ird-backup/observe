package fr.ird.observe.services.topia.service;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Optional;
import fr.ird.observe.ObserveEntityEnum;
import fr.ird.observe.entities.LastUpdateDate;
import fr.ird.observe.entities.ObserveDataEntity;
import fr.ird.observe.entities.ObserveEntity;
import fr.ird.observe.entities.referentiel.ObserveReferentialEntity;
import fr.ird.observe.services.service.LastUpdateDateService;
import fr.ird.observe.services.topia.ObserveServiceTopia;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.support.TopiaSqlQuery;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

/**
 * Created on 07/09/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class LastUpdateDateServiceTopia extends ObserveServiceTopia implements LastUpdateDateService {

    /** Logger. */
    private static final Log log = LogFactory.getLog(LastUpdateDateServiceTopia.class);

    @Override
    public void updateReferentialLastUpdateDates() {

        for (Class<? extends ObserveReferentialEntity> entityType : BINDER_ENGINE.getReferentialEntityToDtoTypes().keySet()) {
            updateLastUpdateDate(entityType);
        }

    }

    @Override
    public void updateDataLastUpdateDates() {

        for (Class<? extends ObserveDataEntity> entityType : BINDER_ENGINE.getDataEntityToDtoTypes().keySet()) {
            updateLastUpdateDate(entityType);
        }
    }

    private <E extends ObserveEntity> void updateLastUpdateDate(Class<E> entityType) {

        Optional<LastUpdateDate> optionalLastUpdateDate = getTopiaPersistenceContext().getLastUpdateDateDao()
                                                                                      .forTypeEquals(entityType.getName())
                                                                                      .setOrderByArguments(LastUpdateDate.PROPERTY_LAST_UPDATE_DATE + " DESC")
                                                                                      .tryFindFirst();

        if (!optionalLastUpdateDate.isPresent()) {
            return;
        }

        LastUpdateDate lastUpdateDate = optionalLastUpdateDate.get();

        ObserveEntityEnum entityEnum = serviceContext.getTopiaApplicationContext().getEntityEnum(entityType);

        String schemaName = entityEnum.dbSchemaName();
        String tableName = entityEnum.dbTableName();


        Timestamp maxLastUpdateDate = getTopiaPersistenceContext().getSqlSupport().findSingleResult(new TopiaSqlQuery<Timestamp>() {
            @Override
            public PreparedStatement prepareQuery(Connection connection) throws SQLException {

                return connection.prepareStatement("SELECT max(lastUpdateDate)::TIMESTAMP FROM " + schemaName + "." + tableName);
            }

            @Override
            public Timestamp prepareResult(ResultSet set) throws SQLException {
                return set.getTimestamp(1);
            }
        });

        if (lastUpdateDate.getLastUpdateDate().before(maxLastUpdateDate)) {

            if (log.isInfoEnabled()) {
                log.info("Update LastUpdateDate for " + entityType.getName() + " with value: " + maxLastUpdateDate);
            }
            lastUpdateDate.setLastUpdateDate(maxLastUpdateDate);
        }
    }
}
