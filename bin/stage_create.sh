#!/usr/bin/env bash

PROJECT=ird-observe
MASTER=master-5.x
DEVELOP=develop-5.x
STAGE=frirdobserve

VERSION=$(grep -e "-SNAPSHOT" pom.xml | cut -d'>' -f2 | cut -d'<' -f1)
VERSION=${VERSION/-SNAPSHOT/}
LOG_DIR=/tmp/${PROJECT}-${VERSION}
echo "version to release: $VERSION"
echo "log dir: $LOG_DIR"
rm -rf ${LOG_DIR}
mkdir -p ${LOG_DIR}

git checkout -B ${MASTER}
git checkout -B ${DEVELOP}

echo "Start release: $VERSION ($LOG_DIR/release-start.log)"
mvn jgitflow:release-start -B -Prelease-profile --log-file ${LOG_DIR}/release-start.log
if [ ! "$?" == "0" ]; then
   echo "Error"
   exit 1
fi
echo "Finish release: $VERSION ($LOG_DIR/release-finish.log)"
mvn jgitflow:release-finish -Prelease-profile --log-file ${LOG_DIR}/release-finish.log
if [ ! "$?" == "0" ]; then
   echo "Error"
   exit 1
fi
echo "Get staging id..."
STAGE_ID=$(mvn nexus-staging:rc-list -N | grep ${STAGE} | grep OPEN | cut -d' ' -f2)

echo "Closing stage: $STAGE_ID ($LOG_DIR/stage-close.log)"
mvn nexus-staging:close -N -DstagingRepositoryId=${STAGE_ID} --log-file ${LOG_DIR}/release-close.log
if [ ! "$?" == "0" ]; then
   echo "Error"
   exit 1
fi

echo "Update changelog: $STAGE_ID ($LOG_DIR/release-changelog.log)"
rm -rf target/gitlab_cache

mvn -N -Pupdate-staging-changelog -Dgitlab.stagingUrl=https://oss.sonatype.org/content/repositories/${STAGE}-${STAGE_ID} -Dgitlab.milestone=${VERSION} --log-file ${LOG_DIR}/release-changelog.log
if [ ! "$?" == "0" ]; then
   echo "Error"
   exit 1
fi

git commit -m"Update changelog for staging version $VERSION" CHANGELOG.md
