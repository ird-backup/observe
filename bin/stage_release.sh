#!/usr/bin/env sh

PROJECT=ird-observe
STAGE=frirdobserve

LOG_DIR=/tmp/${PROJECT}-stage
echo "log dir: $LOG_DIR"
rm -rf ${LOG_DIR}
mkdir -p ${LOG_DIR}

echo "Get staging id..."
STAGE_ID=$(mvn -N nexus-staging:rc-list | grep ${STAGE} | grep CLOSED | cut -d' ' -f2)

echo "Stage release: $STAGE_ID ($LOG_DIR/stage-release.log)"
mvn nexus-staging:release -N -DstagingRepositoryId=${STAGE_ID} --log-file --log-file ${LOG_DIR}/stage-release.log
