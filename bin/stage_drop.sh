#!/usr/bin/env sh

PROJECT=ird-observe
MASTER=master-5.x
DEVELOP=develop-5.x
STAGE=frirdobserve

LOG_DIR=/tmp/${PROJECT}-stage
echo "log dir: $LOG_DIR"
rm -rf ${LOG_DIR}
mkdir -p ${LOG_DIR}

echo "Get staging id..."
STAGE_ID=$(mvn -N nexus-staging:rc-list | grep ${STAGE} | cut -d' ' -f2 )

echo "Stage drop: $STAGE_ID ($LOG_DIR/stage-drop.log)"
mvn nexus-staging:drop -DstagingRepositoryId=${STAGE_ID} --log-file --log-file ${LOG_DIR}/stage-release.log
