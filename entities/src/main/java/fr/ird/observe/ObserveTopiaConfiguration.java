package fr.ird.observe;

/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.hibernate.hikaricp.internal.HikariCPConnectionProvider;
import org.nuiton.topia.persistence.BeanTopiaConfiguration;
import org.nuiton.topia.persistence.HibernateAvailableSettings;
import org.nuiton.topia.persistence.jdbc.JdbcConfiguration;

/**
 * Created on 23/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ObserveTopiaConfiguration extends BeanTopiaConfiguration {

    private static final long serialVersionUID = 1L;

    protected final boolean h2Configuration;

    public ObserveTopiaConfiguration(JdbcConfiguration jdbcConfiguration, boolean h2Configuration) {
        super(jdbcConfiguration);
        this.h2Configuration = h2Configuration;

        hibernateExtraConfiguration.put(HibernateAvailableSettings.CONNECTION_PROVIDER, HikariCPConnectionProvider.class.getName());
        hibernateExtraConfiguration.put("hibernate.hikari.minimumIdle", "1");
        hibernateExtraConfiguration.put("hibernate.hikari.maximumPoolSize", "10");
        hibernateExtraConfiguration.put("hibernate.hikari.autoCommit", "false");
        hibernateExtraConfiguration.put("hibernate.hikari.registerMbeans", "true");

//        hibernateExtraConfiguration.put(HibernateAvailableSettings.CONNECTION_PROVIDER, "org.hibernate.c3p0.internal.C3P0ConnectionProvider");
//        hibernateExtraConfiguration.put(HibernateAvailableSettings.C3P0_MAX_SIZE, "500");
//        hibernateExtraConfiguration.put(HibernateAvailableSettings.C3P0_MAX_STATEMENTS, "500");
//        hibernateExtraConfiguration.put(HibernateAvailableSettings.C3P0_TIMEOUT, "100");
    }

    public boolean isH2Configuration() {
        return h2Configuration;
    }
}
