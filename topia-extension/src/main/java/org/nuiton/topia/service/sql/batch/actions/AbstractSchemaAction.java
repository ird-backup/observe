package org.nuiton.topia.service.sql.batch.actions;

/*
 * #%L
 * ObServe :: ToPIA Extension
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.hibernate.boot.Metadata;
import org.hibernate.boot.registry.BootstrapServiceRegistry;
import org.hibernate.boot.registry.BootstrapServiceRegistryBuilder;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.boot.spi.MetadataImplementor;
import org.hibernate.dialect.Dialect;
import org.hibernate.engine.config.spi.ConfigurationService;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.hibernate.tool.schema.TargetType;
import org.nuiton.topia.persistence.HibernateAvailableSettings;

import java.io.IOException;
import java.nio.file.Path;
import java.sql.SQLException;
import java.util.EnumSet;
import java.util.Properties;

/**
 * Created on 01/01/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0.1
 */
public abstract class AbstractSchemaAction<R extends AbstractSchemaRequest> extends AbstractSqlAction<R> {

    protected AbstractSchemaAction(R request) {
        super(request);
    }

    protected abstract String produceSql(Class<? extends Dialect> dialectType, Path temporaryDirectory) throws IOException;

    @Override
    protected final void execute() throws IOException, SQLException {

        String sqlStatements = produceSql(request.getDialect(), request.getTemporaryPath());

        if (useOutputDb()) {
            targetConnection.createStatement().execute(sqlStatements);
        }

        if (useOutputWriter()) {
            writer.append(sqlStatements);
        }

    }


    protected void generateSqlInFile(Class<? extends Dialect> dialectType, Path sqlScriptFile, SchemaExport.Action action) {

        Metadata hibernateMetadata = getSourcePersistenceContext().getHibernateSupport().getHibernateMetadata();

        StandardServiceRegistry serviceRegistry = ((MetadataImplementor) hibernateMetadata).getMetadataBuildingOptions().getServiceRegistry();

        Properties properties = new Properties();
        properties.putAll(serviceRegistry.getService(ConfigurationService.class).getSettings());
        properties.put(HibernateAvailableSettings.DIALECT, dialectType.getName());

        BootstrapServiceRegistry bsr = new BootstrapServiceRegistryBuilder().build();
        StandardServiceRegistryBuilder ssrBuilder = new StandardServiceRegistryBuilder(bsr);
        ssrBuilder.applySettings(properties);
        StandardServiceRegistry registry = ssrBuilder.build();

        try {
            new SchemaExport()
                    .setOutputFile(sqlScriptFile.toFile().getAbsolutePath())
                    .setDelimiter(";")
                    .execute(EnumSet.of(TargetType.SCRIPT), action, hibernateMetadata, registry);
        } finally {
            StandardServiceRegistryBuilder.destroy(registry);
        }
    }

}
