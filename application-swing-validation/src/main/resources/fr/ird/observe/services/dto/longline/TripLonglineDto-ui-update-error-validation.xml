<?xml version="1.0" encoding="UTF-8"?>
<!--
  #%L
  ObServe :: Application Swing Validation
  %%
  Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

<!DOCTYPE validators PUBLIC
  "-//Apache Struts//XWork Validator 1.0.3//EN"
  "http://struts.apache.org/dtds/xwork-validator-1.0.3.dtd">
<validators>

  <field name="tripType">

    <!-- pas de tripType selectionne -->
    <field-validator type="required" short-circuit="true">
      <message>validator.ui.trip.required.tripType</message>
    </field-validator>

  </field>

  <field name="observer">

    <!-- pas de observer selectionne -->
    <field-validator type="required" short-circuit="true">
      <message>validator.ui.trip.required.observer</message>
    </field-validator>

  </field>

  <field name="vessel">

    <!-- pas de vessel sélectionné -->
    <field-validator type="required" short-circuit="true">
      <message>validator.ui.trip.required.vessel</message>
    </field-validator>

  </field>

  <field name="ocean">

    <!-- pas d'ocean selectionne -->
    <field-validator type="required" short-circuit="true">
      <message>validator.ui.trip.required.ocean</message>
    </field-validator>

  </field>

  <field name="startDate">

    <!-- pas de date de debut selectionne -->
    <field-validator type="required" short-circuit="true">
      <message>validator.ui.trip.required.startDate</message>
    </field-validator>

    <!-- coherence startDate > date de toute route -->
    <field-validator type="collectionFieldExpression">
      <param name="mode">ALL</param>
      <param name="useSensitiveContext">true</param>
      <param name="collectionFieldName">activityLongline</param>
      <param name="expression"><![CDATA[
                startDate.time <= current.timeStamp.time
            ]]>
      </param>
      <message>validator.ui.trip.invalid.startDate##${index}</message>
    </field-validator>

  </field>

  <field name="endDate">

    <!-- pas de date de fin selectionnee -->
    <field-validator type="required" short-circuit="true">
      <message>validator.ui.trip.required.endDate</message>
    </field-validator>

    <!-- date de fin avant date de debut -->
    <field-validator type="fieldexpression" short-circuit="true">
      <param name="expression">
        <![CDATA[ endDate == null || endDate.time >= startDate.time ]]>
      </param>
      <message>validator.ui.trip.endDate.after.startDate</message>
    </field-validator>

    <!-- coherence endDate > date de toute route -->
    <field-validator type="collectionFieldExpression">
      <param name="mode">ALL</param>
      <param name="useSensitiveContext">true</param>
      <param name="collectionFieldName">activityLongline</param>
      <param name="expression"><![CDATA[
                current.timeStamp.time <= endDate.time
            ]]>
      </param>
      <message>validator.ui.trip.invalid.endDate##${index}</message>
    </field-validator>

  </field>

  <!--<field name="activityLongline">-->

    <!--&lt;!&ndash; coherence date des routes  &ndash;&gt;-->
    <!--<field-validator type="collectionFieldExpression">-->
      <!--<param name="mode">ALL</param>-->
      <!--<param name="useSensitiveContext">true</param>-->
      <!--<param name="expression"><![CDATA[-->
                <!--previous == null || previous.timeStamp.time <= current.timeStamp.time-->
            <!--]]>-->
      <!--</param>-->
      <!--<message>validator.ui.trip.invalid.date##${index}</message>-->
    <!--</field-validator>-->

  <!--</field>-->

  <field name="comment">

    <!-- comentaire de moins de 1024 caractères -->
    <field-validator type="stringlength">
      <param name="maxLength">1024</param>
      <message>validator.ui.trip.comment.tobig</message>
    </field-validator>

    <!-- comment requis selon le type de tripType choisi -->
    <field-validator type="fieldexpression">
      <param name="expression">
        <![CDATA[ tripType == null || !tripType.needComment || (comment != null && !comment.empty)  ]]>
      </param>
      <message>validator.ui.trip.required.comment.for.tripType</message>
    </field-validator>

    <!-- comment requis selon le type de departureHarbour choisi -->
    <field-validator type="fieldexpression">
      <param name="expression">
        <![CDATA[ departureHarbour == null || !departureHarbour.needComment || (comment != null && !comment.empty)  ]]>
      </param>
      <message>validator.ui.trip.required.comment.for.departureHarbour</message>
    </field-validator>

    <!-- comment requis selon le type de landingHarbour choisi -->
    <field-validator type="fieldexpression">
      <param name="expression">
        <![CDATA[ landingHarbour == null || !landingHarbour.needComment || (comment != null && !comment.empty)  ]]>
      </param>
      <message>validator.ui.trip.required.comment.for.landingHarbour</message>
    </field-validator>

    <!-- comment requis selon le type de vessel choisi -->
    <field-validator type="fieldexpression">
      <param name="expression">
        <![CDATA[ vessel == null || !vessel.needComment || (comment != null && !comment.empty)  ]]>
      </param>
      <message>validator.ui.trip.required.comment.for.vessel</message>
    </field-validator>

    <!-- comment requis selon le type de ocean choisi -->
    <field-validator type="fieldexpression">
      <param name="expression">
        <![CDATA[ ocean == null || !ocean.needComment || (comment != null && !comment.empty)  ]]>
      </param>
      <message>validator.ui.trip.required.comment.for.ocean</message>
    </field-validator>

    <!-- comment requis selon le type de dataEntryOperator choisi -->
    <field-validator type="fieldexpression">
      <param name="expression">
        <![CDATA[ dataEntryOperator == null || !dataEntryOperator.needComment || (comment != null && !comment.empty)  ]]>
      </param>
      <message>validator.ui.trip.required.comment.for.dataEntryOperator</message>
    </field-validator>

    <!-- comment requis selon le type de observer choisi -->
    <field-validator type="fieldexpression">
      <param name="expression">
        <![CDATA[ observer == null || !observer.needComment || (comment != null && !comment.empty)  ]]>
      </param>
      <message>validator.ui.trip.required.comment.for.observer</message>
    </field-validator>

    <!-- comment requis selon le type de captain choisi -->
    <field-validator type="fieldexpression">
      <param name="expression">
        <![CDATA[ captain == null || !captain.needComment || (comment != null && !comment.empty)  ]]>
      </param>
      <message>validator.ui.trip.required.comment.for.captain</message>
    </field-validator>

  </field>

</validators>
