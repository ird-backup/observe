/*==============================================================*/
/* Nom de SGBD :  PostgreSQL 8                                  */
/* Date de création :  21/10/2008 12:11:00                      */
/*==============================================================*/


/*Drops supprimés*/


/*==============================================================*/
/* Table : ACTIVITE                                             */
/*==============================================================*/
create table ACTIVITE (
   C_PROG               NUMERIC(2)           not null,
   N_MAREE              NUMERIC(4)           not null,
   D_OBS                DATE                 not null,
   H_OBS                DATE                 not null,
   C_NON_COUP_DE_SENNE  NUMERIC(2)           not null,
   C_MODE_DETECT        NUMERIC(2)           null,
   C_BEAUFORT           NUMERIC(2)           null,
   C_ACT_ENV            NUMERIC(2)           null,
   C_ACT_BATEAU         NUMERIC(2)           not null,
   Q_CWP                NUMERIC(1)           not null,
   V_LAT                NUMERIC(4)           not null,
   V_LON                NUMERIC(5)           not null,
   V_VITESSE_BAT        NUMERIC(3,1)         null,
   V_TEMP_SURF          NUMERIC(5,2)         null,
   V_VITESSE_VENT       NUMERIC(3)           null,
   V_DIST_SYST_OBS      FLOAT4               null,
   L_COM_ACT            VARCHAR(1024)        null,
   constraint PK_ACTIVITE primary key (C_PROG, N_MAREE, D_OBS, H_OBS)
);

comment on table ACTIVITE is
'On appelle activité une opération élémentaire effectuée par le bateau. La table ACT_BATEAU qualifie la nature de cette activité. 
L''activité est spécifiée par une petite dizaine d''attributs parmi lesquels les plus importants sont les références au temps (date, heure) et au positionnement géographique du bâtiment.
Les attributs d''environnement (température, courant, vent ) sont éventuellement renseignés.';

/*==============================================================*/
/* Index : ACTIVITE_PK                                          */
/*==============================================================*/
create unique index ACTIVITE_PK on ACTIVITE (
C_PROG,
N_MAREE,
D_OBS,
H_OBS
);

/*==============================================================*/
/* Index : ROUTE_R_ACTIVITE_FK                                  */
/*==============================================================*/
create  index ROUTE_R_ACTIVITE_FK on ACTIVITE (
C_PROG,
N_MAREE,
D_OBS
);

/*==============================================================*/
/* Index : VENT_BEAUFORT_R_ACTIVITE_FK                          */
/*==============================================================*/
create  index VENT_BEAUFORT_R_ACTIVITE_FK on ACTIVITE (
C_BEAUFORT
);

/*==============================================================*/
/* Index : ACT_BATEAU_R_ACTIVITE_FK                             */
/*==============================================================*/
create  index ACT_BATEAU_R_ACTIVITE_FK on ACTIVITE (
C_ACT_BATEAU
);

/*==============================================================*/
/* Index : ACT_ENV_R_ACTIVITE_FK                                */
/*==============================================================*/
create  index ACT_ENV_R_ACTIVITE_FK on ACTIVITE (
C_ACT_ENV
);

/*==============================================================*/
/* Index : MODE_DETECT_R_ACTIVITE_FK                            */
/*==============================================================*/
create  index MODE_DETECT_R_ACTIVITE_FK on ACTIVITE (
C_MODE_DETECT
);

/*==============================================================*/
/* Index : NON_COUP_SENNE_R_ACTIVITE_FK                         */
/*==============================================================*/
create  index NON_COUP_SENNE_R_ACTIVITE_FK on ACTIVITE (
C_NON_COUP_DE_SENNE
);

/*==============================================================*/
/* Table : ACT_BATEAU                                           */
/*==============================================================*/
create table ACT_BATEAU (
   C_ACT_BATEAU         NUMERIC(2)           not null,
   L_ACT_BATEAU         VARCHAR(64)          not null,
   constraint PK_ACT_BATEAU primary key (C_ACT_BATEAU)
);

comment on table ACT_BATEAU is
'Définit les opérations élémentaires éffectuées par le bateau :

0	Au port
1	Transit ( route sans recherche )
2	Recherche ( général )
3	Recherche exclusive d''objets flottants
4	Route vers des systèmes observés
5	Thonier arrivant sur le système détecté
6	Début de pêche ( larguage du skiff )
7	Fin de pêche ( remontée du skiff)
...

99	Autres ( à préciser dans les notes )
';

/*==============================================================*/
/* Index : ACT_BATEAU_PK                                        */
/*==============================================================*/
create unique index ACT_BATEAU_PK on ACT_BATEAU (
C_ACT_BATEAU
);

/*==============================================================*/
/* Table : ACT_ENV                                              */
/*==============================================================*/
create table ACT_ENV (
   C_ACT_ENV            NUMERIC(2)           not null,
   L_ACT_ENV            VARCHAR(64)          not null,
   constraint PK_ACT_ENV primary key (C_ACT_ENV)
);

comment on table ACT_ENV is
'Définit la présence de  ...  à proximité du bateau : 

0	Indéterminé
1	Seul dans la zone
2	Dans un groupe, avec autre(s) thonier(s) visible(s) au radar
3	De même engin et de même pavillon
4	D''engins différents et de même pavillon
5	De même engin et de pavillons différents
6	D''engins et de pavillons différents
';

/*==============================================================*/
/* Index : ACT_ENV_PK                                           */
/*==============================================================*/
create unique index ACT_ENV_PK on ACT_ENV (
C_ACT_ENV
);

/*==============================================================*/
/* Table : ACT_SYSTOBS                                          */
/*==============================================================*/
create table ACT_SYSTOBS (
   C_PROG               NUMERIC(2)           not null,
   N_MAREE              NUMERIC(4)           not null,
   D_OBS                DATE                 not null,
   H_OBS                DATE                 not null,
   C_SYST_OBS           NUMERIC(2)           not null,
   constraint PK_ACT_SYSTOBS primary key (C_PROG, N_MAREE, D_OBS, H_OBS, C_SYST_OBS)
);

comment on table ACT_SYSTOBS is
'Définit les systèmes observés associés à l''activité en cours.
(aucun, un ou plusieurs)';

/*==============================================================*/
/* Index : ACT_SYSTOBS_PK                                       */
/*==============================================================*/
create unique index ACT_SYSTOBS_PK on ACT_SYSTOBS (
C_PROG,
N_MAREE,
D_OBS,
H_OBS,
C_SYST_OBS
);

/*==============================================================*/
/* Index : ACT_SYSTOBS_FK                                       */
/*==============================================================*/
create  index ACT_SYSTOBS_FK on ACT_SYSTOBS (
C_PROG,
N_MAREE,
D_OBS,
H_OBS
);

/*==============================================================*/
/* Index : ACT_SYSTOBS2_FK                                      */
/*==============================================================*/
create  index ACT_SYSTOBS2_FK on ACT_SYSTOBS (
C_SYST_OBS
);

/*==============================================================*/
/* Table : BALISE_LUE                                           */
/*==============================================================*/
create table BALISE_LUE (
   C_PROG               NUMERIC(2)           not null,
   N_MAREE              NUMERIC(4)           not null,
   D_OBS                DATE                 not null,
   H_OBS                DATE                 not null,
   N_OBJET              NUMERIC(3)           not null,
   C_BALISE             NUMERIC(2)           not null,
   C_RECUP              VARCHAR(64)          null,
   C_MISE_A_L_EAU       VARCHAR(64)          null,
   C_VISITE             VARCHAR(64)          null,
   L_TYPE_AUTRE         VARCHAR(64)          null,
   constraint PK_BALISE_LUE primary key (C_PROG, N_MAREE, D_OBS, H_OBS, N_OBJET, C_BALISE)
);

comment on table BALISE_LUE is
'L''association ''Info Balise lue'' du fait des cardinalités assocciées au deux rôles donne lieu à la table BALISE_LUE dans le schéma relationnel. La table historise pour un type de balise donnée le/les codes lus lors de la visite, de la mise à l''eau ou de la relève. ';

/*==============================================================*/
/* Index : BALISE_LUE_PK                                        */
/*==============================================================*/
create unique index BALISE_LUE_PK on BALISE_LUE (
C_PROG,
N_MAREE,
D_OBS,
H_OBS,
N_OBJET,
C_BALISE
);

/*==============================================================*/
/* Index : BALISE_LUE_FK                                        */
/*==============================================================*/
create  index BALISE_LUE_FK on BALISE_LUE (
C_PROG,
N_MAREE,
D_OBS,
H_OBS,
N_OBJET
);

/*==============================================================*/
/* Index : BALISE_LUE2_FK                                       */
/*==============================================================*/
create  index BALISE_LUE2_FK on BALISE_LUE (
C_BALISE
);

/*==============================================================*/
/* Table : BATEAU                                               */
/*==============================================================*/
create table BATEAU (
   C_BAT                NUMERIC(4)           not null,
   C_TYP_B              NUMERIC(2)           null,
   C_CAT_B              NUMERIC(2)           null,
   C_PAYS               NUMERIC(3)           null,
   C_QUILLE             NUMERIC(4)           null,
   C_FLOTTE             NUMERIC(3)           null,
   AN_SERV              NUMERIC(4)           null,
   D_CHGT_PAV           DATE                 null,
   V_L_HT               NUMERIC(6,1)         null,
   V_CT_M3              NUMERIC(5)           null,
   V_P_CV               NUMERIC(5)           null,
   V_MAX_RECH           NUMERIC(3,1)         null,
   L_BAT                VARCHAR(32)          not null,
   L_COM_B              VARCHAR(255)         null,
   constraint PK_BATEAU primary key (C_BAT)
);

comment on table BATEAU is
'La table historise les bateaux des deux pêcheries O. Atlantique et O. Indien. 

Le numéro de bateau constitue la clé primaire de la table.

La table est unique et sera actualisée de manière synchrone aux évolution du fichier TURBOBAT. Une source unique informe les noeuds du réseau des mises à jour à effectuer.

Quand une caractéristique d''un navire change (pavillon par exemple), un nouveau bateau est  introduit et un nouveau code est attribué. les coordonnées de la "version" précédente restent dans la base. 

La SUPPRESSION d''une occurence de BATEAU est INTERDITE.

Le code quille (code origine) a été introduit pour faciliter le suivi d''un bateau dans le temps. La valeur 999 sera initialement affectée à cet attribut.';

comment on column BATEAU.C_TYP_B is
'Code numérique du type de bateau';

comment on column BATEAU.C_CAT_B is
'Code numérique de la catégorie de bateau';

comment on column BATEAU.C_PAYS is
'Code numérique du pays';

comment on column BATEAU.C_QUILLE is
'Code quille : l''attribut permet de reconstituer (requête simple) l''histoire de tout  bateau dans la pêcherie - 999 si ce code n''est pas encore défini.';

comment on column BATEAU.AN_SERV is
'Année service : année de mise en service du bateau selon les caractéristiques mentionnées';

comment on column BATEAU.V_L_HT is
'Long hors_toute : longueur hors-toutes exprimées en mètres';

comment on column BATEAU.V_CT_M3 is
'Capacité Transp : volume des cuves du bateau exprimé en mètres cubes.';

comment on column BATEAU.V_P_CV is
'Puissance moteur : Puissance exprimée en CV';

comment on column BATEAU.V_MAX_RECH is
'Vitesse prospection : vitesse standard de prospection exprimée en noeuds';

comment on column BATEAU.L_BAT is
'Libellé du bateau (codage chaîne de 64 car)';

comment on column BATEAU.L_COM_B is
'Commentaire';

/*==============================================================*/
/* Index : BATEAU_PK                                            */
/*==============================================================*/
create unique index BATEAU_PK on BATEAU (
C_BAT
);

/*==============================================================*/
/* Index : PAYS_R_BATEAU_FK                                     */
/*==============================================================*/
create  index PAYS_R_BATEAU_FK on BATEAU (
C_PAYS
);

/*==============================================================*/
/* Index : TYPE_BATEAU_R_BATEAU_FK                              */
/*==============================================================*/
create  index TYPE_BATEAU_R_BATEAU_FK on BATEAU (
C_TYP_B
);

/*==============================================================*/
/* Index : CAT_BATEAU_R_BATEAU_FK                               */
/*==============================================================*/
create  index CAT_BATEAU_R_BATEAU_FK on BATEAU (
C_CAT_B
);

/*==============================================================*/
/* Table : CALEE                                                */
/*==============================================================*/
create table CALEE (
   C_PROG               NUMERIC(2)           not null,
   N_MAREE              NUMERIC(4)           not null,
   D_OBS                DATE                 not null,
   H_OBS                DATE                 not null,
   ID_CALEE             NUMERIC(3)           not null,
   C_R_COUP_NUL         NUMERIC(2)           null,
   H_DEB_CALEE          DATE                 not null,
   H_FIN_COULISSAGE     DATE                 null,
   H_FIN_CALEE          DATE                 null,
   V_PROF_MAX_ENGIN     NUMERIC(3)           null,
   V_VIT_COUR           NUMERIC(5,2)         null,
   V_DIR_COUR           NUMERIC(3)           null,
   V_PROF_SOMMET_BANC   NUMERIC(3)           null,
   V_PROF_MOYENNE_BANC  NUMERIC(3)           null,
   V_EPAISSEUR_BANC     NUMERIC(3)           null,
   F_UTIL_SONAR         BOOL                 null,
   L_SUPPLY             VARCHAR(32)          null,
   F_REJET_T            BOOL                 not null,
   F_REJET_F            BOOL                 not null,
   L_COMMENT_CALEE      VARCHAR(1024)        null,
   constraint PK_CALEE primary key (C_PROG, N_MAREE, D_OBS, H_OBS, ID_CALEE)
);

comment on table CALEE is
'La calée informe qu''un coup de pêche a eu lieu et que des captures ont été ou non réalisées. Dans le cas positif, le détail des captures (espèce, poids) est décrit dans les deux autres entités Capure thons et Capture faune assoc. La description d''une calée contient éventuellement des informations sur les étapes successives du coup de pêche ( heure de début, heure de fin de coulissage de la senne, heure de fin de la calée). Les paramètres concernant le banc,  profondeur du sommet du banc, profondeur moyenne du banc, l''épaisseur du banc, sont parfois renseignés. Les flag rejet indique si pour cette calée des espèces mises à bord ont été rejetées tant pour les thons que pour la faune associée. ';

comment on column CALEE.F_REJET_T is
'Indique la présence de rejets de thons, même si l''observateur n''a pu en faire l''identification.
Valeur : 0 non Aucune espèce codée
Valeur : 1 oui  Zero une ou plusieurs espèces codées
Valeur : 9 l''information n''est pas disponible ';

comment on column CALEE.F_REJET_F is
'Indique la présence de rejets d''autres espèces, même si l''observateur n''a pu en faire l''identification.
Valeur : 0 non Aucune espèce codée
Valeur : 1 oui  Zero une ou plusieurs espèces codées
Valeur : 9 l''information n''est pas disponible ';

/*==============================================================*/
/* Index : CALEE_PK                                             */
/*==============================================================*/
create unique index CALEE_PK on CALEE (
C_PROG,
N_MAREE,
D_OBS,
H_OBS,
ID_CALEE
);

/*==============================================================*/
/* Index : COUP_NUL_R_CALEE_FK                                  */
/*==============================================================*/
create  index COUP_NUL_R_CALEE_FK on CALEE (
C_R_COUP_NUL
);

/*==============================================================*/
/* Index : ACTIVITE_R_CALEE_FK                                  */
/*==============================================================*/
create  index ACTIVITE_R_CALEE_FK on CALEE (
C_PROG,
N_MAREE,
D_OBS,
H_OBS
);

/*==============================================================*/
/* Table : CALEE_SYSTOBS                                        */
/*==============================================================*/
create table CALEE_SYSTOBS (
   C_PROG               NUMERIC(2)           not null,
   N_MAREE              NUMERIC(4)           not null,
   D_OBS                DATE                 not null,
   H_OBS                DATE                 not null,
   ID_CALEE             NUMERIC(3)           not null,
   C_SYST_OBS           NUMERIC(2)           not null,
   constraint PK_CALEE_SYSTOBS primary key (C_PROG, N_MAREE, D_OBS, H_OBS, ID_CALEE, C_SYST_OBS)
);

comment on table CALEE_SYSTOBS is
'L''association donne lieu dans le schéma relationnel à la table CALEE_SYSTOBS. L''association ne disposant d''aucun attribut, la table résultante contient uniquement une clé primaire. Les occurences de la table listent pour une calée donnée les systèmes observés lors de sa réalisation :  Matte ( Pas de précision sur le type de banc, Balbaya, sardara, Brisant ou Thons en profondeur, Oiseaux, etc. . Le logicielde saisie  actuel autorise 6 possibilités au plus.
';

/*==============================================================*/
/* Index : CALEE_SYSTOBS_PK                                     */
/*==============================================================*/
create unique index CALEE_SYSTOBS_PK on CALEE_SYSTOBS (
C_PROG,
N_MAREE,
D_OBS,
H_OBS,
ID_CALEE,
C_SYST_OBS
);

/*==============================================================*/
/* Index : CALEE_SYSTOBS_FK                                     */
/*==============================================================*/
create  index CALEE_SYSTOBS_FK on CALEE_SYSTOBS (
C_PROG,
N_MAREE,
D_OBS,
H_OBS,
ID_CALEE
);

/*==============================================================*/
/* Index : CALEE_SYSTOBS2_FK                                    */
/*==============================================================*/
create  index CALEE_SYSTOBS2_FK on CALEE_SYSTOBS (
C_SYST_OBS
);

/*==============================================================*/
/* Table : CAPT_F                                               */
/*==============================================================*/
create table CAPT_F (
   C_ESP_F              NUMERIC(3)           not null,
   C_PROG               NUMERIC(2)           not null,
   N_MAREE              NUMERIC(4)           not null,
   D_OBS                DATE                 not null,
   H_OBS                DATE                 not null,
   ID_CALEE             NUMERIC(3)           not null,
   NO_ESP_F             NUMERIC(3)           not null,
   C_DEVENIR_F          NUMERIC(2)           null,
   C_RAISON_REJET       NUMERIC(2)           null,
   V_POIDS_ESTIM_F      NUMERIC(5,1)         null,
   V_NOMBRE_ESTIM_F     NUMERIC(5)           null,
   V_POIDS_MOY_F        NUMERIC(3)           null,
   V_TAILLE_MOY_F       NUMERIC(3)           null,
   L_COM_CAPT_F         VARCHAR(1024)        null,
   constraint PK_CAPT_F primary key (C_PROG, N_MAREE, D_OBS, C_ESP_F, H_OBS, ID_CALEE, NO_ESP_F)
);

comment on table CAPT_F is
'La table issue de cette entité contiendra le détail des captures des espèces  de la faune associée pour chaque calée positive. L''identifiant d''une capture permet de mentionner plusieurs fois la même espèce.  Quatre attribut , leurs valeurs respectives sont facultatives, précisent le volume de la capture :
            le poids estimé en tonnes
            le nombre estimé d''individus
            le poids moyen (en kg) de la capture
            la taille moyenne (en cm) des individus

L''attribut code devenir (obligatoire) précise si l''espèce est conservée à bord ou les conditions du rejet.

Dans le schéma relationnel, la table CAPT_F issue de l''entité est duppliquée dans la table CAPT_F_CC. Des valeurs élaborées par calcul seront attribuées aux colonnes quantitatives non informées dans CAPT_F lors de la saisie. 


';

comment on column CAPT_F.V_POIDS_MOY_F is
'Estimation du patron :
Poids moyen de l''espèce exprimé en Kg (Individu moyen) dans le banc (YFT, SKJ ou BET)
';

/*==============================================================*/
/* Index : CAPT_F_PK                                            */
/*==============================================================*/
create unique index CAPT_F_PK on CAPT_F (
C_PROG,
N_MAREE,
D_OBS,
C_ESP_F,
H_OBS,
ID_CALEE,
NO_ESP_F
);

/*==============================================================*/
/* Index : ESPECE_F_R_CAPT_F_FK                                 */
/*==============================================================*/
create  index ESPECE_F_R_CAPT_F_FK on CAPT_F (
C_ESP_F
);

/*==============================================================*/
/* Index : DEV_F_R_CAPT_F_FK                                    */
/*==============================================================*/
create  index DEV_F_R_CAPT_F_FK on CAPT_F (
C_DEVENIR_F
);

/*==============================================================*/
/* Index : CALEE_R_CAPT_F_FK                                    */
/*==============================================================*/
create  index CALEE_R_CAPT_F_FK on CAPT_F (
C_PROG,
N_MAREE,
D_OBS,
H_OBS,
ID_CALEE
);

/*==============================================================*/
/* Index : RAISON_REJET_R_CAPT_F_FK                             */
/*==============================================================*/
create  index RAISON_REJET_R_CAPT_F_FK on CAPT_F (
C_RAISON_REJET
);

/*==============================================================*/
/* Table : CAPT_F_CC                                            */
/*==============================================================*/
create table CAPT_F_CC (
   C_PROG               NUMERIC(2)           not null,
   N_MAREE              NUMERIC(4)           not null,
   D_OBS                DATE                 not null,
   H_OBS                DATE                 not null,
   ID_CALEE             NUMERIC(3)           not null,
   C_ESP_F              NUMERIC(3)           not null,
   NO_ESP_F_CC          NUMERIC(3)           not null,
   C_RAISON_REJET       NUMERIC(2)           null,
   C_OCEAN              NUMERIC(2)           not null,
   LON_C_ESP_F          NUMERIC(3)           not null,
   ANNEE_R_FAUNE_ASSOC  INT4                 not null,
   C_DEVENIR_F          NUMERIC(2)           null,
   V_POIDS_ESTIM_F_CC   NUMERIC(5,1)         not null,
   V_NOMBRE_ESTIM_F_CC  NUMERIC(6,1)         not null,
   V_POIDS_MOY_F_CC     NUMERIC(4,1)         not null,
   V_TAILLE_MOY_F_CC    NUMERIC(4,1)         not null,
   L_COM_CAPT_F_CC      VARCHAR(1024)        null,
   constraint PK_CAPT_F_CC primary key (C_PROG, N_MAREE, D_OBS, H_OBS, ID_CALEE, C_ESP_F, NO_ESP_F_CC)
);

comment on table CAPT_F_CC is
'Se reporter à la définition de CAPT_F';

/*==============================================================*/
/* Index : CAPT_F_CC_PK                                         */
/*==============================================================*/
create unique index CAPT_F_CC_PK on CAPT_F_CC (
C_PROG,
N_MAREE,
D_OBS,
H_OBS,
ID_CALEE,
C_ESP_F,
NO_ESP_F_CC
);

/*==============================================================*/
/* Index : CALEE_R_CAPT_F_CC_FK                                 */
/*==============================================================*/
create  index CALEE_R_CAPT_F_CC_FK on CAPT_F_CC (
C_PROG,
N_MAREE,
D_OBS,
H_OBS,
ID_CALEE
);

/*==============================================================*/
/* Index : RAISON_REJET_R_CAPT_F_CC_FK                          */
/*==============================================================*/
create  index RAISON_REJET_R_CAPT_F_CC_FK on CAPT_F_CC (
C_RAISON_REJET
);

/*==============================================================*/
/* Index : DEV_F_R_CAPT_F_CC_FK                                 */
/*==============================================================*/
create  index DEV_F_R_CAPT_F_CC_FK on CAPT_F_CC (
C_DEVENIR_F
);

/*==============================================================*/
/* Index : ESPECE_F_R_CAPT_F_CC_FK                              */
/*==============================================================*/
create  index ESPECE_F_R_CAPT_F_CC_FK on CAPT_F_CC (
C_ESP_F
);

/*==============================================================*/
/* Index : REF_LPF_CAPTURE_F_CC_FK                              */
/*==============================================================*/
create  index REF_LPF_CAPTURE_F_CC_FK on CAPT_F_CC (
C_OCEAN,
LON_C_ESP_F,
ANNEE_R_FAUNE_ASSOC
);

/*==============================================================*/
/* Table : CAPT_T                                               */
/*==============================================================*/
create table CAPT_T (
   C_PROG               NUMERIC(2)           not null,
   N_MAREE              NUMERIC(4)           not null,
   D_OBS                DATE                 not null,
   H_OBS                DATE                 not null,
   ID_CALEE             NUMERIC(3)           not null,
   N_CAPT_T             NUMERIC(3)           not null,
   C_RAISON_REJET       NUMERIC(2)           null,
   C_ESP                NUMERIC(3)           not null,
   C_CAT_P              NUMERIC(2)           not null,
   V_POIDS_THON_CAP     NUMERIC(3)           not null,
   CUVE                 VARCHAR(3)           null,
   F_SUR_PONT           BOOL                 not null,
   F_REJET              BOOL                 not null,
   L_COM_CAPT_T         VARCHAR(1024)        null,
   constraint PK_CAPT_T primary key (C_PROG, N_MAREE, D_OBS, H_OBS, ID_CALEE, N_CAPT_T)
);

comment on table CAPT_T is
'La table issue de cette entité contiendra le détail des captures de thon pour chaque calée positive. L''information majeure est le poids de thons capturés (exprimé en tonnes). Une capture concerne uniquement une espèce. La catégorie de poids de l''espèce est toujours mentionnée (Albacore de 11 à 30 kg par exemple). L''identifiant est complété par un numéro (1, 2, etc.) qui permet préciser au besoin la part de la capture rejetée et celle conservée à bord. Deux flags obligatoires précisent si la capture a été montée à bord et si elle a été rejetée. Le seul attribut facultatif concerne la cuve.';

comment on column CAPT_T.F_SUR_PONT is
'Rejet mis sur le pont
Valeur : 0 non Aucune espèce codée
Valeur : 1 oui  Zero une ou plusieurs espèces codées
Valeur : 9 l''information n''est pas disponible ';

/*==============================================================*/
/* Index : CAPT_T_PK                                            */
/*==============================================================*/
create unique index CAPT_T_PK on CAPT_T (
C_PROG,
N_MAREE,
D_OBS,
H_OBS,
ID_CALEE,
N_CAPT_T
);

/*==============================================================*/
/* Index : CAT_POIDS_R_CAPT_T_FK                                */
/*==============================================================*/
create  index CAT_POIDS_R_CAPT_T_FK on CAPT_T (
C_ESP,
C_CAT_P
);

/*==============================================================*/
/* Index : CALEE_R_CAPT_T_FK                                    */
/*==============================================================*/
create  index CALEE_R_CAPT_T_FK on CAPT_T (
C_PROG,
N_MAREE,
D_OBS,
H_OBS,
ID_CALEE
);

/*==============================================================*/
/* Index : RAISON_REJET_R_CAPT_T_FK                             */
/*==============================================================*/
create  index RAISON_REJET_R_CAPT_T_FK on CAPT_T (
C_RAISON_REJET
);

/*==============================================================*/
/* Table : CAT_BATEAU                                           */
/*==============================================================*/
create table CAT_BATEAU (
   C_CAT_B              NUMERIC(2)           not null,
   L_JAUGE              VARCHAR(64)          not null,
   L_CAPAC              VARCHAR(64)          not null,
   constraint PK_CAT_BATEAU primary key (C_CAT_B)
);

comment on table CAT_BATEAU is
'Les catégories de taille des bateaux exprimées en jauge et capacité de transport.

Exemple :

1   - 95 TX    20 tonnes
2   95 - 189 TX    40 tonnes
3   190 - 299 TX    90 tonnes
4   300- 449 TX    200 - 400 tonnes
5   450 - 749 TX    401 - 600 tonnes
...

Note : Un tonneau = 2.83 m3';

comment on column CAT_BATEAU.C_CAT_B is
'Code numérique de la catégorie de bateau';

comment on column CAT_BATEAU.L_JAUGE is
'Jauge exprimée en tonneaux';

comment on column CAT_BATEAU.L_CAPAC is
'Capacité exprimée en tonnes';

/*==============================================================*/
/* Index : CAT_BATEAU_PK                                        */
/*==============================================================*/
create unique index CAT_BATEAU_PK on CAT_BATEAU (
C_CAT_B
);

/*==============================================================*/
/* Table : CAT_POIDS                                            */
/*==============================================================*/
create table CAT_POIDS (
   C_ESP                NUMERIC(3)           not null,
   C_CAT_P              NUMERIC(2)           not null,
   L_CAT_P              VARCHAR(32)          not null,
   constraint PK_CAT_POIDS primary key (C_ESP, C_CAT_P)
);

/*==============================================================*/
/* Index : CAT_POIDS_PK                                         */
/*==============================================================*/
create unique index CAT_POIDS_PK on CAT_POIDS (
C_ESP,
C_CAT_P
);

/*==============================================================*/
/* Index : ESPECE_T_R_CAT_POIDS_FK                              */
/*==============================================================*/
create  index ESPECE_T_R_CAT_POIDS_FK on CAT_POIDS (
C_ESP
);

/*==============================================================*/
/* Table : COUP_NUL                                             */
/*==============================================================*/
create table COUP_NUL (
   C_R_COUP_NUL         NUMERIC(2)           not null,
   L_R_COUP_NUL         VARCHAR(64)          not null,
   constraint PK_COUP_NUL primary key (C_R_COUP_NUL)
);

comment on table COUP_NUL is
'Pourquoi un coup de pêche devient nul :

0	Inconnue
1	Poisson ayant coulé
2	Poisson allant trop vite
3	Courant trop fort
4	Trop de poisson


';

/*==============================================================*/
/* Index : COUP_NUL_PK                                          */
/*==============================================================*/
create unique index COUP_NUL_PK on COUP_NUL (
C_R_COUP_NUL
);

/*==============================================================*/
/* Table : DCP                                                  */
/*==============================================================*/
create table DCP (
   C_PROG               NUMERIC(2)           not null,
   N_MAREE              NUMERIC(4)           not null,
   D_OBS                DATE                 not null,
   H_OBS                DATE                 not null,
   N_OBJET              NUMERIC(3)           not null,
   C_NATURE_DCP         NUMERIC(2)           null,
   C_DEVENIR_DCP        NUMERIC(2)           null,
   C_OPERA_OBJ          NUMERIC(2)           null,
   NB_JOURS             NUMERIC(5)           null,
   L_COM_O              VARCHAR(1024)        null,
   L_SUPPLY_O           VARCHAR(32)          null,
   constraint PK_DCP primary key (C_PROG, N_MAREE, D_OBS, H_OBS, N_OBJET)
);

comment on table DCP is
'La table DCP issue de l''entité Objet flottant stocke les occurrences de DCP mis à l''eau, visités, etc.
L''objet flottant est spécifié par un type, une opération, et un devenir. Il dispose d''un seul attribut quantitatif ''Nb jours objet à l''eau''';

/*==============================================================*/
/* Index : DCP_PK                                               */
/*==============================================================*/
create unique index DCP_PK on DCP (
C_PROG,
N_MAREE,
D_OBS,
H_OBS,
N_OBJET
);

/*==============================================================*/
/* Index : ACTIVIT_R_DCP_FK                                     */
/*==============================================================*/
create  index ACTIVIT_R_DCP_FK on DCP (
C_PROG,
N_MAREE,
D_OBS,
H_OBS
);

/*==============================================================*/
/* Index : TYPE_DCP_R_DCP_FK                                    */
/*==============================================================*/
create  index TYPE_DCP_R_DCP_FK on DCP (
C_NATURE_DCP
);

/*==============================================================*/
/* Index : OPERA_DCP_R_DCP_FK                                   */
/*==============================================================*/
create  index OPERA_DCP_R_DCP_FK on DCP (
C_OPERA_OBJ
);

/*==============================================================*/
/* Index : DEV_DCP_R_DCP_FK                                     */
/*==============================================================*/
create  index DEV_DCP_R_DCP_FK on DCP (
C_DEVENIR_DCP
);

/*==============================================================*/
/* Table : DEV_DCP                                              */
/*==============================================================*/
create table DEV_DCP (
   C_DEVENIR_DCP        NUMERIC(2)           not null,
   L_DEVENIR_DCP        VARCHAR(64)          not null,
   constraint PK_DEV_DCP primary key (C_DEVENIR_DCP)
);

comment on table DEV_DCP is
'Devenir du DCP 

1	Abandonné
2	Pose d''une balise
3	Remonté à bord
4	Détruit

...';

/*==============================================================*/
/* Index : DEV_DCP_PK                                           */
/*==============================================================*/
create unique index DEV_DCP_PK on DEV_DCP (
C_DEVENIR_DCP
);

/*==============================================================*/
/* Table : DEV_F                                                */
/*==============================================================*/
create table DEV_F (
   C_DEVENIR_F          NUMERIC(2)           not null,
   L_DEVENIR_F          VARCHAR(128)         not null,
   constraint PK_DEV_F primary key (C_DEVENIR_F)
);

comment on table DEV_F is
'Détermine le devenir des captures des espèces de la faune associée 

1	Échappe du filet (pour requin-baleine et cétacés)
2	Sortie vivant du filet (pour requin-baleine et cétacés)
3	Sortie mort du filet (pour requin-baleine et cétacés)
4	Rejeté vivant à la mer
...
';

/*==============================================================*/
/* Index : DEV_F_PK                                             */
/*==============================================================*/
create unique index DEV_F_PK on DEV_F (
C_DEVENIR_F
);

/*==============================================================*/
/* Table : ECHANT_F                                             */
/*==============================================================*/
create table ECHANT_F (
   C_PROG               NUMERIC(2)           not null,
   N_MAREE              NUMERIC(4)           not null,
   D_OBS                DATE                 not null,
   H_OBS                DATE                 not null,
   ID_CALEE             NUMERIC(3)           not null,
   ID_ECHANT_F          NUMERIC(2)           not null,
   L_COM_ECH_F          VARCHAR(1024)        null,
   constraint PK_ECHANT_F primary key (C_PROG, N_MAREE, D_OBS, H_OBS, ID_CALEE, ID_ECHANT_F)
);

comment on table ECHANT_F is
'Pour les espèces autres que les thonidés, une calée peut faire l''objet d''un échantillon. La table dérivée contient l''identifiant de l''échantillon. Cette table est essentiellement le symétrique de ECHANT_T - elle est à priori superflue ...';

/*==============================================================*/
/* Index : ECHANT_F_PK                                          */
/*==============================================================*/
create unique index ECHANT_F_PK on ECHANT_F (
C_PROG,
N_MAREE,
D_OBS,
H_OBS,
ID_CALEE,
ID_ECHANT_F
);

/*==============================================================*/
/* Index : CALEE_R_ECHANT_F_FK                                  */
/*==============================================================*/
create  index CALEE_R_ECHANT_F_FK on ECHANT_F (
C_PROG,
N_MAREE,
D_OBS,
H_OBS,
ID_CALEE
);

/*==============================================================*/
/* Table : ECHANT_T                                             */
/*==============================================================*/
create table ECHANT_T (
   C_PROG               NUMERIC(2)           not null,
   N_MAREE              NUMERIC(4)           not null,
   D_OBS                DATE                 not null,
   H_OBS                DATE                 not null,
   ID_CALEE             NUMERIC(3)           not null,
   ID_ECHANT_T          NUMERIC(2)           not null,
   C_CRP                CHAR(1)              null,
   L_COMMENT_ECH        VARCHAR(1024)        null,
   constraint PK_ECHANT_T primary key (C_PROG, N_MAREE, D_OBS, H_OBS, ID_CALEE, ID_ECHANT_T)
);

comment on table ECHANT_T is
'Une calée peut faire l''objet d''un ou plusieurs échantillons de taille. La table dérivée contient essentiellement l''identifiant de l''échantillon et un code qui spécifie s''il s''agit de thons rejetés ou non. Dans la version 2008/04 des formulaires du bordereau C1 il est stipulé que seul les rejets peuvent faire l''objet d''échantillon (Banyuls 2008). La colonne reste nécessaire pour le stockage des données saisies précédemment. L''échantillon est monospécifique, les espèces possibles sont : YFT, BET, SKJ, Auxides, et ou Ravil.


';

/*==============================================================*/
/* Index : ECHANT_T_PK                                          */
/*==============================================================*/
create unique index ECHANT_T_PK on ECHANT_T (
C_PROG,
N_MAREE,
D_OBS,
H_OBS,
ID_CALEE,
ID_ECHANT_T
);

/*==============================================================*/
/* Index : CALEE_R_ECHANT_T_FK                                  */
/*==============================================================*/
create  index CALEE_R_ECHANT_T_FK on ECHANT_T (
C_PROG,
N_MAREE,
D_OBS,
H_OBS,
ID_CALEE
);

/*==============================================================*/
/* Table : ESPECE_F                                             */
/*==============================================================*/
create table ESPECE_F (
   C_ESP_F              NUMERIC(3)           not null,
   C_GR_ESP_F           NUMERIC(2)           not null,
   C_ESP_F_3L           VARCHAR(3)           not null,
   L_ESP_FS             VARCHAR(64)          not null,
   L_ES_ESP_F           VARCHAR(64)          null,
   L_UK_ESP_F           VARCHAR(64)          null,
   L_FR_ESP_F           VARCHAR(64)          null,
   constraint PK_ESPECE_F primary key (C_ESP_F)
);

comment on table ESPECE_F is
'Les espèces de de la faune associée 
(code, groupe, code A3, nom scientifique, nom SP, nom GB, nom FR)

101	1	BLM	Makaira indica	Aguja negra	Black marlin	Makaire noir
102	1	BUM	Makaira nigricans	Aguja azul del Atlántico	Atlantic blue marlin	Makaire bleu
103	1	FIS	xx			Famille des Istiophoridés
104	1	SAI	Istiophorus albicans	Pez vela del Atlántico	Atlantic sailfish	Voilier de l''Atlantique
105	1	SAP	Istiophorus platypterus	Pez vela del Indo-Pacífico	Indo-Pacific sailfish	Voilier de l''Océan Indien
106	1	SHS	Tetrapturus angustirostris	Marlín trompa corta	Shortbill spearfish	Makaire à rostre court
107	1	SPF	Tetrapturus pfluegeri	Aguja picuda	Longbill spearfish	Makaire bécune
108	1	STM	Tetrapturus audax	Marlín rayado	Striped marlin	Marlin rayé
109	1	SWO	Xiphias gladius	Pez espada	Swordfish	Espadon
110	1	WHM	Tetrapterus albidus	Aguja blanca		Makaire blanc
201	2	ANA	Aetobatus narinari	Chucho pintado	Spotted eagle ray	Aigle de mer léopard
202	2	APE	Alopias pelagicus	Zorro pelágico	Pelagic thresher	Renard pélagique
203	2	ASU	Alopias superciliosus	Zorro ojón	Bigeye thresher	Renard à gros yeux
204	2	AVU	Alopias vulpinus	Zorro	Thresher	Renard

...
';

/*==============================================================*/
/* Index : ESPECE_F_PK                                          */
/*==============================================================*/
create unique index ESPECE_F_PK on ESPECE_F (
C_ESP_F
);

/*==============================================================*/
/* Index : GR_ESP_F_R_ESPECE_F_FK                               */
/*==============================================================*/
create  index GR_ESP_F_R_ESPECE_F_FK on ESPECE_F (
C_GR_ESP_F
);

/*==============================================================*/
/* Table : ESPECE_T                                             */
/*==============================================================*/
create table ESPECE_T (
   C_ESP                NUMERIC(3)           not null,
   C_ESP_3L             VARCHAR(3)           not null,
   L_ESP                VARCHAR(32)          not null,
   L_ESP_S              VARCHAR(32)          null,
   constraint PK_ESPECE_T primary key (C_ESP)
);

comment on table ESPECE_T is
'Les espèces de thon capturées, mesurées et ou observés :

1	YFT	Albacore	Thunnus albacares
2	SKJ	Listao	Katsuwonus pelamis
3	BET	Patudo	Thunnus obesus
4	ALB	Germon	Thunnus alalunga
...';

/*==============================================================*/
/* Index : ESPECE_T_PK                                          */
/*==============================================================*/
create unique index ESPECE_T_PK on ESPECE_T (
C_ESP
);

/*==============================================================*/
/* Table : ESP_DCP                                              */
/*==============================================================*/
create table ESP_DCP (
   C_ESP_F              NUMERIC(3)           not null,
   C_PROG               NUMERIC(2)           not null,
   N_MAREE              NUMERIC(4)           not null,
   D_OBS                DATE                 not null,
   H_OBS                DATE                 not null,
   N_OBJET              NUMERIC(3)           not null,
   C_STATUT_ESP_DCP     NUMERIC(1)           not null,
   V_NOMBRE_ESP_DCP     NUMERIC(5)           not null,
   constraint PK_ESP_DCP primary key (C_PROG, N_MAREE, D_OBS, H_OBS, C_ESP_F, N_OBJET, C_STATUT_ESP_DCP)
);

comment on table ESP_DCP is
'L''entité Esp DCP (Table ESP_DCP) précise les espèces de la faune associée observée sous le DCP lors de sa visite ou de sa relève. Le statut de l''espèce est précisé ( libre, maillée vivante, etc.  ) et une estimation du nombre d''individus est donnée. ';

/*==============================================================*/
/* Index : ESP_DCP_PK                                           */
/*==============================================================*/
create unique index ESP_DCP_PK on ESP_DCP (
C_PROG,
N_MAREE,
D_OBS,
H_OBS,
C_ESP_F,
N_OBJET,
C_STATUT_ESP_DCP
);

/*==============================================================*/
/* Index : ESPECE_F_R_ESP_DCP_FK                                */
/*==============================================================*/
create  index ESPECE_F_R_ESP_DCP_FK on ESP_DCP (
C_ESP_F
);

/*==============================================================*/
/* Index : DCP_R_ESP_DCP_FK                                     */
/*==============================================================*/
create  index DCP_R_ESP_DCP_FK on ESP_DCP (
C_PROG,
N_MAREE,
D_OBS,
H_OBS,
N_OBJET
);

/*==============================================================*/
/* Index : STATUT_ESP_DCP_R_ESP_DCP_FK                          */
/*==============================================================*/
create  index STATUT_ESP_DCP_R_ESP_DCP_FK on ESP_DCP (
C_STATUT_ESP_DCP
);

/*==============================================================*/
/* Table : ESTIM_BANC                                           */
/*==============================================================*/
create table ESTIM_BANC (
   C_ESP                NUMERIC(3)           not null,
   C_PROG               NUMERIC(2)           not null,
   N_MAREE              NUMERIC(4)           not null,
   D_OBS                DATE                 not null,
   H_OBS                DATE                 not null,
   ID_CALEE             NUMERIC(3)           not null,
   V_POIDS_ESP_B        NUMERIC(4)           null,
   V_POIDS_ESP_I        NUMERIC(3)           null,
   constraint PK_ESTIM_BANC primary key (C_PROG, N_MAREE, D_OBS, C_ESP, H_OBS, ID_CALEE)
);

comment on table ESTIM_BANC is
'La table ESTIM_BANC donne pour les trois espèces principales (YFT, SKJ, BET) ou l''ensemble des espèce une estimation de la taille du banc. Cette estimation est donnée en poids (tonnes) ou en poids moyen (kg). Le cas ''toutes espèces'' est traité comme s''il s''agissait d''une espèce individuelle. La table ESPECE_T est doté d''une occurence ''Toutes espèces''.
';

/*==============================================================*/
/* Index : ESTIM_BANC_PK                                        */
/*==============================================================*/
create unique index ESTIM_BANC_PK on ESTIM_BANC (
C_PROG,
N_MAREE,
D_OBS,
C_ESP,
H_OBS,
ID_CALEE
);

/*==============================================================*/
/* Index : ESP_T_R_ESTIM_B_FK                                   */
/*==============================================================*/
create  index ESP_T_R_ESTIM_B_FK on ESTIM_BANC (
C_ESP
);

/*==============================================================*/
/* Index : CALEE_R_ESTIM_BANC_FK                                */
/*==============================================================*/
create  index CALEE_R_ESTIM_BANC_FK on ESTIM_BANC (
C_PROG,
N_MAREE,
D_OBS,
H_OBS,
ID_CALEE
);

/*==============================================================*/
/* Table : ESTIM_BANC_OBJ                                       */
/*==============================================================*/
create table ESTIM_BANC_OBJ (
   C_PROG               NUMERIC(2)           not null,
   N_MAREE              NUMERIC(4)           not null,
   D_OBS                DATE                 not null,
   H_OBS                DATE                 not null,
   N_OBJET              NUMERIC(3)           not null,
   C_ESP                NUMERIC(3)           not null,
   V_POIDS_ESP_O        NUMERIC(4)           null,
   constraint PK_ESTIM_BANC_OBJ primary key (C_PROG, N_MAREE, D_OBS, H_OBS, N_OBJET, C_ESP)
);

/*==============================================================*/
/* Index : ESTIM_BANC_OBJ_PK                                    */
/*==============================================================*/
create unique index ESTIM_BANC_OBJ_PK on ESTIM_BANC_OBJ (
C_PROG,
N_MAREE,
D_OBS,
H_OBS,
N_OBJET,
C_ESP
);

/*==============================================================*/
/* Index : DCP_R_ESTIM_BANC_OBJ_FK                              */
/*==============================================================*/
create  index DCP_R_ESTIM_BANC_OBJ_FK on ESTIM_BANC_OBJ (
C_PROG,
N_MAREE,
D_OBS,
H_OBS,
N_OBJET
);

/*==============================================================*/
/* Index : ESP_T_R_ESTIM_BO_FK                                  */
/*==============================================================*/
create  index ESP_T_R_ESTIM_BO_FK on ESTIM_BANC_OBJ (
C_ESP
);

/*==============================================================*/
/* Table : GR_ESP_F                                             */
/*==============================================================*/
create table GR_ESP_F (
   C_GR_ESP_F           NUMERIC(2)           not null,
   L_G_ESP              VARCHAR(32)          null,
   constraint PK_GR_ESP_F primary key (C_GR_ESP_F)
);

comment on table GR_ESP_F is
'Les groupes d''espèces de la faune associée

1	Poissons Porte_Epée
2	Sélaciens
3	Autres poissons
4	Tortues

...';

/*==============================================================*/
/* Index : GR_ESP_F_PK                                          */
/*==============================================================*/
create unique index GR_ESP_F_PK on GR_ESP_F (
C_GR_ESP_F
);

/*==============================================================*/
/* Table : LONPOI_F                                             */
/*==============================================================*/
create table LONPOI_F (
   C_OCEAN              NUMERIC(2)           not null,
   C_ESP_F              NUMERIC(3)           not null,
   ANNEE_R_FAUNE_ASSOC  INT4                 not null,
   RPL_A_F              NUMERIC(10,9)        not null,
   RPL_B_F              NUMERIC(6,5)         not null,
   constraint PK_LONPOI_F primary key (C_OCEAN, C_ESP_F, ANNEE_R_FAUNE_ASSOC)
);

comment on table LONPOI_F is
'Les relations taille-poids : paramètres des équations longueur - poids par espèce et par océan  (faune associée)

Fomule : W = a*L**b

';

/*==============================================================*/
/* Index : LONPOI_F_PK                                          */
/*==============================================================*/
create unique index LONPOI_F_PK on LONPOI_F (
C_OCEAN,
C_ESP_F,
ANNEE_R_FAUNE_ASSOC
);

/*==============================================================*/
/* Index : OCEAN_R_LONPOI_F_FK                                  */
/*==============================================================*/
create  index OCEAN_R_LONPOI_F_FK on LONPOI_F (
C_OCEAN
);

/*==============================================================*/
/* Index : ESPECE_F_R_LONPOI_F_FK                               */
/*==============================================================*/
create  index ESPECE_F_R_LONPOI_F_FK on LONPOI_F (
C_ESP_F
);

/*==============================================================*/
/* Table : LONPOI_T                                             */
/*==============================================================*/
create table LONPOI_T (
   C_OCEAN              NUMERIC(2)           not null,
   C_ESP                NUMERIC(3)           not null,
   ANNEE_R_THON         INT4                 not null,
   RPL_A_T              NUMERIC(10,9)        not null,
   RPL_B_T              NUMERIC(6,5)         not null,
   constraint PK_LONPOI_T primary key (C_OCEAN, C_ESP, ANNEE_R_THON)
);

comment on table LONPOI_T is
'Les relations taille-poids : paramètres des équations longueur - poids par espèce et par océan  (Thons)

Fomule : W = a*L**b

Exemples pour YFT dans l''océan Atlantique (1) et  Indien (2) :
Espèce		Océan		a		b
1		1		0.000021527	2.976
1		2		0.00000748	3.2526';

/*==============================================================*/
/* Index : LONPOI_T_PK                                          */
/*==============================================================*/
create unique index LONPOI_T_PK on LONPOI_T (
C_OCEAN,
C_ESP,
ANNEE_R_THON
);

/*==============================================================*/
/* Index : OCEAN_R_LONPOI_T_FK                                  */
/*==============================================================*/
create  index OCEAN_R_LONPOI_T_FK on LONPOI_T (
C_OCEAN
);

/*==============================================================*/
/* Index : ESPECE_T_R_LONPOI_T_FK                               */
/*==============================================================*/
create  index ESPECE_T_R_LONPOI_T_FK on LONPOI_T (
C_ESP
);

/*==============================================================*/
/* Table : MAREE                                                */
/*==============================================================*/
create table MAREE (
   C_PROG               NUMERIC(2)           not null,
   N_MAREE              NUMERIC(4)           not null,
   C_BAT                NUMERIC(4)           not null,
   ID_OBSERVATEUR       NUMERIC(3)           not null,
   C_OCEAN              NUMERIC(2)           null,
   ID_SENNE             NUMERIC(2)           not null,
   D_DEBUT              DATE                 not null,
   D_FIN                DATE                 not null,
   L_ORGANISME          VARCHAR(128)         null,
   D_DBQ                DATE                 null,
   L_COM_MAREE          VARCHAR(1024)        null,
   NIVEAU_VERIF_MAREE   INT2                 null,
   constraint PK_MAREE primary key (C_PROG, N_MAREE)
);

comment on table MAREE is
'La marée  d''un bateau décrit l''intervalle de temps entre une date départ et la date d''arrivée au port pour débarquement ou relève d''équipage.';

/*==============================================================*/
/* Index : MAREE_PK                                             */
/*==============================================================*/
create unique index MAREE_PK on MAREE (
C_PROG,
N_MAREE
);

/*==============================================================*/
/* Index : BATEAU_R_MAREE_FK                                    */
/*==============================================================*/
create  index BATEAU_R_MAREE_FK on MAREE (
C_BAT
);

/*==============================================================*/
/* Index : OBSERVATEUR_R_MAREE_FK                               */
/*==============================================================*/
create  index OBSERVATEUR_R_MAREE_FK on MAREE (
ID_OBSERVATEUR
);

/*==============================================================*/
/* Index : PROGRAMME_R_MAREE_FK                                 */
/*==============================================================*/
create  index PROGRAMME_R_MAREE_FK on MAREE (
C_PROG
);

/*==============================================================*/
/* Index : SENNE_R_MAREE_FK                                     */
/*==============================================================*/
create  index SENNE_R_MAREE_FK on MAREE (
ID_SENNE
);

/*==============================================================*/
/* Index : OCEAN_R_MAREE_FK                                     */
/*==============================================================*/
create  index OCEAN_R_MAREE_FK on MAREE (
C_OCEAN
);

/*==============================================================*/
/* Table : MODE_DETECT                                          */
/*==============================================================*/
create table MODE_DETECT (
   C_MODE_DETECT        NUMERIC(2)           not null,
   L_MODE_DETECT        VARCHAR(64)          not null,
   constraint PK_MODE_DETECT primary key (C_MODE_DETECT)
);

comment on table MODE_DETECT is
'Spécifie le mode de détection du banc de thons 

0	Mode de détection inconnu
1	Oiel nu
2	Jumelles
3	Radar oiseaux
4	Radar
5	Sonar
 ...';

/*==============================================================*/
/* Index : MODE_DETECT_PK                                       */
/*==============================================================*/
create unique index MODE_DETECT_PK on MODE_DETECT (
C_MODE_DETECT
);

/*==============================================================*/
/* Table : NON_COUP_SENNE                                       */
/*==============================================================*/
create table NON_COUP_SENNE (
   C_NON_COUP_DE_SENNE  NUMERIC(2)           not null,
   L_NON_COUP_DE_SENNE  VARCHAR(128)         not null,
   constraint PK_NON_COUP_SENNE primary key (C_NON_COUP_DE_SENNE)
);

comment on table NON_COUP_SENNE is
'Détermine la raison pour laquelle la senne n''a pas été larguée

0	Rien à signaler ( pas d''observat
1	Banc trop petit
2	Poissons trop petits ( poids, ta
3	Par décision de l''armateur ( ex:
4	Se déplace trop rapidement
5	Le poisson plonge avant la calée

...';

/*==============================================================*/
/* Index : NON_COUP_SENNE_PK                                    */
/*==============================================================*/
create unique index NON_COUP_SENNE_PK on NON_COUP_SENNE (
C_NON_COUP_DE_SENNE
);

/*==============================================================*/
/* Table : OBSERVATEUR                                          */
/*==============================================================*/
create table OBSERVATEUR (
   ID_OBSERVATEUR       NUMERIC(3)           not null,
   NOM_OBSERVATEUR      VARCHAR(32)          not null,
   PRENOM_OBSERVATEUR   VARCHAR(32)          not null,
   constraint PK_OBSERVATEUR primary key (ID_OBSERVATEUR)
);

comment on table OBSERVATEUR is
'Seuls les noms et prénoms des observateurs sont archivés';

/*==============================================================*/
/* Index : OBSERVATEUR_PK                                       */
/*==============================================================*/
create unique index OBSERVATEUR_PK on OBSERVATEUR (
ID_OBSERVATEUR
);

/*==============================================================*/
/* Table : OCEAN                                                */
/*==============================================================*/
create table OCEAN (
   C_OCEAN              NUMERIC(2)           not null,
   L_OCEAN              VARCHAR(64)          not null,
   constraint PK_OCEAN primary key (C_OCEAN)
);

comment on table OCEAN is
'Codification des grandes zones océaniques. 

Exemple : 

Code         Libellé

1	Atlantique
2	Indien

...';

/*==============================================================*/
/* Index : OCEAN_PK                                             */
/*==============================================================*/
create unique index OCEAN_PK on OCEAN (
C_OCEAN
);

/*==============================================================*/
/* Table : OPERA_DCP                                            */
/*==============================================================*/
create table OPERA_DCP (
   C_OPERA_OBJ          NUMERIC(2)           not null,
   L_OPERA_OBJ          VARCHAR(64)          not null,
   constraint PK_OPERA_DCP primary key (C_OPERA_OBJ)
);

comment on table OPERA_DCP is
'Codes des opérations possibles sur un DCP

1	Mise à l''eau
2	Visite/Rencontre
3	Pêche
4	Récupération sans pêche

...';

/*==============================================================*/
/* Index : OPERA_DCP_PK                                         */
/*==============================================================*/
create unique index OPERA_DCP_PK on OPERA_DCP (
C_OPERA_OBJ
);

/*==============================================================*/
/* Table : PAYS                                                 */
/*==============================================================*/
create table PAYS (
   C_PAYS               NUMERIC(3)           not null,
   L_PAYS               VARCHAR(64)          not null,
   constraint PK_PAYS primary key (C_PAYS)
);

comment on table PAYS is
'Le pavillon du bateau. 

Exemples :
1     FR       France
2     SN      Sénégal
3     CI        Côte d''Ivoire
4     SP      Espagne
...';

comment on column PAYS.C_PAYS is
'Code numérique du pays';

comment on column PAYS.L_PAYS is
'Nom du pays';

/*==============================================================*/
/* Index : PAYS_PK                                              */
/*==============================================================*/
create unique index PAYS_PK on PAYS (
C_PAYS
);

/*==============================================================*/
/* Table : PROGRAMME                                            */
/*==============================================================*/
create table PROGRAMME (
   C_PROG               NUMERIC(2)           not null,
   L_PROG               VARCHAR(64)          null,
   constraint PK_PROGRAMME primary key (C_PROG)
);

comment on table PROGRAMME is
'La table programme historise les cadres successifs dans les lesquels des observateurs ont été embarqués à bord des thonniers

1	DCR (Data Collection Regulation)
2	Patudo
3	Moratoire
4	SFA
5	Embarquement IRD
9	Indéterminé
';

/*==============================================================*/
/* Index : PROGRAMME_PK                                         */
/*==============================================================*/
create unique index PROGRAMME_PK on PROGRAMME (
C_PROG
);

/*==============================================================*/
/* Table : RAISON_REJET                                         */
/*==============================================================*/
create table RAISON_REJET (
   C_RAISON_REJET       NUMERIC(2)           not null,
   L_RAISON_REJET       VARCHAR(32)          not null,
   constraint PK_RAISON_REJET primary key (C_RAISON_REJET)
);

comment on table RAISON_REJET is
'Codification de la raison du rejet :

1	Espèce
2	Taille
3	Cuves pleines


';

/*==============================================================*/
/* Index : RAISON_REJET_PK                                      */
/*==============================================================*/
create unique index RAISON_REJET_PK on RAISON_REJET (
C_RAISON_REJET
);

/*==============================================================*/
/* Table : ROUTE                                                */
/*==============================================================*/
create table ROUTE (
   C_PROG               NUMERIC(2)           not null,
   N_MAREE              NUMERIC(4)           not null,
   D_OBS                DATE                 not null,
   LOCH_MATIN           NUMERIC(6)           null,
   LOCH_SOIR            NUMERIC(6)           null,
   L_COM_ROUTE          VARCHAR(1024)        null,
   NIVEAU_VERIF_ROUTE   INT2                 null,
   constraint PK_ROUTE primary key (C_PROG, N_MAREE, D_OBS)
);

comment on table ROUTE is
'La route donne journellement le loch du matin et le loch du soir. Il est ainsi facile de déterminer la distance journalière parcourue par le bateau.

La mesure de la vitesse ( V ) est indispensable à la navigation. Elle permet par intégration (?D = V.?t) de connaître la distance (D) parcourue pendant un temps t. Cette intégration est automatiquement effectuée par les lochs modernes qui comportent donc un compteur de distance.';

/*==============================================================*/
/* Index : ROUTE_PK                                             */
/*==============================================================*/
create unique index ROUTE_PK on ROUTE (
C_PROG,
N_MAREE,
D_OBS
);

/*==============================================================*/
/* Index : MAREE_R_ROUTE_FK                                     */
/*==============================================================*/
create  index MAREE_R_ROUTE_FK on ROUTE (
C_PROG,
N_MAREE
);

/*==============================================================*/
/* Table : SENNE                                                */
/*==============================================================*/
create table SENNE (
   ID_SENNE             NUMERIC(2)           not null,
   LONGUEUR_SENNE       NUMERIC(5)           not null,
   CHUTE_SENNE          NUMERIC(4)           not null,
   POIDS_LEST_SENNE     NUMERIC(4)           null,
   constraint PK_SENNE primary key (ID_SENNE)
);

comment on table SENNE is
'Il s''agit de mémoriser quelques caractèristiques des sennes exploitées (longueur, chutte et poids du leste)
Le code 9 correspond au cas où aucun des pamamètres n''est connu.';

/*==============================================================*/
/* Index : SENNE_PK                                             */
/*==============================================================*/
create unique index SENNE_PK on SENNE (
ID_SENNE
);

/*==============================================================*/
/* Table : STATUT_ESP_DCP                                       */
/*==============================================================*/
create table STATUT_ESP_DCP (
   C_STATUT_ESP_DCP     NUMERIC(1)           not null,
   L_STATUT_ESP_DCP     VARCHAR(32)          not null,
   constraint PK_STATUT_ESP_DCP primary key (C_STATUT_ESP_DCP)
);

comment on table STATUT_ESP_DCP is
'Indique l''état de l''espèce (faune associée) observée sous le DCP 

1	maillée vivante
2	maillée morte
3	libre
';

/*==============================================================*/
/* Index : STATUT_ESP_DCP_PK                                    */
/*==============================================================*/
create unique index STATUT_ESP_DCP_PK on STATUT_ESP_DCP (
C_STATUT_ESP_DCP
);

/*==============================================================*/
/* Table : SYST_OBS                                             */
/*==============================================================*/
create table SYST_OBS (
   C_SYST_OBS           NUMERIC(2)           not null,
   L_SYST_OBS           VARCHAR(128)         not null,
   constraint PK_SYST_OBS primary key (C_SYST_OBS)
);

comment on table SYST_OBS is
'Codes des système observés

0	Aucun système
1	Matte ( pas de précision sur le type de banc )
2	Balbaya, sardara, brisant ou rou
3	Thons en profondeur
4	Oiseaux
5	Epave non balisée
6	Epave balisée
7	Charogne
8	Charogne balisée

...';

/*==============================================================*/
/* Index : SYST_OBS_PK                                          */
/*==============================================================*/
create unique index SYST_OBS_PK on SYST_OBS (
C_SYST_OBS
);

/*==============================================================*/
/* Table : TAILLE_F                                             */
/*==============================================================*/
create table TAILLE_F (
   C_PROG               NUMERIC(2)           not null,
   N_MAREE              NUMERIC(4)           not null,
   D_OBS                DATE                 not null,
   H_OBS                DATE                 not null,
   ID_CALEE             NUMERIC(3)           not null,
   ID_ECHANT_F          NUMERIC(2)           not null,
   C_ESP_F              NUMERIC(3)           not null,
   N_ESP_F              NUMERIC(3)           not null,
   V_LONG_F             NUMERIC(5)           not null,
   C_SEXE_F             NUMERIC(1)           null,
   L_REF_PHOTO          VARCHAR(128)         null,
   constraint PK_TAILLE_F primary key (C_PROG, N_MAREE, D_OBS, H_OBS, ID_CALEE, ID_ECHANT_F, C_ESP_F, N_ESP_F)
);

comment on table TAILLE_F is
'Comparée à la table TAILLE_T, la table TAILLE_F est organisée différemment. Pour une espèce donnée,  une longueur (exprimée en cm) est obligatoirement stipulée. Le sexe et la référence à une photo sont des colonnes optionnelles. La logueur V_LONG_F  (L1) dépend de l''espèce mesurée, Cf. l''entête du borderau de saisie C2. ';

/*==============================================================*/
/* Index : TAILLE_F_PK                                          */
/*==============================================================*/
create unique index TAILLE_F_PK on TAILLE_F (
C_PROG,
N_MAREE,
D_OBS,
H_OBS,
ID_CALEE,
ID_ECHANT_F,
C_ESP_F,
N_ESP_F
);

/*==============================================================*/
/* Index : ECHANT_F_R_TAILLE_F_FK                               */
/*==============================================================*/
create  index ECHANT_F_R_TAILLE_F_FK on TAILLE_F (
C_PROG,
N_MAREE,
D_OBS,
H_OBS,
ID_CALEE,
ID_ECHANT_F
);

/*==============================================================*/
/* Index : ESPECE_F_R_TAILLE_F_FK                               */
/*==============================================================*/
create  index ESPECE_F_R_TAILLE_F_FK on TAILLE_F (
C_ESP_F
);

/*==============================================================*/
/* Table : TAILLE_T                                             */
/*==============================================================*/
create table TAILLE_T (
   C_ESP                NUMERIC(3)           not null,
   C_PROG               NUMERIC(2)           not null,
   N_MAREE              NUMERIC(4)           not null,
   D_OBS                DATE                 not null,
   H_OBS                DATE                 not null,
   ID_CALEE             NUMERIC(3)           not null,
   ID_ECHANT_T          NUMERIC(2)           not null,
   C_LD1LF              NUMERIC(1)           not null,
   V_LONG_T             NUMERIC(6,1)         not null,
   V_EFF_T              NUMERIC(3)           not null,
   constraint PK_TAILLE_T primary key (C_PROG, N_MAREE, D_OBS, H_OBS, ID_CALEE, C_ESP, ID_ECHANT_T, C_LD1LF, V_LONG_T)
);

comment on table TAILLE_T is
'L''entité Taille thon décrit l''ensemble des effectifs par classe de taille d''une espèce de thonidé échantillonnée. Seules les classes d''effectif strictement positif sont archivées. Le type de mesure est spécifié LD1 ou LF.';

/*==============================================================*/
/* Index : TAILLE_T_PK                                          */
/*==============================================================*/
create unique index TAILLE_T_PK on TAILLE_T (
C_PROG,
N_MAREE,
D_OBS,
H_OBS,
ID_CALEE,
C_ESP,
ID_ECHANT_T,
C_LD1LF,
V_LONG_T
);

/*==============================================================*/
/* Index : ESPECE_T_R_TAILLE_T_FK                               */
/*==============================================================*/
create  index ESPECE_T_R_TAILLE_T_FK on TAILLE_T (
C_ESP
);

/*==============================================================*/
/* Index : ECHANT_T_R_TAILLE_T_FK                               */
/*==============================================================*/
create  index ECHANT_T_R_TAILLE_T_FK on TAILLE_T (
C_PROG,
N_MAREE,
D_OBS,
H_OBS,
ID_CALEE,
ID_ECHANT_T
);

/*==============================================================*/
/* Table : TYPE_BALISE                                          */
/*==============================================================*/
create table TYPE_BALISE (
   C_BALISE             NUMERIC(2)           not null,
   L_BALISE             VARCHAR(32)          not null,
   constraint PK_TYPE_BALISE primary key (C_BALISE)
);

comment on table TYPE_BALISE is
'Codes des types de balise

1	Radiogoniomètre
2	Radiogonio + GPS
3	GPS SHERPE
4	Satellite + échosondeur

...';

/*==============================================================*/
/* Index : TYPE_BALISE_PK                                       */
/*==============================================================*/
create unique index TYPE_BALISE_PK on TYPE_BALISE (
C_BALISE
);

/*==============================================================*/
/* Table : TYPE_BATEAU                                          */
/*==============================================================*/
create table TYPE_BATEAU (
   C_TYP_B              NUMERIC(2)           not null,
   L_TYP_B              VARCHAR(64)          not null,
   constraint PK_TYPE_BATEAU primary key (C_TYP_B)
);

comment on table TYPE_BATEAU is
'Définit plus finement l''armement utilisé pour la pêche.

Exemples :
1 xxxx yyyy Canne / glacier
2 xxxx yyyy Canne / congélateur
3 xxxx yyyy Mixte
4 xxxx yyyy Senneur avec appât
5 xxxx yyyy Senneur sans appât
6 xxxx yyyy Grand senneur
...';

comment on column TYPE_BATEAU.C_TYP_B is
'Code numérique du type de bateau';

comment on column TYPE_BATEAU.L_TYP_B is
'Type de bateau';

/*==============================================================*/
/* Index : TYPE_BATEAU_PK                                       */
/*==============================================================*/
create unique index TYPE_BATEAU_PK on TYPE_BATEAU (
C_TYP_B
);

/*==============================================================*/
/* Table : TYPE_DCP                                             */
/*==============================================================*/
create table TYPE_DCP (
   C_NATURE_DCP         NUMERIC(2)           not null,
   L_NATURE_DCP         VARCHAR(64)          not null,
   constraint PK_TYPE_DCP primary key (C_NATURE_DCP)
);

comment on table TYPE_DCP is
'Codes des dispositifs des objets susceptibles de concenter des poissons 

1	tas de paille
2	Palme de cocotier / palmier
3	Arbre ( ou branche )
4	Charogne ( préciser la nature dans les notes )
5	Charogne balisée
6	Radeau balisé en dérive ( ligne et filet )
7	DCP ( Dispositif de Concentration de Poisson )
8	Thonier ( ou skiff )
9	Bateau d''appui ( supply )

...';

/*==============================================================*/
/* Index : TYPE_DCP_PK                                          */
/*==============================================================*/
create unique index TYPE_DCP_PK on TYPE_DCP (
C_NATURE_DCP
);

/*==============================================================*/
/* Table : VENT_BEAUFORT                                        */
/*==============================================================*/
create table VENT_BEAUFORT (
   C_BEAUFORT           NUMERIC(2)           not null,
   L_DESCRIP_VENT       VARCHAR(32)          not null,
   L_VITESSE_VENT       VARCHAR(16)          not null,
   L_DESCRIP_MER        VARCHAR(64)          null,
   L_HAUT_MOY_VAGUE     VARCHAR(16)          null,
   constraint PK_VENT_BEAUFORT primary key (C_BEAUFORT)
);

comment on table VENT_BEAUFORT is
'Qualifie les conditions de vent et l''état de la mer pour l''activité en cours
(code, description du vent, vitesse du vent, état de la mer, hauteur moyenne des vagues)

0	Calme	<1	Calme	0
1	Très légère brise	1 - 3	Calme, ridée	0 - 0,1
2	Légère brise	4 - 6	Belle, vaguelettes	0,1 - 0,5
3	Petite brise	7 - 10	Peu agitée, vaguelettes surmontées de moutons	0,5 - 1,25
4	Jolie brise	11 - 16	Agitée, patites vagues, nombreux moutons	1,25 - 2,5

...';

/*==============================================================*/
/* Index : VENT_BEAUFORT_PK                                     */
/*==============================================================*/
create unique index VENT_BEAUFORT_PK on VENT_BEAUFORT (
C_BEAUFORT
);

alter table ACTIVITE
   add constraint FK_ACTIVITE_ACT_BATEA_ACT_BATE foreign key (C_ACT_BATEAU)
      references ACT_BATEAU (C_ACT_BATEAU)
      on delete restrict on update restrict;

alter table ACTIVITE
   add constraint FK_ACTIVITE_ACT_ENV_R_ACT_ENV foreign key (C_ACT_ENV)
      references ACT_ENV (C_ACT_ENV)
      on delete restrict on update restrict;

alter table ACTIVITE
   add constraint FK_ACTIVITE_MODE_DETE_MODE_DET foreign key (C_MODE_DETECT)
      references MODE_DETECT (C_MODE_DETECT)
      on delete restrict on update restrict;

alter table ACTIVITE
   add constraint FK_ACTIVITE_NON_COUP__NON_COUP foreign key (C_NON_COUP_DE_SENNE)
      references NON_COUP_SENNE (C_NON_COUP_DE_SENNE)
      on delete restrict on update restrict;

alter table ACTIVITE
   add constraint FK_ACTIVITE_ROUTE_R_A_ROUTE foreign key (C_PROG, N_MAREE, D_OBS)
      references ROUTE (C_PROG, N_MAREE, D_OBS)
      on delete restrict on update restrict;

alter table ACTIVITE
   add constraint FK_ACTIVITE_VENT_BEAU_VENT_BEA foreign key (C_BEAUFORT)
      references VENT_BEAUFORT (C_BEAUFORT)
      on delete restrict on update restrict;

alter table ACT_SYSTOBS
   add constraint FK_ACT_SYST_ACT_SYSTO_ACTIVITE foreign key (C_PROG, N_MAREE, D_OBS, H_OBS)
      references ACTIVITE (C_PROG, N_MAREE, D_OBS, H_OBS)
      on delete restrict on update restrict;

alter table ACT_SYSTOBS
   add constraint FK_ACT_SYST_ACT_SYSTO_SYST_OBS foreign key (C_SYST_OBS)
      references SYST_OBS (C_SYST_OBS)
      on delete restrict on update restrict;

alter table BALISE_LUE
   add constraint FK_BALISE_L_BALISE_LU_DCP foreign key (C_PROG, N_MAREE, D_OBS, H_OBS, N_OBJET)
      references DCP (C_PROG, N_MAREE, D_OBS, H_OBS, N_OBJET)
      on delete restrict on update restrict;

alter table BALISE_LUE
   add constraint FK_BALISE_L_BALISE_LU_TYPE_BAL foreign key (C_BALISE)
      references TYPE_BALISE (C_BALISE)
      on delete restrict on update restrict;

alter table BATEAU
   add constraint FK_BATEAU_CAT_BATEA_CAT_BATE foreign key (C_CAT_B)
      references CAT_BATEAU (C_CAT_B)
      on delete restrict on update restrict;

alter table BATEAU
   add constraint FK_BATEAU_PAYS_R_BA_PAYS foreign key (C_PAYS)
      references PAYS (C_PAYS)
      on delete restrict on update restrict;

alter table BATEAU
   add constraint FK_BATEAU_TYPE_BATE_TYPE_BAT foreign key (C_TYP_B)
      references TYPE_BATEAU (C_TYP_B)
      on delete restrict on update restrict;

alter table CALEE
   add constraint FK_CALEE_ACTIVITE__ACTIVITE foreign key (C_PROG, N_MAREE, D_OBS, H_OBS)
      references ACTIVITE (C_PROG, N_MAREE, D_OBS, H_OBS)
      on delete restrict on update restrict;

alter table CALEE
   add constraint FK_CALEE_COUP_NUL__COUP_NUL foreign key (C_R_COUP_NUL)
      references COUP_NUL (C_R_COUP_NUL)
      on delete restrict on update restrict;

alter table CALEE_SYSTOBS
   add constraint FK_CALEE_SY_CALEE_SYS_CALEE foreign key (C_PROG, N_MAREE, D_OBS, H_OBS, ID_CALEE)
      references CALEE (C_PROG, N_MAREE, D_OBS, H_OBS, ID_CALEE)
      on delete restrict on update restrict;

alter table CALEE_SYSTOBS
   add constraint FK_CALEE_SY_CALEE_SYS_SYST_OBS foreign key (C_SYST_OBS)
      references SYST_OBS (C_SYST_OBS)
      on delete restrict on update restrict;

alter table CAPT_F
   add constraint FK_CAPT_F_CALEE_R_C_CALEE foreign key (C_PROG, N_MAREE, D_OBS, H_OBS, ID_CALEE)
      references CALEE (C_PROG, N_MAREE, D_OBS, H_OBS, ID_CALEE)
      on delete restrict on update restrict;

alter table CAPT_F
   add constraint FK_CAPT_F_DEV_F_R_C_DEV_F foreign key (C_DEVENIR_F)
      references DEV_F (C_DEVENIR_F)
      on delete restrict on update restrict;

alter table CAPT_F
   add constraint FK_CAPT_F_ESPECE_F__ESPECE_F foreign key (C_ESP_F)
      references ESPECE_F (C_ESP_F)
      on delete restrict on update restrict;

alter table CAPT_F
   add constraint FK_CAPT_F_RAISON_RE_RAISON_R foreign key (C_RAISON_REJET)
      references RAISON_REJET (C_RAISON_REJET)
      on delete restrict on update restrict;

alter table CAPT_F_CC
   add constraint FK_CAPT_F_C_CALEE_R_C_CALEE foreign key (C_PROG, N_MAREE, D_OBS, H_OBS, ID_CALEE)
      references CALEE (C_PROG, N_MAREE, D_OBS, H_OBS, ID_CALEE)
      on delete restrict on update restrict;

alter table CAPT_F_CC
   add constraint FK_CAPT_F_C_DEV_F_R_C_DEV_F foreign key (C_DEVENIR_F)
      references DEV_F (C_DEVENIR_F)
      on delete restrict on update restrict;

alter table CAPT_F_CC
   add constraint FK_CAPT_F_C_ESPECE_F__ESPECE_F foreign key (C_ESP_F)
      references ESPECE_F (C_ESP_F)
      on delete restrict on update restrict;

alter table CAPT_F_CC
   add constraint FK_CAPT_F_C_RAISON_RE_RAISON_R foreign key (C_RAISON_REJET)
      references RAISON_REJET (C_RAISON_REJET)
      on delete restrict on update restrict;

alter table CAPT_F_CC
   add constraint FK_CAPT_F_C_REF_LPF_C_LONPOI_F foreign key (C_OCEAN, LON_C_ESP_F, ANNEE_R_FAUNE_ASSOC)
      references LONPOI_F (C_OCEAN, C_ESP_F, ANNEE_R_FAUNE_ASSOC)
      on delete restrict on update restrict;

alter table CAPT_T
   add constraint FK_CAPT_T_CALEE_R_C_CALEE foreign key (C_PROG, N_MAREE, D_OBS, H_OBS, ID_CALEE)
      references CALEE (C_PROG, N_MAREE, D_OBS, H_OBS, ID_CALEE)
      on delete restrict on update restrict;

alter table CAPT_T
   add constraint FK_CAPT_T_CAT_POIDS_CAT_POID foreign key (C_ESP, C_CAT_P)
      references CAT_POIDS (C_ESP, C_CAT_P)
      on delete restrict on update restrict;

alter table CAPT_T
   add constraint FK_CAPT_T_RAISON_RE_RAISON_R foreign key (C_RAISON_REJET)
      references RAISON_REJET (C_RAISON_REJET)
      on delete restrict on update restrict;

alter table CAT_POIDS
   add constraint FK_CAT_POID_ESPECE_T__ESPECE_T foreign key (C_ESP)
      references ESPECE_T (C_ESP)
      on delete restrict on update restrict;

alter table DCP
   add constraint FK_DCP_ACTIVIT_R_ACTIVITE foreign key (C_PROG, N_MAREE, D_OBS, H_OBS)
      references ACTIVITE (C_PROG, N_MAREE, D_OBS, H_OBS)
      on delete restrict on update restrict;

alter table DCP
   add constraint FK_DCP_DEV_DCP_R_DEV_DCP foreign key (C_DEVENIR_DCP)
      references DEV_DCP (C_DEVENIR_DCP)
      on delete restrict on update restrict;

alter table DCP
   add constraint FK_DCP_OPERA_DCP_OPERA_DC foreign key (C_OPERA_OBJ)
      references OPERA_DCP (C_OPERA_OBJ)
      on delete restrict on update restrict;

alter table DCP
   add constraint FK_DCP_TYPE_DCP__TYPE_DCP foreign key (C_NATURE_DCP)
      references TYPE_DCP (C_NATURE_DCP)
      on delete restrict on update restrict;

alter table ECHANT_F
   add constraint FK_ECHANT_F_CALEE_R_E_CALEE foreign key (C_PROG, N_MAREE, D_OBS, H_OBS, ID_CALEE)
      references CALEE (C_PROG, N_MAREE, D_OBS, H_OBS, ID_CALEE)
      on delete restrict on update restrict;

alter table ECHANT_T
   add constraint FK_ECHANT_T_CALEE_R_E_CALEE foreign key (C_PROG, N_MAREE, D_OBS, H_OBS, ID_CALEE)
      references CALEE (C_PROG, N_MAREE, D_OBS, H_OBS, ID_CALEE)
      on delete restrict on update restrict;

alter table ESPECE_F
   add constraint FK_ESPECE_F_GR_ESP_F__GR_ESP_F foreign key (C_GR_ESP_F)
      references GR_ESP_F (C_GR_ESP_F)
      on delete restrict on update restrict;

alter table ESP_DCP
   add constraint FK_ESP_DCP_DCP_R_ESP_DCP foreign key (C_PROG, N_MAREE, D_OBS, H_OBS, N_OBJET)
      references DCP (C_PROG, N_MAREE, D_OBS, H_OBS, N_OBJET)
      on delete restrict on update restrict;

alter table ESP_DCP
   add constraint FK_ESP_DCP_ESPECE_F__ESPECE_F foreign key (C_ESP_F)
      references ESPECE_F (C_ESP_F)
      on delete restrict on update restrict;

alter table ESP_DCP
   add constraint FK_ESP_DCP_STATUT_ES_STATUT_E foreign key (C_STATUT_ESP_DCP)
      references STATUT_ESP_DCP (C_STATUT_ESP_DCP)
      on delete restrict on update restrict;

alter table ESTIM_BANC
   add constraint FK_ESTIM_BA_CALEE_R_E_CALEE foreign key (C_PROG, N_MAREE, D_OBS, H_OBS, ID_CALEE)
      references CALEE (C_PROG, N_MAREE, D_OBS, H_OBS, ID_CALEE)
      on delete restrict on update restrict;

alter table ESTIM_BANC
   add constraint FK_ESTIM_BA_ESP_T_R_E_ESPECE_T foreign key (C_ESP)
      references ESPECE_T (C_ESP)
      on delete restrict on update restrict;

alter table ESTIM_BANC_OBJ
   add constraint FK_ESTIM_BA_DCP_R_EST_DCP foreign key (C_PROG, N_MAREE, D_OBS, H_OBS, N_OBJET)
      references DCP (C_PROG, N_MAREE, D_OBS, H_OBS, N_OBJET)
      on delete restrict on update restrict;

alter table ESTIM_BANC_OBJ
   add constraint FK_ESTIM_BA_ESP_T_R_E_ESPECE_T foreign key (C_ESP)
      references ESPECE_T (C_ESP)
      on delete restrict on update restrict;

alter table LONPOI_F
   add constraint FK_LONPOI_F_ESPECE_F__ESPECE_F foreign key (C_ESP_F)
      references ESPECE_F (C_ESP_F)
      on delete restrict on update restrict;

alter table LONPOI_F
   add constraint FK_LONPOI_F_OCEAN_R_L_OCEAN foreign key (C_OCEAN)
      references OCEAN (C_OCEAN)
      on delete restrict on update restrict;

alter table LONPOI_T
   add constraint FK_LONPOI_T_ESPECE_T__ESPECE_T foreign key (C_ESP)
      references ESPECE_T (C_ESP)
      on delete restrict on update restrict;

alter table LONPOI_T
   add constraint FK_LONPOI_T_OCEAN_R_L_OCEAN foreign key (C_OCEAN)
      references OCEAN (C_OCEAN)
      on delete restrict on update restrict;

alter table MAREE
   add constraint FK_MAREE_BATEAU_R__BATEAU foreign key (C_BAT)
      references BATEAU (C_BAT)
      on delete restrict on update restrict;

alter table MAREE
   add constraint FK_MAREE_OBSERVATE_OBSERVAT foreign key (ID_OBSERVATEUR)
      references OBSERVATEUR (ID_OBSERVATEUR)
      on delete restrict on update restrict;

alter table MAREE
   add constraint FK_MAREE_OCEAN_R_M_OCEAN foreign key (C_OCEAN)
      references OCEAN (C_OCEAN)
      on delete restrict on update restrict;

alter table MAREE
   add constraint FK_MAREE_PROGRAMME_PROGRAMM foreign key (C_PROG)
      references PROGRAMME (C_PROG)
      on delete restrict on update restrict;

alter table MAREE
   add constraint FK_MAREE_SENNE_R_M_SENNE foreign key (ID_SENNE)
      references SENNE (ID_SENNE)
      on delete restrict on update restrict;

alter table ROUTE
   add constraint FK_ROUTE_MAREE_R_R_MAREE foreign key (C_PROG, N_MAREE)
      references MAREE (C_PROG, N_MAREE)
      on delete restrict on update restrict;

alter table TAILLE_F
   add constraint FK_TAILLE_F_ECHANT_F__ECHANT_F foreign key (C_PROG, N_MAREE, D_OBS, H_OBS, ID_CALEE, ID_ECHANT_F)
      references ECHANT_F (C_PROG, N_MAREE, D_OBS, H_OBS, ID_CALEE, ID_ECHANT_F)
      on delete restrict on update restrict;

alter table TAILLE_F
   add constraint FK_TAILLE_F_ESPECE_F__ESPECE_F foreign key (C_ESP_F)
      references ESPECE_F (C_ESP_F)
      on delete restrict on update restrict;

alter table TAILLE_T
   add constraint FK_TAILLE_T_ECHANT_T__ECHANT_T foreign key (C_PROG, N_MAREE, D_OBS, H_OBS, ID_CALEE, ID_ECHANT_T)
      references ECHANT_T (C_PROG, N_MAREE, D_OBS, H_OBS, ID_CALEE, ID_ECHANT_T)
      on delete restrict on update restrict;

alter table TAILLE_T
   add constraint FK_TAILLE_T_ESPECE_T__ESPECE_T foreign key (C_ESP)
      references ESPECE_T (C_ESP)
      on delete restrict on update restrict;

