#! /bin/bash

debug () {
  if [ "$DEBUG" = "true" ]; then
    echo "$currentRow/$nbRows  $1"
  fi
}

info() {
  echo "$currentRow/$nbRows  $1"
}

replaceInFile() {
  local file=$1
  local oldValue=$2
  local newValue=$3

  grep ${oldValue} ${file} > /dev/null
  if [ $? -eq 0 ]; then
    info "Replace $oldValue to $newValue in $file"
    sed -i 's/'"$oldValue"'/'"$newValue"'/' ${file}
  fi
}

renameClass() {
  local oldClass=$1
  local newClass=$2
  debug ""
  debug "Rename class $oldClass to $newClass"

  for file in $(ls ${tmpDir}); do
    tmpFile=${tmpDir}/${file}

    replaceInFile ${tmpFile} ${oldClass} ${newClass}

  done
}

renameAttribute() {
  local oldName=$1
  local newName=$2
  debug "Rename attribut $oldName to $newName"
  oldFirstLetter=${oldName:0:1}
  oldFirstLetterCap=${oldFirstLetter^^}
  oldGetter="get${oldFirstLetterCap}${oldName:1}"
  oldGetter2="is${oldFirstLetterCap}${oldName:1}"
  oldSetter="set${oldFirstLetterCap}${oldName:1}"
  newFirstLetter=${newName:0:1}
  newFirstLetterCap=${newFirstLetter^^}
  newGetter="get${newFirstLetterCap}${newName:1}"
  newGetter2="is${newFirstLetterCap}${newName:1}"
  newSetter="set${newFirstLetterCap}${newName:1}"

  debug "Getter: $oldGetter to $newGetter"
  debug "Getter: $oldGetter2 to $newGetter2"
  debug "Setter: $oldSetter to $newSetter"

  for file in $(ls ${tmpDir}); do
    tmpFile=${tmpDir}/${file}

    replaceInFile ${tmpFile} ${oldName} ${newName}
    replaceInFile ${tmpFile} ${oldGetter} ${newGetter}
    replaceInFile ${tmpFile} ${oldSetter} ${newSetter}
    replaceInFile ${tmpFile} ${oldGetter2} ${newGetter2}

  done
}

basedir=$1
classFile=$2
attributFile=$3

DEBUG=false
if [ $# -eq 4 -a "$4" = "debug" ]; then
  DEBUG=true
fi

files=/tmp/observe_translateFiles
rm -rf ${files}
find ${basedir}/doc/v4/observe.zargo_FILES/ -type f | grep -v observe.argo > ${files}

tmpDir=/tmp/observe_translate

rm -rf ${tmpDir}
mkdir ${tmpDir}

for file in $(cat ${files}) ; do
    cp -v ${file} ${tmpDir}
done

nbClasses=$(cat ${classFile} | wc -l)
nbAttributes=$(cat ${attributFile} | wc -l)
nbRows=$(($nbClasses + $nbAttributes))
currentRow=0

while read line
do
  currentRow=$(($currentRow +1))

  if [ "${line:0:1}" == "#" ] ; then
     debug "skip comment $line"
     continue
  fi

  oldName=$(echo ${line} | cut -d';' -f1)
  newName=$(echo ${line} | cut -d';' -f2)

  renameAttribute ${oldName} ${newName}

done < ${attributFile}

while read line
do
  currentRow=$(($currentRow +1))

  if [ "${line:0:1}" == "#" ] ; then
     debug "skip comment $line"
     continue
  fi

  oldName=$(echo ${line} | cut -d';' -f1)
  newName=$(echo ${line} | cut -d';' -f2)

  renameClass ${oldName} ${newName}

done < ${classFile}

#reset; ./doc/v4/translateModel.sh $(pwd) doc/v4/traduction_tables.csv doc/v4/traduction_attributes.csv
#(cd /tmp ; rm -rf observe.zargo ; cd observe_translate ; cp ~/projets/codelutin.com/observe/doc/v4/observe.zargo_FILES/observe.argo .; zip -r /tmp/observe.zargo * ; argouml /tmp/observe.zargo)


