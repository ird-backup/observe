-- Add longline reference XX
CREATE TABLE XX( topiaid character varying(255) NOT NULL,topiaversion BIGINT NOT NULL, topiacreatedate DATE, code INTEGER, status INTEGER DEFAULT 1, uri character varying(255), label1 character varying(255), label2 character varying(255), label3 character varying(255), label4 character varying(255), label5 character varying(255), label6 character varying(255), label7 character varying(255), label8 character varying(255) );
ALTER TABLE XX ADD CONSTRAINT CONSTRAINT_XX_PKEY PRIMARY KEY(TOPIAID);
ALTER TABLE XX ADD CONSTRAINT CONSTRAINT_XX_UNIQUE_KEY_URI UNIQUE (uri);
